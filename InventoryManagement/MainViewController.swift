//
//  ViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 29/08/22.
//

import UIKit
import CoreData

enum TabBarSelectedType {
    case salesOrder
    case purchaseOrder
    case vendor
    case customer
    case inventory
}

class MainViewController: UITabBarController {
    
    // List of View Controllers for tab bar controller
    private var types: [TabBarSelectedType] = [
        .salesOrder,
        .purchaseOrder,
        .vendor,
        .customer,
        .inventory,
    ]
    
    // Configurations for each View Controller
    private let tabConfig = [
        ["Sales Order", "coloncurrencysign.circle", "coloncurrencysign.circle.fill"],
        ["Purchase Order", "cart", "cart.fill"],
        ["Vendors", "person.text.rectangle", "person.text.rectangle.fill"],
        ["Customers", "person", "person.fill"],
        ["Inventory", "archivebox", "archivebox.fill"],
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGroupedBackground
        
        tabBarConfig(with: tabConfig)
//        POGenerator.resetPOGenerator()
//        SOGenerator.resetSOGenerator()
        POGenerator.checkPersistance()
        SOGenerator.checkPersistance()
    }
    
    // Iterating throught configs and assigning proper values
    private func tabBarConfig(with values: [[String]]) {
        
        var viewControllers = [UIViewController]()
        
        for (index, type ) in types.enumerated() {
            let vc = SplitViewBackgroundController(type: type)
            vc.tabBarItem.title = tabConfig[index][0]
            vc.tabBarItem.image = UIImage(systemName: tabConfig[index][1])
            vc.tabBarItem.selectedImage = UIImage(systemName: tabConfig[index][2])
            viewControllers.append(vc)
        }
        
        setViewControllers(viewControllers, animated: true)
        
    }

    // Function to add new View Controller to Tab Bar
    private func add(viewController: UIViewController) {
        viewControllers?.append(viewController)
    }
    
}


