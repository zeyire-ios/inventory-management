//
//  Item+Enums.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 16/10/22.
//

import Foundation

enum ItemFormCellType: String {
    case itemName = "Item Name"
    case itemQuantity = "Item Quantity"
    case lowStockLimit = "Low Stock Limit"
    case isActive = "Active"
    case error
}

enum ItemWarning {
    case outOfStock
    case lowStock
    case inActive
    case remove
}

enum ItemAdjustment {
    case lowLimit
}

enum ActorFormCellType: String {
    case displayName = "Display Name"
    case companyName = "Company Name"
    case mobile = "Mobile"
    case email = "E-Mail"
    case address = "Address"
    case remarks = "Remarks"
    case error
}
