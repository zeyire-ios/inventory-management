//
//  ItemListForm.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 16/10/22.
//

import Foundation

struct ItemFormList {
    
    let forms: [[ItemFormCellType]] = [
        [.itemName, .itemQuantity],
        [.lowStockLimit],
        [.isActive],
    ]
    
    /// Get Next Index of item forms
    /// - Parameter currentIndex: Provide the current index of the Cell ( TextField )
    /// - Returns: the next index of the array struct above.
    func getNextIndex(for currentIndex: IndexPath) -> IndexPath?{
        var nextIndexPath: IndexPath = currentIndex
        switch (currentIndex.section) {
        case 0:
            if currentIndex.row < forms[0].count - 1 {
                nextIndexPath.row = currentIndex.row + 1
            } else {
                nextIndexPath.section += 1
                nextIndexPath.row = 0
            }
        case 1:
            if currentIndex.row < forms[1].count - 1 {
                nextIndexPath.row = currentIndex.row + 1
            } else {
                nextIndexPath.section = 0
                nextIndexPath.row = 0
            }
        default:
            print("Getting next Index: Case not handled")
            return nil
        }
        return nextIndexPath
    }
    
}
