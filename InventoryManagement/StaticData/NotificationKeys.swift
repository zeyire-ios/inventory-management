//
//  ItemKeys.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/10/22.
//

import Foundation

struct ItemKey {
    
    private static let reloadItemName = "com.abilash.ReloadItem"
    
    static let reloadItem = NSNotification.Name(reloadItemName)
    
    private static let deletedItemName = "com.abilash.DeletedItem"
    
    static let deleteItem = NSNotification.Name(deletedItemName)
    
}

struct CustomerKey {
    
    private static let reloadCustomerName = "com.abilash.ReloadCustomer"
    
    static let reloadCustomer = NSNotification.Name(reloadCustomerName)
    
    private static let deleteCustomerName = "com.abilash.DeleteCustomer"
    
    static let deleteCustomer = NSNotification.Name(deleteCustomerName)
    
}

struct VendorKey {
    
    private static let reloadVendorName = "com.abilash.ReloadVendor"
    
    static let reloadVendor = NSNotification.Name(reloadVendorName)
    
    private static let deleteVendorName = "com.abilash.DeleteVendor"
    
    static let deleteVendor = NSNotification.Name(deleteVendorName)
    
}

struct POKey {
    
    private static let reloadPOName = "com.abilash.ReloadPO"
    
    static let reloadPO = NSNotification.Name(reloadPOName)
    
}

struct SOKey {
    
    private static let reloadSOName = "com.abilash.ReloadSO"
    
    static let reloadSO = NSNotification.Name(reloadSOName)
    
}
