//
//  UserDefaults+Extensions.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 27/09/22.
//

import Foundation
import UIKit

extension UserDefaults {
    func color(forKey key: String) -> UIColor? {
        var color: UIColor?
        if let colorData = data(forKey: key) {
            color = try? NSKeyedUnarchiver.unarchivedObject(ofClass: UIColor.self, from: colorData)
        }
        return color
    }
    
    func setColor(color: UIColor?, forKey key: String) {
        var colorData: Data?
        if let color = color {
            colorData = try? NSKeyedArchiver.archivedData(withRootObject: color, requiringSecureCoding: false) as NSData? as Data?
//            colorData = NSKeyedArchiver.archivedData(withRootObject: color) as NSData?
        }
        set(colorData, forKey: key)
    }
}
