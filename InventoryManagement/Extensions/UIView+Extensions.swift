//
//  UIView+Extensions.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 19/10/22.
//

import Foundation
import UIKit

extension UIView {
    
    enum RelativeType {
        case equalTo
        case lessThanEqualTo
        case greaterThanEqualTo
    }
    
    enum YAnchorType {
        case top
        case bottom
        case centerY
    }
    
    enum XAnchorType {
        case leading
        case trailing
        case centerX
    }
    
    enum DimensionType {
        case width
        case height
    }
    
    func setAnchor(_ anchorY: YAnchorType, as relative: RelativeType = .equalTo, to _anchorY: NSLayoutYAxisAnchor, padding: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        switch(anchorY, relative) {
        case (.top, .equalTo):
            topAnchor.constraint(equalTo: _anchorY, constant: padding).isActive = true
        case (.top, .greaterThanEqualTo):
            topAnchor.constraint(greaterThanOrEqualTo: _anchorY, constant: padding).isActive = true
        case (.top, .lessThanEqualTo):
            topAnchor.constraint(lessThanOrEqualTo: _anchorY, constant: padding).isActive = true
        case (.bottom, .equalTo):
            bottomAnchor.constraint(equalTo: _anchorY, constant: padding).isActive = true
        case (.bottom, .lessThanEqualTo):
            bottomAnchor.constraint(lessThanOrEqualTo: _anchorY, constant: padding).isActive = true
        case (.bottom, .greaterThanEqualTo):
            bottomAnchor.constraint(greaterThanOrEqualTo: _anchorY, constant: padding).isActive = true
        case (.centerY, .equalTo):
            centerYAnchor.constraint(equalTo: _anchorY, constant: padding).isActive = true
        case (.centerY, .lessThanEqualTo):
            centerYAnchor.constraint(lessThanOrEqualTo: _anchorY, constant: padding).isActive = true
        case (.centerY, .greaterThanEqualTo):
            centerYAnchor.constraint(greaterThanOrEqualTo: _anchorY, constant: padding).isActive = true
        }
    }
    
    func setAnchor(_ anchorX: XAnchorType, as relative: RelativeType = .equalTo, to _anchorX: NSLayoutXAxisAnchor, padding: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        switch (anchorX, relative) {
        case (.leading, .equalTo):
            leadingAnchor.constraint(equalTo: _anchorX, constant: padding).isActive = true
        case (.leading, .lessThanEqualTo):
            leadingAnchor.constraint(lessThanOrEqualTo: _anchorX, constant: padding).isActive = true
        case (.leading, .greaterThanEqualTo):
            leadingAnchor.constraint(greaterThanOrEqualTo: _anchorX, constant: padding).isActive = true
        case (.trailing, .equalTo):
            trailingAnchor.constraint(equalTo: _anchorX, constant: padding).isActive = true
        case (.trailing, .lessThanEqualTo):
            trailingAnchor.constraint(lessThanOrEqualTo: _anchorX, constant: padding).isActive = true
        case (.trailing, .greaterThanEqualTo):
            trailingAnchor.constraint(greaterThanOrEqualTo: _anchorX, constant: padding).isActive = true
        case (.centerX, .equalTo):
            centerXAnchor.constraint(equalTo: _anchorX, constant: padding).isActive = true
        case (.centerX, .lessThanEqualTo):
            centerXAnchor.constraint(lessThanOrEqualTo: _anchorX, constant: padding).isActive = true
        case (.centerX, .greaterThanEqualTo):
            centerXAnchor.constraint(greaterThanOrEqualTo: _anchorX, constant: padding).isActive = true
        }
    }
    
    func setAnchor(_ anchorD: DimensionType, as relative: RelativeType? = nil, to _anchorD: NSLayoutDimension? = nil, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        switch (anchorD, relative) {
        case (.width, .equalTo):
            if let _anchorD = _anchorD {
                widthAnchor.constraint(equalTo: _anchorD, constant: constant).isActive = true
            }
        case (.width, nil):
            widthAnchor.constraint(equalToConstant: constant).isActive = true
        case (.height, .equalTo):
            if let _anchorD = _anchorD {
                heightAnchor.constraint(equalTo: _anchorD, constant: constant).isActive = true
            }
        case (.height, nil):
            heightAnchor.constraint(equalToConstant: constant).isActive = true
        case (.width, .lessThanEqualTo):
            if let _anchorD = _anchorD {
                widthAnchor.constraint(lessThanOrEqualTo: _anchorD, constant: constant).isActive = true
            }
        case (.height, .greaterThanEqualTo):
            if let _anchorD = _anchorD {
                heightAnchor.constraint(greaterThanOrEqualTo: _anchorD, constant: constant).isActive = true
            }
        default:
            return
        }
    }
    
}
