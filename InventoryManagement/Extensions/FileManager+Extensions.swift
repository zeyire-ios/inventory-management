//
//  FileManager+Extensions.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 20/09/22.
//

import Foundation

public extension FileManager {
    static var documentDirectoryURL: URL {
        get {
            `default`.urls(for: .documentDirectory, in: .userDomainMask)[0]
        }
    }
    static var picturesDirectoryURL: URL {
        get {
            `default`.urls(for: .picturesDirectory, in: .userDomainMask)[0]
        }
    }
}
