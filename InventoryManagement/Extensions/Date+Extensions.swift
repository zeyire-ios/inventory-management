//
//  Date+Extensions.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 25/09/22.
//

import Foundation
import UIKit

extension Date {
    var startOfMonth: Date {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        calendar.timeZone = TimeZone.current
        let dateComponents = calendar.dateComponents([.year, .month], from: self)
        return calendar.date(from: dateComponents)!
    }
    var endOfMonth: Date {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        let dateComponents = DateComponents(month:1)
        return calendar.date(byAdding: dateComponents, to: startOfMonth)!
    }
}

