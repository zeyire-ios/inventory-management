//
//  PODetailModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import Foundation

struct PODetailModal {
    var orderID: UUID?
    var purchaseNo: String?
    var purchaseDate: Date?
    var isOrderReceived: Bool?
    var itemCost: Double?
    var itemQuantity: Int?
    var discount: Double?
    var totalCost: Double?
    var item: Item?
    var vendor: Vendor?
}
