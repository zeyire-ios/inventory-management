//
//  POViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit
import CoreData

class POViewController: UIViewController {
    
    lazy private var tableView: UITableView = {
        var tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.register(OrderListCell.self, forCellReuseIdentifier: OrderListCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.backgroundColor = .systemGroupedBackground
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy var createPurchaseButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Create Purchase Order"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(addButtonPressed), for: .touchUpInside)
        return button
    }()
    
    let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter
    }()
    
    lazy var searchBar: UISearchController = {
        var searchBar = UISearchController()
        searchBar.searchBar.backgroundColor = .systemGroupedBackground
        return searchBar
    }()
    
    private let noFilterViewImage = UIImage(named: "FilterCross")?.withTintColor(.secondaryLabel)
    
    private let noSearchViewImage = UIImage(named: "SearchProfile")?.withTintColor(.secondaryLabel)
    
    private let noOrderViewImage = UIImage(named: "NoTaskNotepad")?.withTintColor(.secondaryLabel)
    
    private let searchResultText = "Sorry, No Search Results!"
    
    private let emptyOrderText = "Sorry, No Orders are made yet!"
    
    private let CDM = CoreDataManager.shared
    
    var poListModal: [PODetailModal]?
    
    private var sortBy: POSort.SortBy {
        POSort.checkSortBy()
    }
    
    private var filter: POSort.FilterBy {
        POSort.checkFilter()
    }
    
    private var ascending: Bool {
        POSort.checkSortOrder()
    }
    
    private var timer = Timer()
    
    weak var splitDelegate: SplitControllerDelegate?
    
    private var firstTime: Bool = true {
        didSet {
            selectFirstRowInSplitView()
        }
    }
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = .systemGroupedBackground
        
        setupConfig()
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
        
        addNotificationObservers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupConfig() {
        tableViewConfig()
        splitViewConfig()
        navigationConfig()
        backgroundViewConfig()
        createRefreshControl()
    }
    
    func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        view.addSubview(createPurchaseButton)
        createPurchaseButton.translatesAutoresizingMaskIntoConstraints = false
        
        backgroundView.setAnchor(.centerY, to: view.centerYAnchor)
        backgroundView.setAnchor(.top, as: .greaterThanEqualTo, to: tableView.topAnchor)
        backgroundView.setAnchor(.bottom, as: .lessThanEqualTo, to: tableView.bottomAnchor)
        backgroundView.setAnchor(.leading, to: view.safeAreaLayoutGuide.leadingAnchor, padding: 16)
        backgroundView.setAnchor(.trailing, to: view.safeAreaLayoutGuide.trailingAnchor, padding: -16)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: tableView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            
            createPurchaseButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createPurchaseButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            
        ])
    }
    
    private func splitViewConfig() {
        
        splitViewController?.delegate = self
        splitViewController?.preferredPrimaryColumnWidthFraction = 0.4
        
    }
    
    func navigationConfig() {
        navigationItem.title = "Purchase Order"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barTintColor = .systemGroupedBackground
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.searchController = self.searchBar
        navigationItem.searchController?.searchBar.placeholder = "Search Orders"
        navigationItem.searchController?.searchBar.delegate = self
        navigationItem.searchController?.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.backButtonTitle = "Back"
        navigationItem.rightBarButtonItems = [ UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed(_:))), UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(sortButtonPressed)) ]
    }
    
    private func backgroundViewConfig() {
        backgroundView.isHidden = true
        createPurchaseButton.isHidden = true
    }
    
    // MARK: - Functions
    
    // This function creates the list modal to display at table view
    func fetchToViewModal(searchFor searchTerm: String? = nil, sortBy: POSort.SortBy, ascending: Bool, filterBy: POSort.FilterBy) {
        DispatchQueue.main.async {
            [weak self] in
            self?.activityIndicator.startAnimating()
        }
        DispatchQueue.global().async {
            [weak self] in
            let secondary: POSort.SortBy = (sortBy == .purchaseNo ? .vendor : .purchaseNo)
            var predicates: [NSPredicate] = []
            if let searchTerm = searchTerm {
                predicates.append(NSPredicate(format: "(purchaseNo CONTAINS[c] %@) OR (vendor.displayName CONTAINS[c] %@)", searchTerm, searchTerm))
            }
            if filterBy != .all {
                predicates.append((self?.convertToCoreDataPredicate(filter: filterBy))!)
            }
            let poCoreDataList = self?.CDM.fetchPurchaseOrder(forItem: nil, check: predicates, sortBy: [NSSortDescriptor(key: self?.convertToCoreDataAttribute(sortBy), ascending: ascending), NSSortDescriptor(key: self?.convertToCoreDataAttribute(secondary), ascending: ascending)])
            self?.poListModal = poCoreDataList?.map({
                po in
                return PODetailModal(orderID: po.purchaseID, purchaseNo: po.purchaseNo, purchaseDate: po.purchaseDate, isOrderReceived: po.isOrderReceived, itemCost: po.purchaseItemCost, itemQuantity: Int(po.purchaseQuantity), discount: po.discount, totalCost: po.purchaseTotalCost, item: po.item, vendor: po.vendor)
            })
            DispatchQueue.main.async {
                self?.showResultBackgroundFor(emptyList: (poCoreDataList?.isEmpty)!, withSearchTerm:  searchTerm != nil, withFilter:  filterBy != .all)
                self?.activityIndicator.stopAnimating()
                self?.tableView.reloadData()
                
                if self?.firstTime ?? false {
                    self?.firstTime = false
                }
            }
        }
    }
    
    /// Generates a background view when the database can't fetch any results based on few conditions
    /// - Parameters:
    ///   - emptyList: A bool value that represents the database fetched zero entities
    ///   - withSearchTerm: A bool value that represents if a search term is used when fetching data
    ///   - withFilter: A bool value that represents whether any filters are applied.
    private func showResultBackgroundFor(emptyList: Bool, withSearchTerm: Bool, withFilter: Bool) {
        switch (emptyList, withSearchTerm, withFilter) {
            
            // When Searching produces no results
            // All Search results are applied with no filters, hence this case
        case (true, true, false):
            createPurchaseButton.isHidden = true
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noSearchViewImage
            backgroundView.titleLabel.text = searchResultText
            
            // When applying filters produces no results
        case (true, false, true):
            createPurchaseButton.isHidden = true
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noFilterViewImage
            switch (filter) {
            case .newLastWeek:
                backgroundView.titleLabel.text = "Sorry, No Orders made Last week!"
            case .newThisWeek:
                backgroundView.titleLabel.text = "Sorry, No Orders made this week!"
            case .notReceived:
                backgroundView.titleLabel.text = "No Orders to be Received!"
            case .received:
                backgroundView.titleLabel.text = "Sorry, Received Orders!"
                #warning("Have to decide on this")
            case .recentlyCreated:
                backgroundView.titleLabel.text = "Sorry, No Orders created recently!"
            default:
                break
            }
            
            // When there are no more Items in Inventory.
        case (true, false, false):
            createPurchaseButton.isHidden = false
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noOrderViewImage
            backgroundView.titleLabel.text = emptyOrderText
            
        default:
            createPurchaseButton.isHidden = true
            backgroundView.isHidden = true
        }
    }
    
    private func createRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToReferesh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func deleteRefreshControl() {
        tableView.refreshControl = nil
    }
    
    private func convertToCoreDataAttribute(_ attribute: POSort.SortBy) -> String {
        switch (attribute) {
        case .purchaseNo:
            return "purchaseNo"
        case .vendor:
            return "vendor.displayName"
        case .date:
            return "purchaseDate"
        }
    }
    
    private func convertToCoreDataPredicate(filter: POSort.FilterBy) -> NSPredicate {
        let calendar = Calendar.current
        let startOfToday = calendar.startOfDay(for: Date())
        let lastWeekEndDate = calendar.date(byAdding: .day, value: -7, to: startOfToday)
        let lastWeekStartDate = calendar.date(byAdding: .day, value: -7, to: lastWeekEndDate!)
        switch (filter) {
            
        case .all:
            return NSPredicate()
        case .received:
            return NSPredicate(format: "isOrderReceived = true")
        case .notReceived:
            return NSPredicate(format: "isOrderReceived = false")
        case .newThisWeek:
            return NSPredicate(format: "purchaseDate > %@", lastWeekEndDate! as CVarArg)
        case .newLastWeek:
            return NSPredicate(format: "(%@ < purchaseDate) AND (purchaseDate < %@)", lastWeekStartDate! as CVarArg, lastWeekEndDate! as CVarArg)
        case .recentlyCreated:
            return NSPredicate(format: "purchaseDate > %@", startOfToday as CVarArg)
        }
    }
    
    private func selectFirstRowInSplitView() {
        if traitCollection.userInterfaceIdiom == .pad {
            if let id = poListModal?.first?.orderID {
                let vc = PODetailVC(id: id)
                splitDelegate?.itemSelected(present: vc)
            }
        }
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: POKey.reloadPO, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: ItemKey.deleteItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: ItemKey.deleteItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: VendorKey.reloadVendor, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: VendorKey.deleteVendor, object: nil)
    }
    
    // MARK: - Objc Functions
    
    @objc
    private func dismissView() {
        if traitCollection.userInterfaceIdiom == .pad {
            let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
            vc?.setViewControllers([SplitViewController.blankVC], animated: true)
        } else if traitCollection.userInterfaceIdiom == .phone {
            splitViewController?.hide(.secondary)
        }
    }
    
    @objc
    private func reloadModal() {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
    
    @objc func addButtonPressed(_ sender: UIBarButtonItem) {
        let vc = UINavigationController(rootViewController: AddPOVC())
        vc.modalPresentationStyle = .formSheet
        present(vc, animated: true)
    }
    
    @objc
    private func sortButtonPressed(_ sender: UIBarButtonItem) {
        present(UINavigationController(rootViewController: SortViewController<POSort.SortBy, POSort.FilterBy>(sortModal: POSort() as SortOptions, sortBy: POSort.sortBy, sortedBy: sortBy, ascending: ascending, filterBy: POSort.filterBy, filter: filter, completion: {
            [weak self] in
            self?.fetchToViewModal(sortBy: (self?.sortBy) ?? POSort.SortBy.purchaseNo, ascending: (self?.ascending) ?? true, filterBy: self?.filter ?? POSort.FilterBy.all)
        })), animated: true)
    }
    
    @objc
    private func pullToReferesh(_ sender: UIRefreshControl) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
        sender.endRefreshing()
    }
    
}

extension POViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if poListModal?.count == 0 || poListModal == nil {
            tableView.isScrollEnabled = false
            return 0
        } else {
            backgroundViewConfig()
            tableView.isScrollEnabled = true
            return poListModal?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let modal = poListModal?[indexPath.row] else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderListCell.identifier, for: indexPath) as! OrderListCell
        
        cell.setValuesFor(
            orderNo: modal.purchaseNo ?? "000",
            actorName: modal.vendor?.displayName ?? "Unknown Vendor!",
            amount: currencyFormatter.string(from: (modal.totalCost ?? 0) as NSNumber),
            date: dateFormatter.string(from: (modal.purchaseDate) ?? Date(timeIntervalSince1970: 0))
        )
        
        cell.setReceivedStatus(modal.isOrderReceived ?? false)
        
        return cell
    }
    
}

extension POViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = poListModal?[indexPath.row].orderID else { return }
        let vc = PODetailVC(id: id)
        vc.hidesBottomBarWhenPushed = true
        splitDelegate?.itemSelected(present: vc)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions: [UIContextualAction] = [UIContextualAction]()
        if let order = poListModal?[indexPath.row] {
            if !order.isOrderReceived! {
                let receiveAction = UIContextualAction(style: .normal,
                                                   title: "Mark as Received") {
                    [weak self] (action, view, completionHandler) in
                    
                    let alert = UIAlertController(title: "Are you sure?", message: "Once you mark an Order as Received, you can't undo it. Are you sure you received it.", preferredStyle: .actionSheet)
                    
                    alert.addAction(UIAlertAction(title: "Proceed", style: .destructive) {
                        action in
                        self?.handleMarkAsReceived(indexPath, id: order.orderID)
                        completionHandler(true)
                    })
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                    
                    // For showing action sheet in iPad, we have to make it presented as popoverController; can be done like this or by assigning to naviBar button.
                    if let popoverController = alert.popoverPresentationController {
                        popoverController.sourceView = self?.splitViewController?.view
                        popoverController.sourceRect = CGRect(x: (self?.splitViewController?.view.bounds.midX)!, y: (self?.splitViewController?.view.bounds.midY)!, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    
                    self?.present(alert, animated: true)
                    completionHandler(false)
                    
                }
                receiveAction.backgroundColor = UIColor(red: 156/255.0, green: 158/255.0, blue: 254/255.0, alpha: 1)
                actions.append(receiveAction)
            }
        }
        let cloneAction = UIContextualAction(style: .normal,
                                             title: "Clone Order") {
                                            [weak self] (action, view, completionHandler) in
                                            self?.cloneOrder(indexPath)
                                            completionHandler(true)
        }
        cloneAction.image = UIImage(systemName: "doc.on.doc")
        actions.append(cloneAction)
        return UISwipeActionsConfiguration(actions: actions)
    }
    
    private func handleMarkAsReceived(_ indexPath: IndexPath, id: UUID?) {
        if let order = self.CDM.fetchPurchaseOrder(forOrder: id)?.first {
            if !order.isOrderReceived {
                order.isOrderReceived.toggle()
            }
        }
        do {
            try self.CDM.viewContext.save()
            NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
        } catch let error {
            print("\n\nPOViewController -> Core Data Save failed\n\(error)\n\n")
        }
        self.poListModal?[indexPath.row].isOrderReceived = true
        if filter == .notReceived {
            self.poListModal?.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            dismissView()
            if poListModal?.isEmpty ?? false {
                backgroundView.isHidden = false
                backgroundView.noDataImage.image = noFilterViewImage
                backgroundView.titleLabel.text = "No Orders to be Received!"
            }
        } else {
            self.tableView.reconfigureRows(at: [indexPath])
        }
    }
    
    private func cloneOrder(_ indexPath: IndexPath) {
        if let order = poListModal?[indexPath.row] {
            let vc = UINavigationController(rootViewController: AddPOVC(item: order.item, vendor: order.vendor, itemQuantity: order.itemQuantity, itemCost: order.itemCost, discount: order.discount))
            vc.modalPresentationStyle = .formSheet
            present(vc, animated: true)
        }
    }
    
}

extension POViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {
            [weak self] timer in
            if searchText.isEmpty {
                self?.fetchToViewModal(sortBy: self?.sortBy ?? POSort.SortBy.purchaseNo, ascending: self?.ascending ?? true, filterBy: self?.filter ?? POSort.FilterBy.all)
            } else {
                self?.fetchToViewModal(searchFor: searchText,sortBy: self?.sortBy ?? POSort.SortBy.purchaseNo, ascending: self?.ascending ?? true, filterBy: self?.filter ?? POSort.FilterBy.all)
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        timer.invalidate()
        fetchToViewModal(searchFor: searchBar.text, sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
}

extension POViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }
    
}

extension POViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        deleteRefreshControl()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        createRefreshControl()
    }
    
}
