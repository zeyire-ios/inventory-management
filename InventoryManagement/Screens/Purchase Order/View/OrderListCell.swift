//
//  POTableViewCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit

class OrderListCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.OrderCell"
    
    private let orderSymbol: UIImageView = {
        let config = UIImage.SymbolConfiguration(pointSize: 17, weight: .semibold, scale: .small)
        let image = UIImage(systemName: "number", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .label
        return imageView
    }()
    
    private let orderNo: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.text = "Actor Name"
        return label
    }()
    
    private let actorSymbol: UIImageView = {
        let config = UIImage.SymbolConfiguration(pointSize: 17, weight: .regular, scale: .small)
        let image = UIImage(systemName: "person.text.rectangle", withConfiguration: config)
        let imageView = UIImageView(image: image)
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    
    private let actorName: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.text = "Item Name"
        label.textColor = .secondaryLabel
        return label
    }()
    
    private let dateSymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 13, weight: .regular, scale: .medium)
        let image = UIImage(systemName: "calendar", withConfiguration: config)
        let imageView = UIImageView(image: image)
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    
    private let date: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        label.text = "Company Name"
        label.textColor = .secondaryLabel
        return label
    }()
    
    private let amount: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.text = "1000"
        return label
    }()
    
    private let receivedImage: UIImageView = {
        let config = UIImage.SymbolConfiguration(pointSize: 13, weight: .regular, scale: .small)
        let imageView = UIImageView(frame: CGRect())
        imageView.image = UIImage(systemName: "tag", withConfiguration: config)
        return imageView
    }()
    
    private let received: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        label.text = "Received"
        label.textColor = .secondaryLabel
        return label
    }()
    
    override func setupCell() {
        
        contentView.addSubview(orderSymbol)
        orderSymbol.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(orderNo)
        orderNo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            orderSymbol.centerYAnchor.constraint(equalTo: orderNo.centerYAnchor),
            orderSymbol.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            
            orderNo.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 8),
            orderNo.leadingAnchor.constraint(equalTo: orderSymbol.trailingAnchor, constant: 4),
            orderNo.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
        ])
        
        orderSymbol.setContentHuggingPriority(.init(251), for: .horizontal)
        contentView.addSubview(actorSymbol)
        actorSymbol.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(actorName)
        actorName.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(amount)
        amount.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            actorSymbol.centerYAnchor.constraint(equalTo: actorName.centerYAnchor),
            actorSymbol.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            actorName.topAnchor.constraint(equalTo: orderNo.bottomAnchor, constant: 8),
            actorName.leadingAnchor.constraint(equalTo: actorSymbol.trailingAnchor, constant: 4),
            amount.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            amount.leadingAnchor.constraint(equalTo: actorName.trailingAnchor, constant: 8),
            amount.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
        ])
        actorSymbol.setContentHuggingPriority(.init(251), for: .horizontal)
        contentView.addSubview(dateSymbol)
        dateSymbol.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(date)
        date.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(receivedImage)
        receivedImage.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(received)
        received.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dateSymbol.centerYAnchor.constraint(equalTo: date.centerYAnchor),
            dateSymbol.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            date.topAnchor.constraint(equalTo: actorName.bottomAnchor, constant: 8),
            date.leadingAnchor.constraint(equalTo: dateSymbol.trailingAnchor, constant: 4),
            date.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            receivedImage.centerYAnchor.constraint(equalTo: received.centerYAnchor),
            receivedImage.leadingAnchor.constraint(equalTo: received.trailingAnchor, constant: 4),
            receivedImage.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            received.centerYAnchor.constraint(equalTo: date.centerYAnchor),
            received.leadingAnchor.constraint(greaterThanOrEqualTo: date.trailingAnchor, constant: 8),
            received.trailingAnchor.constraint(equalTo: receivedImage.leadingAnchor, constant: -4),
        ])
        receivedImage.setContentHuggingPriority(.init(251), for: .horizontal)
        dateSymbol.setContentHuggingPriority(.init(251), for: .horizontal)
    }
    
    func setValuesFor(orderNo: String?, actorName: String?, amount: String?, date: String?) {
        self.orderNo.text = orderNo
        self.actorName.text = actorName
        self.amount.text = amount
        self.date.text = date
    }
    
    func setReceivedStatus(_ status: Bool) {
        if status {
            receivedImage.image = {
                let image = UIImage(systemName: "circle.fill")?.withTintColor(.systemBlue)
                return image
            }()
            received.text = "Received"
        } else {
            receivedImage.image = {
                let image = UIImage(systemName: "circle.righthalf.filled")?.withTintColor(.systemBlue)
                return image
            }()
            received.text = "Not Received"
        }
    }
    
    func setForSales() {
        let config = UIImage.SymbolConfiguration(pointSize: 17, weight: .regular, scale: .small)
        actorSymbol.image = UIImage(systemName: "person", withConfiguration: config)
        receivedImage.isHidden = true
        received.isHidden = true
    }
}
