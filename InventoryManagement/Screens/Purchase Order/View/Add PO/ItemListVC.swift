//
//  ItemListVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit
import CoreData

class ItemListVC: UIViewController {
    
    lazy private var tableView: UITableView = {
        var tableView = UITableView()
        tableView.register(ItemTableViewCell.self, forCellReuseIdentifier: ItemTableViewCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    var backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy var createItemButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Add Item"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(createItemPressed), for: .touchUpInside)
        return button
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private let noItemViewImage = UIImage(named: "EmptyBox")?.withTintColor(.secondaryLabel)
    
    private let emptyItemText = "Sorry, No Items in Inventory!"
    
    private let CDM = CoreDataManager.shared
    
    var itemListDisplayModals: [ItemListDisplayModal]?
    
    weak var delegate: EntityListDelegate?
    
    var indexPath: IndexPath?
    
    var selected: UUID?
    
    var hideOutOfStock: Bool
    
    // MARK: - Init
    
    init(hideZeroStock: Bool) {
        self.hideOutOfStock = hideZeroStock
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        
        setupConfig()
        fetchDisplayModal()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: ItemKey.reloadItem, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupConfig() {
        tableViewConfig()
        navigationConfig()
        backgroundViewConfig()
    }
    
    private func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(createItemButton)
        createItemButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            backgroundView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            backgroundView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            backgroundView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            createItemButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createItemButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
        ])
    }
    
    private func navigationConfig() {
        navigationItem.title = "Items"
        navigationController?.navigationBar.prefersLargeTitles = true
        if !hideOutOfStock {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createItemPressed))
        }
    }
    
    private func backgroundViewConfig() {
        backgroundView.isHidden = true
        createItemButton.isHidden = true
    }
    
    // MARK: - Functions
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            if (self?.hideOutOfStock) ?? false {
                if let itemCoreDataList = self?.CDM.fetchItem(with: ["itemID", "itemName", "itemQuantity", "itemImage"], check: [NSPredicate(format: "itemQuantity > 0"), NSPredicate(format: "isActive == true")], sortBy: [NSSortDescriptor(key: "itemName", ascending: true)]) {
                    self?.itemListDisplayModals = itemCoreDataList.map({
                        item in
                        ItemListDisplayModal(itemID: item.itemID, itemName: item.itemName, itemQuantity: item.itemQuantity, itemImage: item.itemImage, quantityDetial: item.quantityDetail, priceDetail: item.priceDetail)
                    })
                }
            } else {
                if let itemCoreDataList = self?.CDM.fetchItem(with: ["itemID", "itemName", "itemQuantity", "itemImage"], check: [NSPredicate(format: "isActive == true")], sortBy: [NSSortDescriptor(key: "itemName", ascending: true)]) {
                    self?.itemListDisplayModals = itemCoreDataList.map({
                        item in
                        ItemListDisplayModal(itemID: item.itemID, itemName: item.itemName, itemQuantity: item.itemQuantity, itemImage: item.itemImage, quantityDetial: item.quantityDetail, priceDetail: item.priceDetail)
                    })
                }
            }
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.selectPrevious()
            }
        }
    }
    
    private func selectPrevious() {
        if let id = selected {
            if let row = selectedRow(id: id) {
                let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0))
                cell?.accessoryType = .checkmark
            }
        }
    }
    
    private func selectedRow(id: UUID) -> Int? {
        guard let itemList = itemListDisplayModals else { return nil}
        for i in 0..<(itemList.count) {
            if id == itemList[i].itemID {
                return i
            }
        }
        return nil
    }
    
    // MARK: - Objc Functions
    
    @objc
    private func createItemPressed() {
        present(UINavigationController(rootViewController: AddItemVC()), animated: true)
    }
    
    @objc
    private func reloadModal() {
        fetchDisplayModal()
    }
    
}

extension ItemListVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if itemListDisplayModals?.count == 0 || itemListDisplayModals == nil {
            if !hideOutOfStock {
                createItemButton.isHidden = false
            }
            backgroundView.isHidden = false
            return 0
        } else {
            createItemButton.isHidden = true
            backgroundView.isHidden = true
            return itemListDisplayModals?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let item = itemListDisplayModals?[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier, for: indexPath) as! ItemTableViewCell
            
            cell.setViewModal(
                itemName: item.itemName ?? "Unknown Item",
                itemPrice: currencyFormatter.string(from: (item.priceDetail?[.sellprice] ?? 0)  as NSNumber)!,
                itemQuantity: "\(item.quantityDetial?[.available] ?? 0)"
            )
            cell.setImage(from: item.itemImage)
            
            return cell
        }
        return UITableViewCell()
    }
    
}

extension ItemListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = itemListDisplayModals?[indexPath.row].itemID else {
            return
        }
        
        delegate?.selectedItem(id: id, at: self.indexPath!)
        navigationController?.popViewController(animated: true)
    }
    
}
