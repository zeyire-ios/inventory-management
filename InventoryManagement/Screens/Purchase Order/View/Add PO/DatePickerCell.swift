//
//  DataPickerCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit

protocol DatePickerDelegate: AnyObject {
    func dateSelected(date: Date)
}

class DatePickerCell: BaseTableViewCell {

    static let identifier = "com.abilash.DatePickerCell"
    
    weak var datePickerDelegate: DatePickerDelegate?
    
    private let label: UILabel = {
        var label = UILabel()
        label.text = "Select Date"
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    lazy private var datePicker: UIDatePicker = {
        var picker = UIDatePicker(frame: CGRect())
        picker.datePickerMode = .date
        picker.preferredDatePickerStyle = .compact
        picker.minimumDate = Date()
        picker.maximumDate = Date() + (7 * 24 * 60 * 60)
        picker.addTarget(self, action: #selector(datePicked), for: .valueChanged)
        return picker
    }()
    
    override func setupCell() {
        contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(datePicker)
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            label.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.35),
            label.centerYAnchor.constraint(equalTo: datePicker.centerYAnchor),
            
            datePicker.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            datePicker.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 8),
            datePicker.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -8),
            datePicker.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
        ])
    }
    
    func setLabel(_ label: String? ) {
        self.label.text = label
    }
    
    @objc
    private func datePicked(_ sender: UIDatePicker) {
        datePickerDelegate?.dateSelected(date: sender.date)
    }
    
}
