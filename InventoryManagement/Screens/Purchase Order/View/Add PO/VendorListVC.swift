//
//  VendorListVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit
import CoreData

protocol EntityListDelegate: AnyObject {
    func selectedVendor(id: UUID, at indexPath: IndexPath)
    
    func selectedItem(id: UUID, at indexPath: IndexPath)
}

extension EntityListDelegate {
    func selectedVendor(id: UUID, at indexPath: IndexPath) {
        
    }
    
    func selectedItem(id: UUID, at indexPath: IndexPath) {
        
    }
}

class VendorListVC: UIViewController {
    
    lazy private var tableView: UITableView = {
        var tableView = UITableView()
        tableView.register(ActorListCell.self, forCellReuseIdentifier: ActorListCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.noDataImage.image = UIImage(named: "Group")?.withTintColor(.secondaryLabel)
        noDataView.titleLabel.text = "No Vendors are Added!"
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy private  var createVendorButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Add Vendor"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(createVendorPressed), for: .touchUpInside)
        return button
    }()
    
    private let CDM = CoreDataManager.shared
    
    private var vendorListDisplayModals: [VendorListDisplayModal]?
    
    weak var delegate: EntityListDelegate?
    
    var indexPath: IndexPath?
    
    var selected: UUID?
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        setupConfig()
        fetchDisplayModal()
        
        addNotificationObservers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupConfig() {
        tableViewConfig()
        navigationConfig()
        backgroundViewConfig()
    }
    
    private func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(createVendorButton)
        createVendorButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: tableView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            
            backgroundView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            backgroundView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            backgroundView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            createVendorButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createVendorButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
        ])
    }
    
    private func navigationConfig() {
        navigationItem.title = "Vendors"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createVendorPressed))
    }
    
    private func backgroundViewConfig() {
        backgroundView.isHidden = true
        createVendorButton.isHidden = true
    }
    
    // MARK: - Functions
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            let vendorCoreDataList = self?.CDM.fetchVendor(with: ["vendorID", "displayName", "companyName", "profileImage"], sortBy: [NSSortDescriptor(key: "displayName", ascending: true)])
            self?.vendorListDisplayModals = vendorCoreDataList?.map {
                vendor in
                VendorListDisplayModal(vendorId: vendor.vendorID, displayName: vendor.displayName, companyName: vendor.companyName, profileImage: vendor.profileImage)
            }
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.selectPrevious()
            }
        }
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: VendorKey.reloadVendor, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: VendorKey.deleteVendor, object: nil)
    }
    
    private func selectPrevious() {
        if let id = selected {
            if let row = selectedRow(id: id) {
                let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0))
                cell?.accessoryType = .checkmark
            }
        }
    }
    
    private func selectedRow(id: UUID) -> Int? {
        guard let vendorList = vendorListDisplayModals else { return nil}
        for i in 0..<(vendorList.count) {
            if id == vendorList[i].vendorId {
                return i
            }
        }
        return nil
    }
    
    @objc
    private func createVendorPressed() {
        present(UINavigationController(rootViewController: AddVendorVC()), animated: true)
    }
    
    @objc
    private func reloadModal() {
        fetchDisplayModal()
    }
    
}

extension VendorListVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if vendorListDisplayModals?.count == 0 || vendorListDisplayModals == nil {
            backgroundView.isHidden = false
            createVendorButton.isHidden = false
            return 0
        } else {
            backgroundViewConfig()
            return vendorListDisplayModals?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let vendor = vendorListDisplayModals?[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorListCell.identifier, for: indexPath) as! ActorListCell
            
            cell.setNamesFor(
                actor: vendor.displayName ?? "Unknown Vendor",
                company: vendor.companyName ?? "Unknown Company"
            )
            
            cell.setImage(imageName: vendor.profileImage, for: .VendorImage)
           
            return cell
        }
        return UITableViewCell()
    }
}

extension VendorListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = vendorListDisplayModals?[indexPath.row].vendorId else {
            return
        }
        
        delegate?.selectedVendor(id: id, at: self.indexPath!)
        navigationController?.popViewController(animated: true)
    }
    
}
