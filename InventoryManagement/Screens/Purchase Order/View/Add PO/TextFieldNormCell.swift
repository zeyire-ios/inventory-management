//
//  TextFieldNormCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit
import AVFoundation

protocol CheckWarningDelegate: AnyObject {
    func checkWarning(for cell: TextFieldNormCell)
}

class TextFieldNormCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.TextFieldNormCell"
    
    private let mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 4
        return stackView
    }()
    
    private let fieldView = UIView()
    
    private let label: UILabel = {
        var label = UILabel()
        label.text = "Enter Data"
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.font = .preferredFont(forTextStyle: .body)
        return textField
    }()
    
    private let warningView: UIView = {
        let view = UIView()
        return view
    }()
    
    private let warningLabel: UILabel = {
        var label = UILabel()
        label.font = .preferredFont(forTextStyle: .caption1)
        label.textColor = .secondaryLabel
        return label
    }()
    
    weak var textFieldSaveData: TextFieldSaveData?
    
    weak var showWarningDelegate: CheckWarningDelegate?
    
    var placeholder: OrderFormCellType?
    
    override func setupCell() {
        fieldView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        fieldView.addSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: fieldView.leadingAnchor),
            label.widthAnchor.constraint(equalTo: fieldView.widthAnchor, multiplier: 0.35),
            label.centerYAnchor.constraint(equalTo: textField.centerYAnchor),
            label.trailingAnchor.constraint(equalTo: textField.leadingAnchor, constant: -8),
            
            textField.topAnchor.constraint(equalTo: fieldView.topAnchor),
            textField.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 8),
            textField.bottomAnchor.constraint(equalTo: fieldView.bottomAnchor),
            textField.trailingAnchor.constraint(equalTo: fieldView.trailingAnchor, constant: -8),
            
        ])
        
        warningView.addSubview(warningLabel)
        warningLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            warningLabel.topAnchor.constraint(equalTo: warningView.topAnchor),
            warningLabel.leadingAnchor.constraint(equalTo: warningView.leadingAnchor),
            warningLabel.bottomAnchor.constraint(equalTo: warningView.bottomAnchor),
            warningLabel.trailingAnchor.constraint(equalTo: warningView.trailingAnchor),
        ])
        
        let mainStackBottom = mainStackView.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -12)
        mainStackBottom.priority = .init(999)
        contentView.addSubview(mainStackView)
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 12),
            mainStackView.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            mainStackBottom,
            mainStackView.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
        ])
        
        mainStackView.addArrangedSubview(fieldView)
        mainStackView.addArrangedSubview(warningView)
        
        textField.delegate = self
    }
    
    // MARK: - Functions
    
    func setLabel(_ label: String? ) {
        self.label.text = label
    }
    
    func setValueFor(textFieldValue: String?) {
        textField.text = textFieldValue
    }
    
    func setTextFieldProperties(keyboardType: UIKeyboardType, clearButtonMode: UITextField.ViewMode) {
        textField.keyboardType = keyboardType
        textField.clearButtonMode = clearButtonMode
    }
    
    func setWarningLabel(color: UIColor) {
        warningLabel.textColor = color
    }
    
    func makeTextFieldFirstResponder() {
        textField.becomeFirstResponder()
    }
    
    func getLabel() -> String? {
        return label.text
    }
    
    func getTextFieldValue() -> String? {
        return textField.text
    }
    
    func showInstruction(with message: String) {
        if warningLabel.textColor != .systemRed {
            warningLabel.text = message
        }
    }
    
    func showWarning(with message: String) {
        warningLabel.text = message
        warningLabel.textColor = .systemRed
    }
    
}

extension TextFieldNormCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        guard let placeholder = self.placeholder else { return false}
        textFieldSaveData?.updateModal(for: placeholder.rawValue, value: "")
        showWarningDelegate?.checkWarning(for: self)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let placeholder = self.placeholder else { return false }
        switch(placeholder) {
        case .itemQuantity:
            if let _ = (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)) {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    if updatedText.count > 10 {
                        return false
                    }
                    textFieldSaveData?.updateModal(for: placeholder.rawValue, value: updatedText)
                    return true
                } else { return false }
            } else if string == "" {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    textFieldSaveData?.updateModal(for: placeholder.rawValue, value: updatedText)
                    return true
                } else {return false}
            } else {
                return false
            }
        case .itemCost:
            if let _ = (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)) {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    if updatedText.count > 13 {
                        return false
                    }
                    textFieldSaveData?.updateModal(for: placeholder.rawValue, value: updatedText)
                    return true
                } else { return false }
            } else if string == "" || string == "." {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    textFieldSaveData?.updateModal(for: placeholder.rawValue, value: updatedText)
                    return true
                } else {return false}
            } else {
                return false
            }
        case .orderDiscount:
            if let _ = (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)) {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    if Double(updatedText) ?? 0 > 100.0 {
                        return false
                    }
                    textFieldSaveData?.updateModal(for: placeholder.rawValue, value: updatedText)
                    return true
                } else { return false }
            } else if string == "" || string == "." {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    textFieldSaveData?.updateModal(for: placeholder.rawValue, value: updatedText)
                    return true
                } else {return false}
            } else {
                return false
            }
        default:
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, let placeholder = self.placeholder else { return }
        textFieldSaveData?.updateModal(for: placeholder.rawValue, value: text)
        showWarningDelegate?.checkWarning(for: self)
    }
    
}
