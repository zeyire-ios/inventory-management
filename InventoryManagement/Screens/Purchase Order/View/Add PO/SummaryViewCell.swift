//
//  SummaryView.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 15/09/22.
//

import UIKit

class SummaryViewCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.SummaryCell"
    
    private let titleLabel: UILabel = {
        var label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    private let subtotalLabel: UILabel = {
        var label = UILabel()
        label.text = "Subtotal"
        label.textColor = .lightGray
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let subtotalValue: UILabel = {
        var label = UILabel()
        label.textColor = .systemGray
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let discountLabel: UILabel = {
        var label = UILabel()
        label.text = "Discount"
        label.textColor = .lightGray
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let discountValue: UILabel = {
        var label = UILabel()
        label.textColor = .systemGray
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private var separatorView1 = UIView(frame: CGRect())
    private var separatorView2 = UIView(frame: CGRect())
    
    
    private let totalCostLabel: UILabel = {
        var label = UILabel()
        label.text = "Total Cost"
        label.textColor = .lightGray
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    private let totalCostValue: UILabel = {
        var label = UILabel()
        label.textColor = .systemGray
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    private let netQuantityLabel: UILabel = {
        var label = UILabel()
        label.text = "New Net Quantity"
        label.textColor = .lightGray
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    private let netQuantityValue: UILabel = {
        var label = UILabel()
        label.textColor = .systemGray
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    // MARK: - Init
    
    override func setupCell() {
        contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        ])
        contentView.addSubview(subtotalLabel)
        subtotalLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(subtotalValue)
        subtotalValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            subtotalLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            subtotalLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            subtotalValue.centerYAnchor.constraint(equalTo: subtotalLabel.centerYAnchor),
            subtotalValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        ])
        contentView.addSubview(discountLabel)
        discountLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(discountValue)
        discountValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            discountLabel.topAnchor.constraint(equalTo: subtotalLabel.bottomAnchor, constant: 8),
            discountLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            discountValue.centerYAnchor.constraint(equalTo: discountLabel.centerYAnchor),
            discountValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        ])
        separatorView1.backgroundColor = .systemGray4
        contentView.addSubview(separatorView1)
        separatorView1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            separatorView1.topAnchor.constraint(equalTo: discountLabel.bottomAnchor, constant: 4),
            separatorView1.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            separatorView1.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            separatorView1.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.005)
        ])
        contentView.addSubview(totalCostLabel)
        totalCostLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(totalCostValue)
        totalCostValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            totalCostLabel.topAnchor.constraint(equalTo: discountLabel.bottomAnchor, constant: 8),
            totalCostLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            totalCostValue.centerYAnchor.constraint(equalTo: totalCostLabel.centerYAnchor),
            totalCostValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        ])
        
        contentView.addSubview(netQuantityLabel)
        netQuantityLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(netQuantityValue)
        netQuantityValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            netQuantityLabel.topAnchor.constraint(equalTo: totalCostLabel.bottomAnchor, constant: 8),
            netQuantityLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            netQuantityLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            netQuantityValue.centerYAnchor.constraint(equalTo: netQuantityLabel.centerYAnchor),
            netQuantityValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        ])
    }
    
    // MARK: - Functions
    
    func setValueFor(discount: String?) {
        discountValue.text = discount
    }
    
    func setValueFor(subtotal: String?, total: String?) {
        subtotalValue.text = subtotal
        totalCostValue.text = total
    }
    
    func setValueFor(netQuantity: String?) {
        netQuantityValue.text = netQuantity
    }
}
