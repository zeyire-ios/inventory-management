//
//  AddPOVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit
import CoreData
import CoreMedia

enum OrderFormCellType: String {
    case vendorID = "Vendor"
    case customerID = "Customer"
    case itemID = "Item"
    case itemCost = "Item Cost"
    case itemQuantity = "Item Quantity"
    case orderDiscount = "Discount"
    case purchaseDate = "Purchase Date"
    case purchaseAmount = "Purcahse Amount"
    case isReceived
    case orderSummary
}

struct POFormList {
    
    let forms: [OrderFormCellType] = [.vendorID,
                                      .itemID,
                                      .itemQuantity,
                                      .itemCost,
                                      .orderDiscount,
                                      .purchaseDate,
                                      .orderSummary,]
}

final class AddPOVC: UIViewController {
    
    private let mainStack: UIStackView = {
        var stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    lazy private var tableView: UITableView = {
        var tableView = UITableView()
        tableView.register(SelectionCell.self, forCellReuseIdentifier: SelectionCell.identifier)
        tableView.register(DatePickerCell.self, forCellReuseIdentifier: DatePickerCell.identifier)
        tableView.register(TextFieldNormCell.self, forCellReuseIdentifier: TextFieldNormCell.identifier)
        tableView.register(SummaryViewCell.self, forCellReuseIdentifier: SummaryViewCell.identifier)
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private let orderForm = POFormList().forms
    
    private var poModal = PODetailModal()
    
    private var itemLimit = 500
    
    private var summaryIndex: IndexPath?
    
    private var doneButton: Bool = false {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = doneButton
        }
    }
    
    private var itemNetQuantity: Int {
        get {
            Int(poModal.item?.itemQuantity ?? 0) + (poModal.itemQuantity ?? 0)
        }
    }
    
    private var allCheck: Bool {
        get {
            if let _ = poModal.item, let _ = poModal.vendor, let _ = poModal.itemQuantity, let _ = poModal.itemCost {
                return true
            } else {
                return false
            }
        }
    }
    
    private let CDM = CoreDataManager.shared
    
    // MARK: - Init
    
    init(item: Item? = nil, vendor: Vendor? = nil, itemQuantity: Int? = nil, itemCost: Double? = nil, discount: Double? = nil) {
        self.poModal.item = item
        self.poModal.vendor = vendor
        self.poModal.itemQuantity = itemQuantity
        self.poModal.itemCost = itemCost
        self.poModal.discount = discount
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        isModalInPresentation = true
        navigationController?.presentationController?.delegate = self
        
        setupConfig()
        
        scrollViewAboveKeyboard()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if mainStack.axis == .vertical && UIDevice.current.orientation.isLandscape {
            mainStack.axis = .horizontal
            mainStack.alignment = .fill
            mainStack.distribution = .fillEqually
            mainStack.addArrangedSubview(tableView)
        } else if mainStack.axis == .horizontal && UIDevice.current.orientation.isPortrait {
            mainStack.axis = .vertical
            mainStack.alignment = .fill
            mainStack.distribution = .fill
            mainStack.addArrangedSubview(tableView)
      }
    }
    
    deinit {
        
    }
    
    private func setupConfig() {
        mainStackConfig()
        navigationConfig()
        updateSummary()
    }
    
    private func mainStackConfig() {
        view.addSubview(mainStack)
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainStack.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mainStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let size = UIScreen.main.bounds
        if size.width < size.height {
            mainStack.axis = .vertical
            mainStack.alignment = .fill
            mainStack.addArrangedSubview(tableView)
        } else {
            mainStack.axis = .horizontal
            mainStack.alignment = .fill
            mainStack.distribution = .fillEqually
            mainStack.addArrangedSubview(tableView)
        }
    }
    
    private func navigationConfig() {
        navigationItem.title = "Add Purchase Order"
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(_:)))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    // MARK: - Functions
    
    private func saveData(with modal: PODetailModal) {
        guard let vendor = modal.vendor, let item = modal.item, let cost = modal.itemCost, let quantity = modal.itemQuantity else { return }
        let order = PurchaseOrder(context: CDM.viewContext)
        order.purchaseNo = POGenerator.getPO()
        print(order.purchaseNo)
        order.purchaseID = UUID()
        order.purchaseDate = modal.purchaseDate ?? Date()
        order.purchaseItemCost = cost
        order.purchaseQuantity = Int64(quantity)
        order.discount = Double(modal.discount ?? 0)
        let netCost = (cost * Double(quantity))
        order.purchaseTotalCost = netCost - (netCost * (order.discount / 100))
        order.isOrderReceived = false
        order.item = item
        order.vendor = vendor
        let record = ItemRecord(context: CDM.viewContext)
        order.itemRecord = record
        DispatchQueue.global().async {
            [weak self] in
            do {
                guard let quantity = self?.itemNetQuantity else { return }
                item.itemQuantity = Int64(quantity)
                record.item = item
                record.purchaseOrder = order
                record.recordDate = order.purchaseDate
                record.netQuantity = Int64(quantity)
                
                try self?.CDM.viewContext.save()
                NotificationCenter.default.post(name: POKey.reloadPO, object: nil)
                NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
            } catch let error {
                print("\n\nAddPOVC: -> Saving Data:\n\(error)\n\n")
            }
        }
    }
    
    private func updateSummary() {
        if allCheck {
            doneButton = true
        } else {
            doneButton = false
        }
        if let index = summaryIndex {
            tableView.reconfigureRows(at: [index])
        }
    }
    
    // This function makes the view move above the keyboard
    private func scrollViewAboveKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // MARK: - Objc Functions
    
    @objc
    private func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @objc
    private func doneButtonPressed(_ sender: UIBarButtonItem) {
        saveData(with: poModal)
        dismiss(animated: true)
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        tableView.scrollIndicatorInsets = tableView.contentInset
        
    }
    
}

extension AddPOVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderForm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let placeholder = orderForm[indexPath.row]
        switch (placeholder) {
            
        case .vendorID:
            let cell = tableView.dequeueReusableCell(withIdentifier: SelectionCell.identifier, for: indexPath) as! SelectionCell
            cell.setValueFor(label: "Select Vendor", value: poModal.vendor?.displayName)
            cell.accessoryType = .disclosureIndicator
            return cell
        case .itemID:
            let cell = tableView.dequeueReusableCell(withIdentifier: SelectionCell.identifier, for: indexPath) as! SelectionCell
            cell.setValueFor(label: "Select Item", value: poModal.item?.itemName)
            cell.accessoryType = .disclosureIndicator
            return cell
        case .itemQuantity:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldNormCell.identifier, for: indexPath) as! TextFieldNormCell
            cell.setLabel("Item Quantity")
            if let text = poModal.itemQuantity {
                cell.setValueFor(textFieldValue: String(text))
            }
            cell.setTextFieldProperties(keyboardType: .numberPad, clearButtonMode: .whileEditing)
            cell.placeholder = placeholder
            cell.showInstruction(with: "Maximum limit 500")
            cell.textFieldSaveData = self
            cell.showWarningDelegate = self
            return cell
        case .itemCost:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldNormCell.identifier, for: indexPath) as! TextFieldNormCell
            cell.setLabel("Item Cost")
            if let text = poModal.itemCost {
                cell.setValueFor(textFieldValue: String(text))
            }
            cell.setTextFieldProperties(keyboardType: .decimalPad, clearButtonMode: .whileEditing)
            if let costPrice = poModal.item?.priceDetail[.costprice] {
                cell.showInstruction(with: "Lastest price \(currencyFormatter.string(from: costPrice as NSNumber) ?? "")")
            }
            cell.placeholder = placeholder
            cell.textFieldSaveData = self
            return cell
        case .orderDiscount:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldNormCell.identifier, for: indexPath) as! TextFieldNormCell
            cell.setLabel("Discount")
            if let text = poModal.discount {
                cell.setValueFor(textFieldValue: String(text))
            }
            cell.setTextFieldProperties(keyboardType: .decimalPad, clearButtonMode: .whileEditing)
            cell.showInstruction(with: "(Optional)")
            cell.placeholder = placeholder
            cell.textFieldSaveData = self
            return cell
        case .purchaseDate:
            let cell = tableView.dequeueReusableCell(withIdentifier: DatePickerCell.identifier, for: indexPath) as! DatePickerCell
            cell.setLabel("Purchase Date")
            cell.datePickerDelegate = self
            cell.selectionStyle = .none
            return cell
        case .orderSummary:
            let cell = tableView.dequeueReusableCell(withIdentifier: SummaryViewCell.identifier, for: indexPath) as! SummaryViewCell
            cell.setValueFor(discount: "\(poModal.discount ?? 0)%")
            let subtotal = Double(poModal.itemQuantity ?? 0) * (poModal.itemCost ?? 0.0)
            let total = subtotal - (subtotal * (poModal.discount ?? 0)/100)
            if let subtotalCurrency = currencyFormatter.string(from: subtotal as NSNumber), let totalCurrency = currencyFormatter.string(from: total as NSNumber), subtotalCurrency.count < 20 {
                cell.setValueFor(
                    subtotal: subtotalCurrency,
                    total: totalCurrency
                )
            } else {
                cell.setValueFor(
                    subtotal: "-",
                    total: "-"
                )
            }
            cell.setValueFor(netQuantity: String(itemNetQuantity))
            self.summaryIndex = indexPath
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension AddPOVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeholder = orderForm[indexPath.row]
        
        switch(placeholder) {
        case .vendorID:
            let vc = VendorListVC()
            vc.delegate = self
            vc.indexPath = indexPath
            vc.selected = poModal.vendor?.vendorID
            navigationController?.pushViewController(vc, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        case .itemID:
            let vc = ItemListVC(hideZeroStock: false)
            vc.delegate = self
            vc.indexPath = indexPath
            vc.selected = poModal.item?.itemID
            navigationController?.pushViewController(vc, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        case .itemQuantity, .itemCost, .orderDiscount:
            let cell = tableView.cellForRow(at: indexPath) as! TextFieldNormCell
            cell.makeTextFieldFirstResponder()
            tableView.deselectRow(at: indexPath, animated: true)
        default:
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

extension AddPOVC: UISheetPresentationControllerDelegate {
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alert = UIAlertController(title: "", message: "Are you sure, you want to discard the changes?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Discard Changes", style: .destructive){
            action in
            self.dismiss(animated: true)
        })
        alert.addAction(UIAlertAction(title: "Keep Editing", style: .cancel))
        
        // For showing action sheet in iPad, we have to make it presented as popoverController.
        if let popoverController = alert.popoverPresentationController {
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true)
    }
}

extension AddPOVC: EntityListDelegate {
    
    func selectedVendor(id: UUID, at indexPath: IndexPath) {
        poModal.vendor = CDM.fetchVendor(for: id, with: [])?.first
        tableView.reconfigureRows(at: [indexPath])
        updateSummary()
    }
    
    func selectedItem(id: UUID, at indexPath: IndexPath) {
        poModal.item = CDM.fetchItem(for: id, with: [])?.first
        let nextIndex = IndexPath(row: indexPath.row + 1, section: indexPath.section)
        let costIndex = IndexPath(row: indexPath.row + 2, section: indexPath.section)
        if let cell = tableView.cellForRow(at: nextIndex) as? TextFieldNormCell {
            cell.showWarningDelegate?.checkWarning(for: cell)
        }
        tableView.reconfigureRows(at: [indexPath, nextIndex, costIndex])
        updateSummary()
    }
}

extension AddPOVC: TextFieldSaveData {
    
    func updateModal(for type: String, value: String) {
        switch( OrderFormCellType.init(rawValue: type)) {
        case.itemQuantity:
            poModal.itemQuantity = Int(value)
        case .itemCost:
            poModal.itemCost = Double(value)
        case .orderDiscount:
            poModal.discount = Double(value)
        default:
            return 
        }
        updateSummary()
    }
    
}

extension AddPOVC: DatePickerDelegate {
    func dateSelected(date: Date) {
        poModal.purchaseDate = date
    }
}

extension AddPOVC: CheckWarningDelegate {
    func checkWarning(for cell: TextFieldNormCell) {
        if let index = tableView.indexPath(for: cell) {
            if cell.getLabel() == "Item Quantity" {
            #warning("Should this be itemQuantity or Available Items")
                if let filledText = cell.getTextFieldValue(), filledText != "" {
                    if let _ = poModal.item {
                        if (poModal.itemQuantity ?? itemLimit+1) > itemLimit {
                            cell.showWarning(with: "Maximum allowed limit is \(itemLimit)")
                            tableView.reconfigureRows(at: [index])
                            updateModal(for: cell.placeholder!.rawValue, value: "")
                        } else {
                            cell.setWarningLabel(color: .secondaryLabel)
                            tableView.reconfigureRows(at: [index])
                        }
                    } else {
                        cell.showWarning(with: "Select an Item")
                        tableView.reconfigureRows(at: [index])
                    }
                } else {
                    cell.setWarningLabel(color: .secondaryLabel)
                    tableView.reconfigureRows(at: [index])
                }
            }
        }
    }
}
