//
//  SelectionCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit

class SelectionCell: BaseTableViewCell {

    static let identifier = "com.abilash.SelectionCell"
    
    private let label: UILabel = {
        var label = UILabel()
        label.text = "Vendor Name"
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let subLabel: UILabel = {
        var label = UILabel()
        label.text = "Vendor Name"
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let separatorView: UIView = {
        var view = UIView()
        view.backgroundColor = .separator
        view.isHidden = true
        return view
    }()
    
    override func setupCell() {
        contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(subLabel)
        subLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separatorView)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 12),
            label.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            label.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -12),
            label.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.35),
            
            subLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 12),
            subLabel.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 8),
            subLabel.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -12),
            subLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -8),
            
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            separatorView.heightAnchor.constraint(equalToConstant: 0.33),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
        ])
    }
    
    func setValueFor(label: String?, value: String?) {
        self.label.text = label
        self.subLabel.text = value
    }
    
}
