//
//  ItemQuantityCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 10/10/22.
//

import UIKit

class ItemQuantityCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.ItemQuantityCell"
    
    private let imageHeight:CGFloat = 50
    
    private let itemImageView: UIImageView = {
        var imageView = UIImageView(image: UIImage(systemName: "photo"))
        imageView.tintColor = .quaternaryLabel
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let itemName: UILabel = {
        var label = UILabel()
        label.text = "Item Name"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let denomination: UILabel = {
        var label = UILabel()
        label.text = " x "
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private let subtotal: UILabel = {
        var label = UILabel()
        label.text = "0.0"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let separatorView: UIView = {
        var view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    override func setupCell() {
        setupConfig()
    }
    
    private func setupConfig() {
        contentView.addSubview(itemImageView)
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        itemImageView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(itemName)
        itemName.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(denomination)
        denomination.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(subtotal)
        addSubview(separatorView)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        subtotal.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            itemImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            itemImageView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -8),
            itemImageView.trailingAnchor.constraint(equalTo: itemName.leadingAnchor, constant: -8),
            itemImageView.heightAnchor.constraint(equalToConstant: imageHeight),
            itemImageView.widthAnchor.constraint(equalToConstant: imageHeight),
            
            activityIndicator.topAnchor.constraint(equalTo: itemImageView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: itemImageView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: itemImageView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            itemName.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            itemName.leadingAnchor.constraint(equalTo: itemImageView.trailingAnchor, constant: 8),
            itemName.trailingAnchor.constraint(lessThanOrEqualTo: subtotal.leadingAnchor, constant: -8),
            
            denomination.topAnchor.constraint(equalTo: itemName.bottomAnchor, constant: 8),
            denomination.leadingAnchor.constraint(equalTo: itemName.leadingAnchor),
            denomination.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -8),
            denomination.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -16),
            
            subtotal.topAnchor.constraint(equalTo: contentView.topAnchor),
            subtotal.leadingAnchor.constraint(greaterThanOrEqualTo: itemName.trailingAnchor, constant: 8),
            subtotal.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            subtotal.centerYAnchor.constraint(equalTo: itemName.centerYAnchor),
            
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            separatorView.heightAnchor.constraint(equalToConstant: 0.33),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    func setValueFor(itemName: String?) {
        self.itemName.text = itemName
    }
    
    func setValueFor(denomination: String?, subtotal: String?) {
        self.denomination.text = denomination
        self.subtotal.text = subtotal
    }
    
    func hideSeparator() {
        separatorView.isHidden = true
    }
    
    func setImage(imageName: UUID?) {
        DispatchQueue.main.async {
            [weak self] in
            self?.activityIndicator.startAnimating()
        }
        DispatchQueue.global().async {
            [weak self] in
            guard let imageName = imageName?.uuidString else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "photo")
                    self?.itemImageView.contentMode = .scaleAspectFit
                }
                return
            }
            let imagePath = URLStore.itemImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            do {
                let data = try Data(contentsOf: imagePath)
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(data: data)
                    self?.itemImageView.contentMode = .scaleAspectFill
                }
            } catch let error {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "photo")
                    self?.itemImageView.contentMode = .scaleAspectFit
                }
                print("\n\nItemQuantityCell -> Loading Image to ImageView:\n\(error)\n\n")
            }
        }
    }
    
}
