//
//  PODetailVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 10/10/22.
//

import UIKit

class PODetailVC: UIViewController {
    
    private lazy var orderMenu: UIMenu = {
        return UIMenu(title: "", image: nil, identifier: nil, options: .destructive, children: [topMenu, bottomMenu])
    }()
    
    private lazy var topMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        if !(self?.purchaseModal.isOrderReceived ?? false) {
            actions.append(UIAction(title: "Mark as Received",
                                    image: UIImage(systemName: "circle.fill"),
                                    handler: {
                [weak self] action in
                
                let alert = UIAlertController(title: "Are you sure?", message: "Once you mark an Order as Received, you can't undo it. Are you sure you received it.", preferredStyle: .actionSheet)
                
                alert.addAction(UIAlertAction(title: "Proceed", style: .destructive) {
                    action in
                    self?.markOrderReceived()
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                
                // For showing action sheet in iPad, we have to make it presented as popoverController.
                if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = self?.view
                    popoverController.sourceRect = CGRect(x: (self?.view.bounds.midX)!, y: (self?.view.bounds.midY)!, width: 0, height: 0)
                    popoverController.permittedArrowDirections = []
                    //                    popoverController.barButtonItem = navigationItem.leftBarButtonItem
                }
                
                self?.present(alert, animated: true)
            }))
        } else {
            actions.append(UIAction(title: "Mark as Received",
                                    image: UIImage(systemName: "circle.righthalf.filled"),
                                    attributes: .disabled,
                                    handler: {
                [weak self] action in
            }))
        }
        
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    private lazy var bottomMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        actions.append(UIAction(title: "Clone Order",
                                image: {
            let image = UIImage(systemName: "doc.on.doc")
            return image
        }(),
                                handler: {
            [weak self] action in
            self?.cloneOrder()
        }))
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    private lazy var mainTableView: UITableView = {
        var tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.register(AmountCell.self, forCellReuseIdentifier: AmountCell.identifier)
        tableView.register(ActorListCell.self, forCellReuseIdentifier: ActorListCell.identifier)
        tableView.register(ItemQuantityCell.self, forCellReuseIdentifier: ItemQuantityCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.register(TotalDisplayCell.self, forCellReuseIdentifier: TotalDisplayCell.identifier)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .long
        return formatter
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private var purchaseModal = PODetailModal()
    
    private let CDM = CoreDataManager.shared
    
    private var id: UUID
    
    // MARK: - Init
    
    init(id: UUID) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemGroupedBackground
        // Do any additional setup after loading the view.
        
        setupConfig()
        fetchDisplayModal()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupConfig() {
        tableViewConfig()
        navigationConfig()
    }
    
    private func tableViewConfig() {
        view.addSubview(mainTableView)
        mainTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            mainTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    private func navigationConfig() {
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.backgroundColor = .systemGroupedBackground
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(title: "", image: UIImage(systemName: "ellipsis.circle"), primaryAction: nil, menu: orderMenu),
        ]
    }
    
    // MARK: - Functions
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            if let purchaseOrder = self?.CDM.fetchPurchaseOrder(forOrder: self?.id, with: [], sortBy: [])?.first {
                self?.purchaseModal = PODetailModal( purchaseNo: purchaseOrder.purchaseNo, purchaseDate: purchaseOrder.purchaseDate, isOrderReceived: purchaseOrder.isOrderReceived, itemCost: purchaseOrder.purchaseItemCost, itemQuantity: Int(purchaseOrder.purchaseQuantity), discount: purchaseOrder.discount, totalCost: purchaseOrder.purchaseTotalCost, item: purchaseOrder.item, vendor: purchaseOrder.vendor)
            }
            DispatchQueue.main.async {
                self?.updateVC()
            }
        }
    }
    
    private func updateVC() {
        mainTableView.reloadData()
    }
    
    private func markOrderReceived() {
        DispatchQueue.global().async {
            [weak self] in
            let order = self?.CDM.fetchPurchaseOrder(forOrder: self?.id)?.first
            order?.isOrderReceived = true
            do {
                try self?.CDM.viewContext.save()
                self?.purchaseModal.isOrderReceived = true
            } catch let error {
                print("\n\nPODetailVC: -> Saving Data:\n\(error)\n\n")
            }
            NotificationCenter.default.post(name: POKey.reloadPO, object: nil)
            DispatchQueue.main.async {
                self?.mainTableView.reconfigureRows(at: [IndexPath(row: 0, section: 0)])
            }
        }
    }
    
    private func cloneOrder() {
        let vc = UINavigationController(rootViewController: AddPOVC(item: purchaseModal.item, vendor: purchaseModal.vendor, itemQuantity: purchaseModal.itemQuantity, itemCost: purchaseModal.itemCost, discount: purchaseModal.discount))
        vc.modalPresentationStyle = .formSheet
        present(vc, animated: true)
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: VendorKey.reloadVendor, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: ItemKey.deleteItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: VendorKey.deleteVendor, object: nil)
    }
    
    // MARK: - Objc Functions
    
    @objc
    private func reloadModal() {
        fetchDisplayModal()
    }
    
    @objc
    private func dismissView() {
        if traitCollection.userInterfaceIdiom == .phone {
            splitViewController?.hide(.secondary)
        } else if traitCollection.userInterfaceIdiom == .pad {
            let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
            vc?.setViewControllers([SplitViewController.blankVC], animated: true)
        }
    }
    
}

extension PODetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: AmountCell.identifier, for: indexPath) as! AmountCell
            cell.setValueFor(
                orderNo: purchaseModal.purchaseNo,
                dateLabel: dateFormatter.string(from: purchaseModal.purchaseDate ?? Date(timeIntervalSince1970: 0))
            )
            if purchaseModal.isOrderReceived! {
                cell.checkReceived()
            }
            cell.hideSeparator()
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorListCell.identifier, for: indexPath) as! ActorListCell
            if let vendor = purchaseModal.vendor {

                cell.setNamesFor(
                    actor: vendor.displayName,
                    company: vendor.companyName
                )
                cell.setColor(.displayName, color: .label)
                cell.accessoryType = .detailButton
                cell.setImage(imageName: vendor.profileImage, for: .VendorImage)
                
            } else {
                cell.setNamesFor(
                    actor: "Unkown Vendor",
                    company: "Company Not Found!"
                )
                cell.setColor(.displayName, color: .secondaryLabel)
                cell.accessoryType = .none
            }
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemQuantityCell.identifier, for: indexPath) as! ItemQuantityCell
            cell.setValueFor(itemName: purchaseModal.item?.itemName)
            
            let itemCost = currencyFormatter.string(from: (purchaseModal.itemCost ?? 0) as NSNumber)
            let subtotal = (purchaseModal.itemCost ?? 0) * Double(purchaseModal.itemQuantity ?? 0)
            
            cell.setValueFor(
                denomination: "\(purchaseModal.itemQuantity ?? 0) x \(itemCost ?? "0")",
                subtotal: currencyFormatter.string(from: subtotal as NSNumber)
            )
            
            cell.hideSeparator()
            cell.setImage(imageName: purchaseModal.item?.itemImage)
            
            cell.accessoryType = .detailButton
                
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: TotalDisplayCell.identifier, for: indexPath) as! TotalDisplayCell
            let subtotal = (purchaseModal.itemCost ?? 0) * Double(purchaseModal.itemQuantity ?? 0)
            
            cell.setValueFor(
                subtotal: currencyFormatter.string(from: subtotal as NSNumber),
                discount: "\(purchaseModal.discount ?? 0)%",
                total: currencyFormatter.string(from: (purchaseModal.totalCost ?? 0) as NSNumber)
            )
            cell.hideLastSeparator()
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension PODetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if indexPath.section == 1 {
            navigationController?.pushViewController(VendorDetailVC(id: purchaseModal.vendor?.vendorID ?? UUID()), animated: true)
        }
        if indexPath.section == 2 {
            navigationController?.pushViewController(ItemDetailVC(id: purchaseModal.item!.itemID), animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch (section) {
        case 1:
            return "Vendor"
        case 2:
            return "Item"
        case 3:
            return ""
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (section) {
        case 0:
            return 0.0
        case 1:
            return UITableView.automaticDimension
        case 2:
            return UITableView.automaticDimension
        case 3:
            return UITableView.automaticDimension
        default:
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
}
