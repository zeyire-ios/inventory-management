//
//  AmountCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 10/10/22.
//

import UIKit

class AmountCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.AmountCell"
    
    private let orderSymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration.init(pointSize: 24, weight: .bold, scale: .medium)
        var image = UIImage(systemName: "number", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .label
        return imageView
    }()
    
    private let orderNo: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 28)
        return label
    }()
    
    private let orderView = UIView()
    
    private let dateSymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 17, weight: .regular, scale: .medium)
        var image = UIImage(systemName: "calendar", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    
    private let dateLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private let dateView = UIView()
    
    private let receivedSymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration.init(pointSize: 17, weight: .regular, scale: .medium)
        var image = UIImage(systemName: "circle.righthalf.filled")
        var imageView = UIImageView(image: image)
        return imageView
    }()
    
    private let receivedLabel: UILabel = {
        var label = UILabel()
        label.text = "Not Received"
        label.textColor = .label
        return label
    }()
    
    private let receivedView: UIView = UIView()
    
    private let separatorView: UIView = {
        var view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    override func setupCell() {
        orderViewConfig()
        receivedViewConfig()
        dateViewConfig()
        configuration()
    }
    
    private func orderViewConfig() {
        orderView.addSubview(orderSymbol)
        orderSymbol.translatesAutoresizingMaskIntoConstraints = false
        orderView.addSubview(orderNo)
        orderNo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            orderSymbol.leadingAnchor.constraint(equalTo: orderView.leadingAnchor),
            orderSymbol.centerYAnchor.constraint(equalTo: orderNo.centerYAnchor),
            orderSymbol.trailingAnchor.constraint(equalTo: orderNo.leadingAnchor, constant: -4),
            
            orderNo.topAnchor.constraint(equalTo: orderView.topAnchor),
            orderNo.leadingAnchor.constraint(equalTo: orderSymbol.trailingAnchor, constant: 4),
            orderNo.trailingAnchor.constraint(lessThanOrEqualTo: orderView.trailingAnchor),
            orderNo.bottomAnchor.constraint(equalTo: orderView.bottomAnchor),
        ])
        orderSymbol.setContentHuggingPriority(.init(251), for: .horizontal)
    }
    
    private func dateViewConfig() {
        dateView.addSubview(dateSymbol)
        dateSymbol.translatesAutoresizingMaskIntoConstraints = false
        dateView.addSubview(dateLabel)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dateSymbol.leadingAnchor.constraint(equalTo: dateView.leadingAnchor),
            dateSymbol.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor),
            dateSymbol.trailingAnchor.constraint(equalTo: dateLabel.leadingAnchor, constant: -4),
            
            dateLabel.topAnchor.constraint(equalTo: dateView.topAnchor),
            dateLabel.leadingAnchor.constraint(equalTo: dateSymbol.trailingAnchor, constant: 4),
            dateLabel.trailingAnchor.constraint(equalTo: dateView.trailingAnchor),
            dateLabel.bottomAnchor.constraint(equalTo: dateView.bottomAnchor),
        ])
    }
    
    private func receivedViewConfig() {
        receivedView.addSubview(receivedSymbol)
        receivedSymbol.translatesAutoresizingMaskIntoConstraints = false
        receivedView.addSubview(receivedLabel)
        receivedLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            receivedLabel.topAnchor.constraint(equalTo: receivedView.topAnchor),
            receivedLabel.leadingAnchor.constraint(equalTo: receivedView.leadingAnchor),
            receivedLabel.bottomAnchor.constraint(equalTo: receivedView.bottomAnchor),
            receivedLabel.trailingAnchor.constraint(equalTo: receivedSymbol.leadingAnchor, constant: -4),
            
            receivedSymbol.leadingAnchor.constraint(equalTo: receivedLabel.trailingAnchor, constant: 4),
            receivedSymbol.centerYAnchor.constraint(equalTo: receivedLabel.centerYAnchor),
            receivedSymbol.trailingAnchor.constraint(equalTo: receivedView.trailingAnchor),
        ])
    }
    
    private func configuration() {
        contentView.addSubview(orderView)
        orderView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(receivedView)
        receivedView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(dateView)
        dateView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separatorView)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            orderView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            orderView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            orderView.trailingAnchor.constraint(lessThanOrEqualTo: receivedView.leadingAnchor, constant: -8),
            orderView.bottomAnchor.constraint(equalTo: dateView.topAnchor, constant: -8),
            
            receivedView.leadingAnchor.constraint(greaterThanOrEqualTo: orderView.trailingAnchor, constant: 8),
            receivedView.centerYAnchor.constraint(equalTo: orderView.centerYAnchor),
            receivedView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            dateView.topAnchor.constraint(equalTo: orderView.bottomAnchor, constant: 8),
            dateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            dateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            dateView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -16),
            
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            separatorView.heightAnchor.constraint(equalToConstant: 0.33),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
        ])
        receivedView.setContentHuggingPriority(.init(251), for: .horizontal)
    }
    
    func setValueFor(orderNo: String?, dateLabel: String?) {
        self.orderNo.text = orderNo
        self.dateLabel.text = dateLabel
    }
    
    func hideSeparator() {
        separatorView.isHidden = true
    }
    
    func hideReceivedView() {
        receivedView.isHidden = true
    }
    
    func checkReceived() {
        let config = UIImage.SymbolConfiguration.init(pointSize: 17, weight: .regular, scale: .medium)
        receivedSymbol.image = UIImage(systemName: "circle.fill", withConfiguration: config)
        receivedLabel.text = "Received"
    }
    
}
