//
//  TotalDisplayCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 10/10/22.
//

import UIKit

class TotalDisplayCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.TotalDisplayCell"
    
    private let subtotalLabel: UILabel = {
        var label = UILabel()
        label.text = "Subtotal"
        label.textAlignment = .right
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let subtotalValue: UILabel = {
        var label = UILabel()
        label.text = ""
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let separatorView_1: UIView = {
        var view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    private let discountLabel: UILabel = {
        var label = UILabel()
        label.text = "Discount"
        label.textAlignment = .right
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let discountValue: UILabel = {
        var label = UILabel()
        label.text = ""
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private let separatorView_2: UIView = {
        var view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    private let totalLabel: UILabel = {
        var label = UILabel()
        label.text = "Total"
        label.textAlignment = .right
        label.font = .preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private let totalValue: UILabel = {
        var label = UILabel()
        label.text = ""
        label.font = .preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private let separator_3: UIView = {
        var view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    override func setupCell() {
        subtotalConstraints()
        discountConstraints()
        totalConstraints()
    }
    
    private func subtotalConstraints() {
        contentView.addSubview(subtotalLabel)
        subtotalLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(subtotalValue)
        subtotalValue.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(separatorView_1)
        separatorView_1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            subtotalLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            subtotalLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            
            subtotalValue.centerYAnchor.constraint(equalTo: subtotalLabel.centerYAnchor),
            subtotalValue.leadingAnchor.constraint(equalTo: subtotalLabel.trailingAnchor, constant: 32),
            subtotalValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            separatorView_1.topAnchor.constraint(equalTo: subtotalLabel.bottomAnchor, constant: 12),
            separatorView_1.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            separatorView_1.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            separatorView_1.heightAnchor.constraint(equalToConstant: 0.33),
        ])
    }
    
    private func discountConstraints() {
        contentView.addSubview(discountLabel)
        discountLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(discountValue)
        discountValue.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(separatorView_2)
        separatorView_2.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            discountLabel.topAnchor.constraint(equalTo: separatorView_1.bottomAnchor, constant: 12),
            discountLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            discountLabel.trailingAnchor.constraint(equalTo: subtotalLabel.trailingAnchor),
            discountLabel.bottomAnchor.constraint(equalTo: separatorView_2.topAnchor, constant: -12),
            
            discountValue.centerYAnchor.constraint(equalTo: discountLabel.centerYAnchor),
            discountValue.leadingAnchor.constraint(equalTo: subtotalValue.leadingAnchor),
            discountValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            separatorView_2.topAnchor.constraint(equalTo: discountLabel.bottomAnchor, constant: 12),
            separatorView_2.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            separatorView_2.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            separatorView_2.heightAnchor.constraint(equalToConstant: 0.33),
        ])
    }
    
    private func totalConstraints() {
        contentView.addSubview(totalLabel)
        totalLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(totalValue)
        totalValue.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separator_3)
        separator_3.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            totalLabel.topAnchor.constraint(equalTo: separatorView_2.bottomAnchor, constant: 12),
            totalLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            totalLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12),
            totalLabel.trailingAnchor.constraint(equalTo: subtotalLabel.trailingAnchor),
            
            totalValue.centerYAnchor.constraint(equalTo: totalLabel.centerYAnchor),
            totalValue.leadingAnchor.constraint(equalTo: subtotalValue.leadingAnchor),
            totalValue.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            separator_3.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            separator_3.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            separator_3.heightAnchor.constraint(equalToConstant: 0.33),
            separator_3.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    func setValueFor(subtotal: String?, discount: String?, total: String?) {
        subtotalValue.text = subtotal
        discountValue.text = discount
        totalValue.text = total
    }
    
    func hideLastSeparator() {
        separator_3.isHidden = true
    }
    
}
