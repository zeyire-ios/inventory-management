//
//  PurchaseOrder+CoreDataProperties.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/10/22.
//
//

import Foundation
import CoreData


extension PurchaseOrder {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PurchaseOrder> {
        return NSFetchRequest<PurchaseOrder>(entityName: "PurchaseOrder")
    }

    @NSManaged public var isOrderReceived: Bool
    @NSManaged public var purchaseDate: Date
    @NSManaged public var purchaseID: UUID
    @NSManaged public var purchaseItemCost: Double
    @NSManaged public var purchaseQuantity: Int64
    @NSManaged public var purchaseTotalCost: Double
    @NSManaged public var purchaseNo: String
    @NSManaged public var item: Item
    @NSManaged public var itemRecord: ItemRecord
    @NSManaged public var vendor: Vendor
    @NSManaged public var discount: Double

}

extension PurchaseOrder : Identifiable {

}
