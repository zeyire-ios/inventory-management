//
//  POSort.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 11/10/22.
//

import Foundation

struct POSort: SortOptions {
    
    func setSortBy(sortBy: Any) {
        POSort._setSortBy(sortBy: sortBy as! SortBy)
    }
    
    func setSortOrder(ascending: Bool) {
        POSort._setSortOrder(ascending: ascending)
    }
    
    func setFilterBy(filterBy: Any) {
        POSort._setFilter(by: filterBy as! FilterBy)
    }
    
    static let sortBy: [SortBy] = [.purchaseNo, .vendor, .date]
    
    static let filterBy: [FilterBy] = [.all, .received, .notReceived, .recentlyCreated, .newThisWeek, .newLastWeek]
    
    enum SortBy: String {
        case purchaseNo = "Purchase Order"
        case vendor = "Vendor Name"
        case date = "Purchase Date"
    }
    
    enum FilterBy: String {
        case all = "All Orders"
        case received = "Received Orders"
        case notReceived = "Not Received Orders"
        case newThisWeek = "New This Week"
        case newLastWeek = "New Last Week"
        case recentlyCreated = "Recently Created Order"
    }
    
    static func checkSortOrder() -> Bool {
        if UserDefaults.standard.value(forKey: "POAscending") == nil {
            UserDefaults.standard.set(true, forKey: "POAscending")
        }
        return UserDefaults.standard.bool(forKey: "POAscending")
    }
    
    static func _setSortOrder(ascending: Bool) {
        UserDefaults.standard.set(ascending, forKey: "POAscending")
    }
    
    static func checkSortBy() -> SortBy {
        if UserDefaults.standard.value(forKey: "POSortBy") == nil {
            UserDefaults.standard.set(SortBy.purchaseNo.rawValue, forKey: "POSortBy")
        }
        return SortBy.init(rawValue: (UserDefaults.standard.string(forKey: "POSortBy"))!)!
        
    }
    
    static func _setSortBy(sortBy: SortBy) {
        UserDefaults.standard.set(sortBy.rawValue, forKey: "POSortBy")
    }
    
    static func checkFilter() -> FilterBy {
        if UserDefaults.standard.value(forKey: "POFilter") == nil {
            UserDefaults.standard.set(FilterBy.all.rawValue, forKey: "POFilter")
        }
        return FilterBy.init(rawValue: UserDefaults.standard.string(forKey: "POFilter")!)!
    }
    
    static func _setFilter(by: FilterBy) {
        UserDefaults.standard.set(by.rawValue, forKey: "POFilter")
    }
    
}
