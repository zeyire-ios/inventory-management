//
//  ItemViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/09/22.
//

import UIKit
import CoreData

final class ItemViewController: UIViewController {

    lazy private var tableView: UITableView = {
        var tableView = UITableView(frame: CGRect(), style: .insetGrouped)
        tableView.register(ItemTableViewCell.self, forCellReuseIdentifier: ItemTableViewCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy private var createItemButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Add Item"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(addButtonPressed), for: .touchUpInside)
        return button
    }()
    
    lazy private var searchBar = UISearchController()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private var timer = Timer()
    
    private let noFilterViewImage = UIImage(named: "FilterCross")?.withTintColor(.secondaryLabel)
    
    private let noSearchViewImage = UIImage(named: "SearchProfile")?.withTintColor(.secondaryLabel)
    
    private let noItemViewImage = UIImage(named: "EmptyBox")?.withTintColor(.secondaryLabel)
    
    private let searchResultText = "Sorry, No Search Results!"
    
    private let emptyInventoryText = "Sorry, No Items in Inventory!"
    
    private var itemListDisplayModals: [ItemListDisplayModal]?
    
    private var sortBy: ItemSort.SortBy {
        ItemSort.checkSortBy()
    }
    
    private var filter: ItemSort.FilterBy {
        ItemSort.checkFilter()
    }
    
    private var ascending: Bool {
        ItemSort.checkSortOrder()
    }
    
    weak var splitDelegate: SplitControllerDelegate?
    
    private let CDM = CoreDataManager.shared
    
    private var firstTime: Bool = true {
        didSet {
            selectFirstRowInSplitView()
        }
    }
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGroupedBackground
        
        setupConfig()
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
        
        addNotificationObservers()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
//        print("deinit")
    }
    
    private func setupConfig() {
        
        tableViewConfig()
        splitViewConfig()
        navigationConfig()
        backgroundViewConfig()
        
        createRefreshControl()
    }
    
    private func tableViewConfig() {
        view.addSubview(tableView)
        tableView.addSubview(activityIndicator)
        view.addSubview(backgroundView)
        view.addSubview(createItemButton)
        
        tableView.setAnchor(.top, as: .equalTo, to: view.topAnchor)
        tableView.setAnchor(.leading, as: .equalTo, to: view.leadingAnchor)
        tableView.setAnchor(.bottom, as: .equalTo, to: view.bottomAnchor)
        tableView.setAnchor(.trailing, as: .equalTo, to: view.trailingAnchor)
        
        activityIndicator.setAnchor(.top, as: .equalTo, to: tableView.topAnchor)
        activityIndicator.setAnchor(.bottom, to: tableView.bottomAnchor)
        activityIndicator.setAnchor(.leading, to: tableView.leadingAnchor)
        activityIndicator.setAnchor(.trailing, to: tableView.trailingAnchor)
        
        backgroundView.setAnchor(.centerY, to: view.centerYAnchor)
        backgroundView.setAnchor(.top, as: .greaterThanEqualTo, to: tableView.topAnchor)
        backgroundView.setAnchor(.bottom, as: .lessThanEqualTo, to: tableView.bottomAnchor)
        backgroundView.setAnchor(.leading, to: view.safeAreaLayoutGuide.leadingAnchor, padding: 16)
        backgroundView.setAnchor(.trailing, to: view.safeAreaLayoutGuide.trailingAnchor, padding: -16)
        
        createItemButton.setAnchor(.top, to: backgroundView.bottomAnchor)
        createItemButton.setAnchor(.centerX, to: backgroundView.centerXAnchor)
    }
    
    private func splitViewConfig() {
        
        splitViewController?.delegate = self
        splitViewController?.preferredPrimaryColumnWidthFraction = 0.4
        
    }
    
    private func navigationConfig() {
        navigationItem.title = "Inventory"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barTintColor = .systemGroupedBackground
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.searchController = self.searchBar
        navigationItem.searchController?.searchBar.placeholder = "Search Item"
        navigationItem.searchController?.searchBar.delegate = self
        navigationItem.searchController?.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed)), UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(sortButtonPressed))]
    }
    
    private func backgroundViewConfig() {
        backgroundView.isHidden = true
        createItemButton.isHidden = true
    }
    
    // MARK: - Functions
    
    /// Fetches data from the CoreData and copies it to the view modal
    /// - Parameters:
    ///   - searchTerm: A optional String that represents the search term for searching
    ///   - sortBy: A enum that represents by which factor sorting is performed
    ///   - ascending: A bool value that represents whether to sort Ascending or Descending
    ///   - filterBy: A enum that represents by which factor should we filter through the data.
    private func fetchToViewModal(searchFor searchTerm: String? = nil, sortBy: ItemSort.SortBy, ascending: Bool, filterBy: ItemSort.FilterBy) {
        DispatchQueue.main.async {
            [weak self] in
            self?.activityIndicator.startAnimating()
        }
        DispatchQueue.global().async {
            [weak self] in
            
            var predicates: [NSPredicate] = []
            let secondary: ItemSort.SortBy = (sortBy == .name) ? .quantity : .name
            
            if let searchTerm = searchTerm {
                predicates.append(NSPredicate(format: "itemName CONTAINS[c] %@", searchTerm))
            }
            
            if filterBy != .all {
                predicates.append((self?.convertToCoreDataPredicate(filter: filterBy))!)
            }
            
            let itemCoreDataList = self?.CDM.fetchItem(for: nil, with: ["itemID", "itemName", "itemQuantity", "lowStockLimit", "isActive", "itemImage"], check: predicates, sortBy: [NSSortDescriptor(key: self?.convertToCoreDataAttributeName(sortBy), ascending: ascending), NSSortDescriptor(key: self?.convertToCoreDataAttributeName(secondary), ascending: ascending)])
            
            self?.itemListDisplayModals = itemCoreDataList?.map({
                item in
                ItemListDisplayModal(itemID: item.itemID, itemName: item.itemName, itemQuantity: item.itemQuantity, isLowStock: item.isLowStock, isActive: item.isActive, itemImage: item.itemImage, quantityDetial: item.quantityDetail, priceDetail: item.priceDetail)
            })
            
            DispatchQueue.main.async {
                self?.showResultBackgroundFor(emptyList: itemCoreDataList?.isEmpty ?? true, withSearchTerm: searchTerm != nil, withFilter: filterBy != .all)
                self?.tableView.reloadData()
                if self?.firstTime ?? false {
                    self?.firstTime = false
                }
                self?.activityIndicator.stopAnimating()
            }
        }
    }
    
    /// Generates a background view when the database can't fetch any results based on few conditions
    /// - Parameters:
    ///   - emptyList: A bool value that represents the database fetched zero entities
    ///   - withSearchTerm: A bool value that represents if a search term is used when fetching data
    ///   - withFilter: A bool value that represents whether any filters are applied.
    private func showResultBackgroundFor(emptyList: Bool, withSearchTerm: Bool, withFilter: Bool) {
        switch (emptyList, withSearchTerm, withFilter) {
            
            // When Searching produces no results
            // All Search results are applied with no filters, hence this case
        case (true, true, false):
            createItemButton.isHidden = true
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noSearchViewImage
            backgroundView.titleLabel.text = searchResultText
            
            // When applying filters produces no results
        case (true, false, true):
            createItemButton.isHidden = true
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noFilterViewImage
            switch (filter) {
            case .active:
                backgroundView.titleLabel.text = "Sorry, No Active Items Found!"
            case .inactive:
                backgroundView.titleLabel.text = "Sorry, No Inactive Items Found!"
            case .lowStock:
                backgroundView.titleLabel.text = "Sorry, No Low Stock Items Found!"
            case .outOfStock:
                backgroundView.titleLabel.text = "Sorry, No Out of Stock Items Found!"
            default:
                break
            }
            
            // When there are no more Items in Inventory.
        case (true, false, false):
            createItemButton.isHidden = false
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noItemViewImage
            backgroundView.titleLabel.text = emptyInventoryText
            
        default:
            createItemButton.isHidden = true
            backgroundView.isHidden = true
        }
    }
    
    private func selectFirstRowInSplitView() {
        if traitCollection.userInterfaceIdiom == .pad {
            if let id = itemListDisplayModals?.first?.itemID {
                let vc = ItemDetailVC(id: id)
                splitDelegate?.itemSelected(present: vc)
            }
        }
    }
    
    private func convertToCoreDataAttributeName(_ attribute: ItemSort.SortBy) -> String {
        switch (attribute) {
        case .name:
            return "itemName"
        case .quantity:
            return "itemQuantity"
        }
    }
    
    private func convertToCoreDataPredicate(filter: ItemSort.FilterBy) -> NSPredicate {
        switch (filter) {
        case .all:
            return NSPredicate()
        case .active:
            return NSPredicate(format: "isActive = true")
        case .inactive:
            return NSPredicate(format: "isActive = false")
        case .lowStock:
            return NSPredicate(format: "(lowStockLimit > itemQuantity) AND (itemQuantity > 0)")
        case .outOfStock:
            return NSPredicate(format: "itemQuantity = 0")
        }
    }
    
    private func convertToCurrency(_ number: NSNumber ) -> String? {
        return currencyFormatter.string(from: number)
    }
    
    private func createRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func deleteRefreshContorl() {
        tableView.refreshControl = nil
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: ItemKey.reloadItem, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: ItemKey.deleteItem, object: nil)
    }
    
    // MARK: Objc Functions
    
    @objc
    private func dismissView() {
        
    }
    
    @objc
    private func pullToRefresh(_ sender: UIRefreshControl) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
        sender.endRefreshing()
    }
    
    @objc
    private func reloadModal(_ sender: NSNotification) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
    
    @objc
    private func addButtonPressed(_ sender: UIBarButtonItem) {
        present(UINavigationController(rootViewController: AddItemVC()), animated: true)
    }
    
    @objc
    private func sortButtonPressed(_ sender: UIBarButtonItem) {
        present(UINavigationController(rootViewController: SortViewController<ItemSort.SortBy, ItemSort.FilterBy>(sortModal: ItemSort(), sortBy: ItemSort.sortBy, sortedBy: sortBy, ascending: ascending, filterBy: ItemSort.filterBy, filter: filter, completion: {
            [weak self] in
            self?.fetchToViewModal(sortBy: (self?.sortBy) ?? .name, ascending: (self?.ascending) ?? true, filterBy: self?.filter ?? .all)
        })), animated: true)
    }
    
}

extension ItemViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if itemListDisplayModals?.count == 0 {
            tableView.isScrollEnabled = false
            return 0
        } else {
            // This makes sure that the background views are hidden when we have data to display.
            backgroundViewConfig()
            tableView.isScrollEnabled = true
            return itemListDisplayModals?.count ?? 0
        }
    }
    
    private func getWarningTypeFor(_ item: ItemListDisplayModal) -> ItemWarning {
        if !(item.isActive)! {
            return .inActive
        } else if (item.quantityDetial?[ .available ] ?? 0 ) == 0 {
            return .outOfStock
        } else if item.isLowStock! {
            return .lowStock
        } else {
            return .remove
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let item = itemListDisplayModals?[indexPath.section] {
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier, for: indexPath) as! ItemTableViewCell
            
            cell.setViewModal(
                itemName: item.itemName ?? "Unknown Item" ,
                itemPrice: convertToCurrency((item.priceDetail?[.sellprice])! as NSNumber)!,
                itemQuantity: "\(item.quantityDetial?[ .available ] ?? 0)"
            )
            cell.showWarning(type: getWarningTypeFor(item))
            cell.setImage(from: item.itemImage)
            
            return cell
        }
        return UITableViewCell()
    }
}

extension ItemViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ItemDetailVC(id: (itemListDisplayModals?[indexPath.section].itemID)!)
        vc.hidesBottomBarWhenPushed = true
        splitDelegate?.itemSelected(present: vc)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var stateAction = UIContextualAction()
        var trashAction = UIContextualAction()
        if let item = itemListDisplayModals?[indexPath.section] {
            
            stateAction = getStateAction(for: item.isActive!, id: item.itemID, indexPath: indexPath)
            stateAction.backgroundColor = .systemMint
            
            trashAction = getTrashAction(id: item.itemID, indexPath: indexPath)
            
            trashAction.backgroundColor = UIColor(red: 249/255.0, green: 102/255.0, blue: 102/255.0, alpha: 1)
            trashAction.image = UIImage(systemName: "trash")
        }
        return UISwipeActionsConfiguration(actions: [stateAction, trashAction])
    }
    
    private func getTrashAction(id: UUID?, indexPath: IndexPath) -> UIContextualAction {
        return UIContextualAction(style: .destructive, title: "Trash") {
            [weak self] (action, view, completionHandler) in
            let alert = UIAlertController(title: "", message: "Are you sure you want to delete this Item", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive){
                action in
                self?.handleDeleteAction(id: id, indexPath: indexPath)
                completionHandler(true)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            
            // For showing action sheet in iPad, we have to make it presented as popoverController.
            if let popoverController = alert.popoverPresentationController {

                let cell = self?.tableView.cellForRow(at: indexPath)
                if #available(iOS 16.0, *) {
                    popoverController.sourceItem = cell
                } else {
                    // Fallback on earlier versions
                    popoverController.sourceView = cell
                    popoverController.sourceRect = CGRect(x: (cell?.contentView.bounds.midX)!, y: (cell?.contentView.bounds.maxY)!, width: 0, height: 0)
                }
            }
            
            self?.present(alert, animated: true)
            completionHandler(false)
        }
    }
    
    private func getStateAction(for isActive: Bool, id: UUID?, indexPath: IndexPath) -> UIContextualAction {
        if !isActive {
            return UIContextualAction(style: .normal, title: "Mark as Active", handler: {
                [weak self] (action, view, completionHandler) in
                self?.handleActiveState(id: id, indexPath: indexPath)
                completionHandler(true)
            })
        } else {
            return UIContextualAction(style: .normal, title: "Mark as Inactive", handler: {
                [weak self] (action, view, completionHandler) in
                self?.handleInactiveState(id: id, indexPath: indexPath)
                completionHandler(true)
            })
        }
    }
    
    private func handleActiveState(id: UUID?, indexPath: IndexPath) {
        if let item = self.CDM.fetchItem(for: id, with: ["itemName"])?.first {
            item.isActive = true
            itemListDisplayModals?[indexPath.section].isActive = true
        }
        do {
            try self.CDM.viewContext.save()
        } catch let error {
            print("\n\nItemViewController -> Core Data Save failed\n\(error)\n\n")
        }
        if filter == .inactive {
            itemListDisplayModals?.remove(at: indexPath.section)
            tableView.deleteSections(IndexSet(integer: indexPath.section), with: .automatic)
            if traitCollection.userInterfaceIdiom == .pad {
                let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
                vc?.setViewControllers([SplitViewController.blankVC], animated: true)
            }
            if itemListDisplayModals?.isEmpty ?? false {
                backgroundView.isHidden = false
                backgroundView.noDataImage.image = noFilterViewImage
                backgroundView.titleLabel.text = "Sorry, No Inactive Items Found!"
                
            }
        } else {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    private func handleInactiveState(id: UUID?, indexPath: IndexPath) {
        if let item = self.CDM.fetchItem(for: id, with: ["itemName"])?.first {
            item.isActive = false
            itemListDisplayModals?[indexPath.section].isActive = false
        }
        do {
            try self.CDM.viewContext.save()
        } catch let error {
            print("\n\nItemViewController -> Core Data Save failed\n\(error)\n\n")
        }
        if filter == .active {
            itemListDisplayModals?.remove(at: indexPath.section)
            tableView.deleteSections(IndexSet(integer: indexPath.section), with: .automatic)
            if traitCollection.userInterfaceIdiom == .pad {
                let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
                vc?.setViewControllers([SplitViewController.blankVC], animated: true)
            }
            if itemListDisplayModals?.isEmpty ?? false {
                backgroundView.isHidden = false
                backgroundView.noDataImage.image = noFilterViewImage
                backgroundView.titleLabel.text = "Sorry, No Active Items Found!"
            }
        } else {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    private func handleDeleteAction(id: UUID?, indexPath: IndexPath) {
        if let item = self.CDM.fetchItem(for: id, with: ["itemName"])?.first {
            DispatchQueue.global().async {
                ImageHelper.deleteImage(item.itemImage, for: .ItemImage)
            }
            CDM.viewContext.delete(item)
            do {
                try CDM.viewContext.save()
            } catch {
                print("\n\nItemViewController: -> Saving to CoreData\n\(error)\n\n")
            }
            NotificationCenter.default.post(name: ItemKey.deleteItem, object: nil)
            NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
            itemListDisplayModals?.remove(at: indexPath.section)
            tableView.deleteSections(IndexSet(integer: indexPath.section), with: .automatic)
        }
    }
}

extension ItemViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }
    
}

extension ItemViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {
            [weak self] timer in
            if searchText.isEmpty {
                self?.fetchToViewModal( searchFor: "", sortBy: self?.sortBy ?? ItemSort.SortBy.name, ascending: self?.ascending ?? true, filterBy: .all)
            } else {
                self?.fetchToViewModal(searchFor: searchText, sortBy: self?.sortBy ?? ItemSort.SortBy.name, ascending: self?.ascending ?? true, filterBy: .all)
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        timer.invalidate()
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        timer.invalidate()
        fetchToViewModal(searchFor: searchBar.text, sortBy: sortBy, ascending: ascending, filterBy: .all)
    }
}

extension ItemViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        deleteRefreshContorl()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        createRefreshControl()
    }
    
}
