//
//  ItemTableViewCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/09/22.
//

import UIKit

final class ItemTableViewCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.ItemCell"
    
    private let itemImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "person.circle")
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .quaternaryLabel
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let itemDetailStack: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .equalSpacing
        stackView.spacing = 6
        return stackView
    }()
    
    private let itemTitle: UILabel = {
        var label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    private let itemPriceQuantityView = UIView()
    
    private let itemPrice: UILabel = {
        var label = UILabel()
        label.textColor = .secondaryLabel
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        return label
    }()
    
    private let trailingStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .trailing
        stackView.distribution = .equalSpacing
        stackView.spacing = 6
        return stackView
    }()
    
    private let quantityView = UIView()
    
    private let quantityImage: UIImageView = {
        var config = UIImage.SymbolConfiguration.init(pointSize: 22, weight: .medium, scale: .small)
        var imageView = UIImageView(image: (UIImage(systemName: "shippingbox", withConfiguration: config)))
        imageView.tintColor = .label
        return imageView
    }()
    
    private let itemQuantity: UILabel = {
        var label = UILabel()
        label.text = "100"
        label.font = UIFont.systemFont(ofSize: 22.0, weight: .medium)
        return label
    }()
    
    private let warningImage: UIImageView = {
        var config = UIImage.SymbolConfiguration.init(pointSize: 22, weight: .medium, scale: .small)
        var imageView = UIImageView(image: (UIImage(named: "lowLimit")))
        imageView.preferredSymbolConfiguration = config
        return imageView
    }()
    
    // MARK: - Init
    
    override func prepareForReuse() {
        itemImageView.image = nil
        itemImageView.tintColor = .quaternaryLabel
        itemTitle.text = nil
        itemTitle.textColor = .label
        itemPrice.text = nil
        itemPrice.textColor = .secondaryLabel
        itemQuantity.text = nil
        itemQuantity.textColor = .label
        quantityImage.image = nil
        quantityImage.tintColor = .label
    }
    
    deinit {
//        print("Deinit")
    }
    
    override func setupCell() {
        itemImageViewConfig()
        quantityViewConfig()
        itemDetailViewConfig()
    }
    
    private func itemImageViewConfig() {
        contentView.addSubview(itemImageView)
        itemImageView.addSubview(activityIndicator)
        
        itemImageView.setAnchor(.top, to: topAnchor, padding: 8)
        itemImageView.setAnchor(.bottom, to: bottomAnchor, padding: -8)
        itemImageView.setAnchor(.leading, to: leadingAnchor, padding: 8)
        itemImageView.setAnchor(.width, as: .equalTo, to: itemImageView.heightAnchor)
        
        itemImageView.setContentHuggingPriority(.init(751), for: .vertical)
        itemImageView.setContentCompressionResistancePriority(.init(760), for: .vertical)
        
        activityIndicator.setAnchor(.top, to: itemImageView.topAnchor)
        activityIndicator.setAnchor(.leading, to: itemImageView.leadingAnchor)
        activityIndicator.setAnchor(.bottom, to: itemImageView.bottomAnchor)
        activityIndicator.setAnchor(.trailing, to: itemImageView.trailingAnchor)
        
    }
    
    private func itemDetailViewConfig() {
        contentView.addSubview(itemDetailStack)
        contentView.addSubview(trailingStackView)
        
        itemDetailStack.setAnchor(.top, as: .greaterThanEqualTo, to: contentView.topAnchor, padding: 8)
        itemDetailStack.setAnchor(.leading, to: itemImageView.trailingAnchor, padding: 12)
        itemDetailStack.setAnchor(.centerY, to: contentView.centerYAnchor)
        itemDetailStack.setAnchor(.bottom, as: .lessThanEqualTo, to: contentView.bottomAnchor, padding: -8)
        itemDetailStack.setAnchor(.trailing, as: .lessThanEqualTo, to: trailingStackView.leadingAnchor, padding: -8)
        
        trailingStackView.setAnchor(.top, as: .greaterThanEqualTo, to: contentView.topAnchor, padding: 8)
        trailingStackView.setAnchor(.leading, to: itemDetailStack.trailingAnchor, padding: 8)
        trailingStackView.setAnchor(.centerY, to: contentView.centerYAnchor)
        trailingStackView.setAnchor(.bottom, as: .lessThanEqualTo, to: contentView.bottomAnchor, padding: -8)
        trailingStackView.setAnchor(.trailing, to: contentView.trailingAnchor, padding: -12)
        
        itemDetailStack.addArrangedSubview(itemTitle)
        itemDetailStack.addArrangedSubview(itemPrice)
        trailingStackView.addArrangedSubview(quantityView)
        itemDetailStack.setContentHuggingPriority(.init(270), for: .horizontal)
    }
    
    private func quantityViewConfig() {
        quantityView.addSubview(quantityImage)
        quantityView.addSubview(itemQuantity)
        
        quantityImage.setAnchor(.leading, to: quantityView.leadingAnchor, padding: 8)
        quantityImage.setAnchor(.centerY, to: itemQuantity.centerYAnchor)
        quantityImage.setAnchor(.trailing, to: itemQuantity.leadingAnchor, padding: -8)
        
        itemQuantity.setAnchor(.top, to: quantityView.topAnchor, padding: 8)
        itemQuantity.setAnchor(.leading, to: quantityImage.trailingAnchor, padding: 8)
        itemQuantity.setAnchor(.bottom, to: quantityView.bottomAnchor, padding: -8)
        itemQuantity.setAnchor(.trailing, to: quantityView.trailingAnchor, padding: -12)
        
    }
    
    // MARK: - Functions
    func showWarning(type: ItemWarning) {
        let config = UIImage.SymbolConfiguration.init(pointSize: 22, weight: .medium, scale: .small)
        switch (type) {
        case .inActive:
            let image = UIImage(systemName: "xmark.bin.fill", withConfiguration: config)
            quantityImage.image = image
            quantityImage.tintColor = .quaternaryLabel
            itemTitle.textColor = .quaternaryLabel
            itemPrice.textColor = .quaternaryLabel
            itemQuantity.textColor = .quaternaryLabel
        case .outOfStock:
            quantityImage.tintColor = UIColor(named: "PeachSet")
            quantityImage.image = UIImage(systemName: "shippingbox", withConfiguration: config)
            itemQuantity.textColor = UIColor(named: "PeachSet")
        case .lowStock:
            let image = UIImage(systemName: "exclamationmark.circle.fill", withConfiguration: config)
            quantityImage.image = image
            quantityImage.tintColor = .systemYellow
        case .remove:
            quantityImage.image = UIImage(systemName: "shippingbox", withConfiguration: config)
            quantityImage.tintColor = .label
            itemTitle.textColor = .label
            itemPrice.textColor = .secondaryLabel
            itemQuantity.textColor = .label
        }
    }
    
    func setViewModal(itemName: String, itemPrice: String, itemQuantity: String) {
        self.itemTitle.text = itemName
        self.itemPrice.text = itemPrice
        self.itemQuantity.text = itemQuantity
    }
    
    func setImage(from imageName: UUID?) {
        activityIndicator.startAnimating()
        DispatchQueue.global().async {
            [weak self] in
            guard let imageName = imageName?.uuidString else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "photo")
                    self?.itemImageView.contentMode = .scaleAspectFit
                }
                return
            }
            let imagePath = URLStore.itemImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            do {
                let data = try Data(contentsOf: imagePath)
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(data: data)
                    self?.itemImageView.contentMode = .scaleAspectFill
                }
            } catch let error {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "photo")
                    self?.itemImageView.contentMode = .scaleAspectFit
                }
                print("\n\nItemDetailVC -> Loading Image to ImageView:\n\(error)\n\n")
            }
        }
    }
    
}


