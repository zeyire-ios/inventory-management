//
//  SwitchCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/09/22.
//

import UIKit

protocol SwitchDelegate: AnyObject {
    
    func updatedSwitch(with value: Bool)
    
}

class SwitchCell: BaseTableViewCell {

    static let identifier = "com.abilash.SwitchCell"
    
    var requiredField: Bool?
    
    weak var switchDelegate: SwitchDelegate?
    
    var switchLabel: UILabel = {
        let label = UILabel()
        label.text = "Switch"
        return label
    }()

    lazy private var switchControl: UISwitch = {
        let switchControl = UISwitch()
        switchControl.preferredStyle = .sliding
        switchControl.isOn = false
        switchControl.onTintColor = .systemBlue
        switchControl.addTarget(self, action: #selector(switchValueChanged(_:)), for: .valueChanged)
        return switchControl
    }()
    
    deinit {
//        print("deinit")
    }
    
    override func setupCell() {
        contentView.addSubview(switchLabel)
        switchLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(switchControl)
        switchControl.translatesAutoresizingMaskIntoConstraints = false
        let scTop = switchControl.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 8)
        scTop.priority = UILayoutPriority(251)
        let scBottom = switchControl.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -8)
        scBottom.priority = UILayoutPriority(251)
        NSLayoutConstraint.activate([
            switchLabel.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 8),
            switchLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            switchLabel.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -8),
            switchControl.centerYAnchor.constraint(equalTo: switchLabel.centerYAnchor),
            switchControl.leadingAnchor.constraint(equalTo: switchLabel.trailingAnchor, constant: 8),
            switchControl.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -8),
            scTop,
            scBottom,
        ])
        
    }
    
    func setSwitch(on state: Bool) {
        switchControl.isOn = state
    }
    
    @objc
    func switchValueChanged(_ sender: UISwitch) {
        switchDelegate?.updatedSwitch(with: sender.isOn)
    }
}

