//
//  AddItemVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/09/22.
//

import UIKit
import Photos
import PhotosUI

final class AddItemVC: UIViewController {
    
    private let pickImage: UIImageView = {
        let imageConfig = UIImage.SymbolConfiguration(pointSize: 120, weight: .bold, scale: .medium)
        let image = UIImage(systemName: "person.circle.fill", withConfiguration: imageConfig)
        let imageView = UIImageView(image: image)
        imageView.tintColor = .systemGray3
        imageView.layer.borderWidth = 1.0
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.cornerRadius = 120 / 2
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let pickImageButton: UIButton = {
        let button = UIButton()
        button.setTitle("Add Photo", for: .normal)
        button.setTitleColor(UIColor.systemBlue, for: .normal)
        return button
    }()
    
    lazy private var tableView: UITableView = {
        let tableView = UITableView(frame: CGRect(), style: .insetGrouped)
        tableView.register(TextFieldCell.self, forCellReuseIdentifier: TextFieldCell.identifier)
        tableView.register(SwitchCell.self, forCellReuseIdentifier: SwitchCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.keyboardDismissMode = .interactive
        tableView.allowsSelection = true
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private var doneButton: Bool = false {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = doneButton
        }
    }
    
    private let itemFormList = ItemFormList()
    
    private var newItem = ItemDetailModal()
    
    private var selectedImage: UIImage?
    
    private let CDM = CoreDataManager.shared
    
    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        isModalInPresentation = true
        navigationController?.presentationController?.delegate = self
        
        setupConfig()
        scrollViewAboveKeyboard()
    }
    
    deinit {
//        print("Deinit")
    }
    
    private func setupConfig() {
        tableViewConfig()
        navigationConfig()
        imageViewConfig()
    }
    
    private func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pickImage)
        pickImage.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pickImageButton)
        pickImageButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pickImage.widthAnchor.constraint(lessThanOrEqualToConstant: 120),
            pickImage.heightAnchor.constraint(equalTo: pickImage.widthAnchor),
            pickImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pickImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            pickImageButton.centerXAnchor.constraint(equalTo: pickImage.centerXAnchor),
            pickImageButton.topAnchor.constraint(equalTo: pickImage.bottomAnchor, constant: 8),
            pickImageButton.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8),
            pickImageButton.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8),
            tableView.topAnchor.constraint(equalTo: pickImageButton.bottomAnchor, constant: 16),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
        pickImageButton.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        pickImageButton.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        tableView.dataSource = self
    }
    
    private func navigationConfig() {
        navigationItem.title = "Add Items"
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(_:)))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    private func imageViewConfig() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(activateImagePicker(_:)))
        pickImage.addGestureRecognizer(tap)
        pickImageButton.addTarget(self, action: #selector(activateImagePicker(_:)), for: .touchUpInside)
    }
    
    // MARK: - Functions
    private func saveData() {
        let item = Item(context: CDM.viewContext)
        item.itemID = UUID()
        item.itemName = newItem.itemName!
        item.itemQuantity = Int64(newItem.itemQuantity!)
        item.lowStockLimit = Int64(newItem.lowStockLimit!)
        item.isActive = newItem.isActive ?? true
        let itemRecord = ItemRecord(context: CDM.viewContext)
        itemRecord.purchaseOrder = nil
        itemRecord.item = item
        itemRecord.recordDate = Date.now
        itemRecord.netQuantity = item.itemQuantity
        itemRecord.salesOrder = nil
        do {
            if let image = selectedImage {
                item.itemImage = UUID()
                guard let imageName = item.itemImage?.uuidString else { fatalError() }
                ImageHelper.writeImage(image, for: .ItemImage, to: URLStore.itemImageURL, with: imageName)
            }
            try CDM.viewContext.save()
        } catch let error{
            print("\n\nSaving Data for new Item:\n\(error)\n\n")
        }
        NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
    }
//
//    private func hideKeyboardOnTap() {
//        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
//        view.addGestureRecognizer(tap)
//    }
//
    private func scrollViewAboveKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    private func validateModal() -> Bool {
        if let _ = newItem.itemName, let _ = newItem.itemQuantity, let _ = newItem.lowStockLimit {
            return true
        } else {
            return false
        }
    }
    
    // MARK: Objc Functions
    @objc
    private func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @objc
    private func doneButtonPressed(_ sender: UIBarButtonItem) {
        if validateModal() {
            DispatchQueue.global().async {
                [weak self] in
                self?.saveData()
            }
            dismiss(animated: true)
        }
    }
    
    // Using PhotoPicker from Photo Kit
    @objc
    private func activateImagePicker(_ sender: UITapGestureRecognizer) {
        var config = PHPickerConfiguration(photoLibrary: .shared())
        config.selectionLimit = 1
        config.filter = .images
        config.selection = .default
        let imagePicker = PHPickerViewController(configuration: config)
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    private func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        tableView.scrollIndicatorInsets = tableView.contentInset
        
    }
}

extension AddItemVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return itemFormList.forms.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemFormList.forms[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let placeholder = itemFormList.forms[indexPath.section][indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldCell.identifier, for: indexPath) as! TextFieldCell
        cell.setHeightForFeild(30)
        cell.selectionStyle = .none
        
        let requiredText = NSMutableAttributedString(string: "*", attributes: [.foregroundColor: UIColor.systemRed])
        requiredText.append(NSMutableAttributedString(string: placeholder.rawValue))
        
        cell.setTextfieldPlaceholder(attributedString: requiredText)
        cell.requiredField = true
        cell.textFieldSaveData = self
        cell.textFieldHelper = self
        
        switch (placeholder) {
            
        case .itemName:
            cell.setKeyboardType(.alphabet)
            if let text = newItem.itemName, !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                cell.setTextfieldText(text: text)
                cell.setPlaceholderLabel(text: placeholder.rawValue)
                cell.requiredSymbol.isHidden = false
            }
            return cell
        case .itemQuantity:
            cell.setKeyboardType(.numberPad)
            if let quantity = newItem.itemQuantity {
                let text = String(quantity)
                if !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeholder.rawValue)
                    cell.requiredSymbol.isHidden = false
                }
            }
            return cell
        case .lowStockLimit:
            cell.setKeyboardType(.numberPad)
            if let lowLimit = newItem.lowStockLimit {
                let text = String(lowLimit)
                if !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeholder.rawValue)
                    cell.requiredSymbol.isHidden = false
                }
            }
            return cell
        case .isActive:
            let cell = tableView.dequeueReusableCell(withIdentifier: SwitchCell.identifier, for: indexPath) as! SwitchCell
            
            cell.switchLabel.text = placeholder.rawValue
            cell.setSwitch(on: true)
            cell.switchDelegate = self
            
            return cell
            
        case .error:
            let cell = UITableViewCell()
            return cell
        }
    }
}

extension AddItemVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! TextFieldCell
        cell.makeTextfieldFirstResponder()
    }
    
}

extension AddItemVC: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        
        if newItem.itemName != nil || newItem.itemQuantity != nil || newItem.lowStockLimit != nil {
            
            let alert = UIAlertController(title: "", message: "Are you sure, you want to discard the changes?", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Discard Changes", style: .destructive){
                action in
                self.dismiss(animated: true)
            })
            alert.addAction(UIAlertAction(title: "Keep Editing", style: .cancel))
            
            // For showing action sheet in iPad, we have to make it presented as popoverController.
            if let popoverController = alert.popoverPresentationController {
                if #available(iOS 16.0, *) {
                    popoverController.sourceItem = navigationItem.leftBarButtonItem
                } else {
                    // Fallback on earlier versions
                    popoverController.barButtonItem = navigationItem.leftBarButtonItem
                }
            }
            
            self.present(alert, animated: true)
            
        } else {
            
            self.dismiss(animated: true)
            
        }
    }
}

extension AddItemVC: TextFieldSaveData {
    
    /// This Function is a part of a delegate to save the data whenever the textfield is interacted with.
    /// - Parameters:
    ///   - type: This is a raw value of enum ItemFormCellType, which determines from which textfield the value is passed from
    ///   - value: The actual value entered in the textField, this will be updated after some safety checks
    func updateModal(for type: String, value: String) {
        guard let type = ItemFormCellType(rawValue: type) else {
            return
        }
        switch(type) {
        case .itemName:
            
            newItem.itemName = !(value.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) ? value : nil
            
        case .itemQuantity:
            
            if let quantity = Int(value) {
                newItem.itemQuantity = quantity
            } else if !(value.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                newItem.itemQuantity = nil
            } else {
                newItem.itemQuantity = nil
                print("AddItemVC -> TextFieldSaveDataDeleage -> Attempting to Assign non int value to quantity")
            }
        case .lowStockLimit:
            if let limit = Int(value) {
                newItem.lowStockLimit = limit
            } else if !(value.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                newItem.lowStockLimit = nil
            } else {
                newItem.lowStockLimit = nil
                print("AddItemVC -> TextFieldSaveDataDeleage -> Attempting to Assign non int value to lowStockLimit")
            }
        default:
            return
        }
        guard let _ = newItem.itemName, let _ = newItem.itemQuantity, let _ = newItem.lowStockLimit else {
            doneButton = false
            return
        }
        doneButton = true
    }
    
}

extension AddItemVC: TextFieldHelper {
    
    /// This function makes the next textfield in the table view rows as first responder;
    /// - Parameter cell: the current cell will be passed as parameter for getting the next cell's index
    func assignNextResponder(from cell: TextFieldCell) {
        let indexPath = tableView.indexPath(for: cell)
        let nextIndexPath = itemFormList.getNextIndex(for: indexPath!)!
        let cell = tableView.cellForRow(at: nextIndexPath) as! TextFieldCell
        cell.makeTextfieldFirstResponder()
    }
    
    func getPlaceholder(for cell: TextFieldCell) -> String  {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return ItemFormCellType.error.rawValue
        }
        return itemFormList.forms[indexPath.section][indexPath.row].rawValue
    }
    
    func reconfigRow(for cell: TextFieldCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        tableView.reconfigureRows(at: [indexPath])
    }
    
}

extension AddItemVC: SwitchDelegate {
    
    func updatedSwitch(with value: Bool) {
        newItem.isActive = value
    }
    
}

extension AddItemVC: PHPickerViewControllerDelegate {
    
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        /// When cancel is pressed an empty array is passed.
        if results.isEmpty {
            DispatchQueue.main.async {
                picker.dismiss(animated: true)
            }
        }
        /// Otherwise we process this.
        results.first?.itemProvider.loadObject(ofClass: UIImage.self) {
            reading, error in
            guard let image = reading as? UIImage, error == nil else { return }
            DispatchQueue.main.async {
                [weak self] in
                self?.pickImage.image = image
                self?.pickImageButton.setTitle("Edit", for: .normal)
                self?.selectedImage = image
                /// Have to dismiss youself
                picker.dismiss(animated: true)
            }
        }
    }
}
