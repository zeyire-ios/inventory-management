//
//  ItemDetailView+Testing.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 23/09/22.
//

import UIKit

final class ItemDetailView: UIView {
    
    private var mainStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        return stackView
    }()
    
    private var itemName: UILabel = {
        var label = UILabel()
        label.text = "Unknown Item"
        label.font = UIFont.systemFont(ofSize: 22, weight: .semibold)
        return label
    }()
    
    private var cardStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.spacing = 8
        return stackView
    }()
    
    private var detailStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        return stackView
    }()
    
    private var itemImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.tintColor = .quaternaryLabel
        return imageView
    }()
    
    private var imageActivityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private var topStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private var quantityStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.layer.cornerRadius = 10
        stackView.layer.masksToBounds = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.clipsToBounds = false
        stackView.layoutMargins = UIEdgeInsets(top: 3, left: 2, bottom: 3, right: 2)
        stackView.layer.shadowOffset = CGSize(width: 0.3,
                                              height: 0.3)
        stackView.layer.shadowRadius = 1
        stackView.layer.shadowOpacity = 0.2
        stackView.backgroundColor = UIColor(named: "LightBlueSet")
        return stackView
    }()
    
    private var orderedStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.layer.cornerRadius = 10
        stackView.layer.masksToBounds = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 3, left: 2, bottom: 3, right: 2)
        stackView.clipsToBounds = false
        stackView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
        stackView.layer.shadowRadius = 1
        stackView.layer.shadowOpacity = 0.2
        stackView.backgroundColor = UIColor(named: "LightBlueSet")
        return stackView
    }()
    
    private var quantityLabel: UILabel = {
        var label = UILabel()
        label.text = "Quantity"
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textAlignment = .natural
        return label
    }()
    
    private var quantityValue: UILabel = {
        var label = UILabel()
        label.numberOfLines = 0
        label.text = "0"
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.textAlignment = .natural
        return label
    }()
    
    private var receiveLabel: UILabel = {
        var label = UILabel()
        label.text = "Ordered"
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textAlignment = .natural
        return label
    }()
    
    private var receiveValue: UILabel = {
        var label = UILabel()
        label.numberOfLines = 0
        label.text = "0"
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.textAlignment = .natural
        return label
    }()
    
    private var bottomStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private var lowStockStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.layer.cornerRadius = 10
        stackView.layer.masksToBounds = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 3, left: 2, bottom: 3, right: 2)
        stackView.clipsToBounds = false
        stackView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
        stackView.layer.shadowRadius = 1
        stackView.layer.shadowOpacity = 0.2
        stackView.backgroundColor = UIColor(named: "LightBlueSet")
        return stackView
    }()
    
    private var warningStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 4
        stackView.alignment = .leading
        stackView.distribution = .fill
        stackView.layer.cornerRadius = 8
        stackView.layer.masksToBounds = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        stackView.isHidden = true
        return stackView
    }()
    
    private var warningImageView: UIImageView = {
//        let imageConfig = UIImage.SymbolConfiguration(pointSize: 26, weight: .medium, scale: .medium)
//        let image = UIImage(systemName: "exclamationmark.circle.fill", withConfiguration: imageConfig)
//        var imageView = UIImageView(image: image)
        var imageView = UIImageView()
        return imageView
    }()
    
    private var warningLabel: UILabel = {
        var label = UILabel()
        label.textColor = .systemRed
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.textAlignment = .natural
        return label
    }()
    
    private var outOfStockImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.tintColor = .systemRed
        return imageView
    }()
    
    private var outOfStockLabel: UILabel = {
        var label = UILabel()
        label.textColor = .systemRed
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.textAlignment = .center
        return label
    }()
    
    private var limitWarningImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.tintColor = .systemYellow
        return imageView
    }()
    
    private var limitWarningLabel: UILabel = {
        var label = UILabel()
        label.textColor = .systemYellow
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.textAlignment = .center
        return label
    }()
    
    // MARK: -  Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        imageConfig()
        detailStackViewConfig()
        cardStackConfig()
        itemDetailHorizontalConfig()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    deinit {
//        print("deinit")
    }
    
    private func setupView() {
        layer.cornerRadius = 10
        layer.masksToBounds = true
        backgroundColor = .secondarySystemGroupedBackground
        addSubview(mainStackView)
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
        ])
        mainStackView.addArrangedSubview(itemName)
        mainStackView.addArrangedSubview(cardStackView)
        mainStackView.addArrangedSubview(warningStackView)
        
        warningStackView.addArrangedSubview(warningImageView)
        warningStackView.addArrangedSubview(warningLabel)
        warningImageView.centerYAnchor.constraint(equalTo: warningLabel.centerYAnchor).isActive = true
        warningImageView.setContentHuggingPriority(.init(251), for: .horizontal)
    }
    
    private func detailStackViewConfig() {
        detailStackView.addArrangedSubview(topStackView)
        detailStackView.addArrangedSubview(bottomStackView)
    }
    
    private func cardStackConfig(){
        quantityStackView.addArrangedSubview(quantityValue)
        quantityStackView.addArrangedSubview(quantityLabel)
        quantityValue.translatesAutoresizingMaskIntoConstraints = false
        quantityValue.centerXAnchor.constraint(equalTo: quantityStackView.centerXAnchor).isActive = true
        quantityLabel.translatesAutoresizingMaskIntoConstraints = false
        quantityLabel.centerXAnchor.constraint(equalTo: quantityStackView.centerXAnchor).isActive = true
        
        topStackView.addArrangedSubview(quantityStackView)
        orderedStackView.addArrangedSubview(receiveValue)
        orderedStackView.addArrangedSubview(receiveLabel)
        receiveValue.translatesAutoresizingMaskIntoConstraints = false
        receiveValue.centerXAnchor.constraint(equalTo: orderedStackView.centerXAnchor).isActive = true
        receiveLabel.translatesAutoresizingMaskIntoConstraints = false
        receiveLabel.centerXAnchor.constraint(equalTo: orderedStackView.centerXAnchor).isActive = true
        
        bottomStackView.addArrangedSubview(orderedStackView)
    }
    
    private func imageConfig() {
        itemImageView.addSubview(imageActivityIndicator)
        imageActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageActivityIndicator.topAnchor.constraint(equalTo: itemImageView.topAnchor),
            imageActivityIndicator.leadingAnchor.constraint(equalTo: itemImageView.leadingAnchor),
            imageActivityIndicator.bottomAnchor.constraint(equalTo: itemImageView.bottomAnchor),
            imageActivityIndicator.trailingAnchor.constraint(equalTo: itemImageView.trailingAnchor),
        ])
    }
    
    private func itemDetailHorizontalConfig() {
        cardStackView.axis = .horizontal
        cardStackView.alignment = .center
        cardStackView.distribution = .fillProportionally
        itemImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        itemImageView.heightAnchor.constraint(equalToConstant: 125).isActive = true
        cardStackView.addArrangedSubview(itemImageView)
        cardStackView.addArrangedSubview(detailStackView)
    }
    
    private func itemDetailVerticalConfig() {
        cardStackView.axis = .vertical
        cardStackView.alignment = .fill
        cardStackView.distribution = .fill
        itemImageView.widthAnchor.constraint(equalToConstant: 125).isActive = true
        itemImageView.heightAnchor.constraint(lessThanOrEqualTo: itemImageView.widthAnchor).isActive = true
        itemImageView.setContentHuggingPriority(.init(249), for: .vertical)
        itemImageView.setContentCompressionResistancePriority(.init(749), for: .vertical)
        cardStackView.addArrangedSubview(itemImageView)
        cardStackView.addArrangedSubview(detailStackView)
    }
    
    
    // MARK: - Functions
    
    func setItemModal(name: String, quantity: Int, received: Int) {
        itemName.text = name
        quantityValue.text = String(quantity)
        receiveValue.text = String(received)
        self.layoutIfNeeded()
    }
    
    func setItemImage(imageName: UUID?) {
        imageActivityIndicator.startAnimating()
        DispatchQueue.global().async {
            [weak self] in
//                            sleep(1)
            guard let imageName = imageName?.uuidString else {
                DispatchQueue.main.async {
                    self?.imageActivityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "photo")
                }
                return
            }
            let imagePath = URLStore.itemImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            do {
                let data = try Data(contentsOf: imagePath)
                DispatchQueue.main.async {
                    self?.imageActivityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(data: data)
                }
            } catch let error {
                DispatchQueue.main.async {
                    self?.imageActivityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "photo")
                }
                print("\n\nItemDetailView -> Loading Image to ImageView:\n\(error)\n\n")
            }
        }
    }
    
    func showWarning(for type: ItemWarning) {
        let imageconfig = UIImage.SymbolConfiguration.init(pointSize: 20)
        switch(type) {
        case .lowStock:
            warningLabel.text = "Low Stock"
            warningImageView.image = UIImage(systemName: "exclamationmark.circle.fill", withConfiguration: imageconfig)
            warningLabel.textColor = UIColor(named: "SandalSet")
            warningImageView.tintColor = UIColor(named: "SandalSet")
            warningStackView.isHidden = false
        case .outOfStock:
            warningLabel.text = "Out of Stock"
            warningImageView.image = UIImage(systemName: "exclamationmark.triangle.fill", withConfiguration: imageconfig)
            let warningColor = UIColor(named: "RedSet")
            warningImageView.tintColor = warningColor
            warningLabel.textColor = warningColor
            warningStackView.isHidden = false
        case .inActive:
            warningLabel.text = "Item Inactive"
            warningImageView.image = UIImage(systemName: "xmark.bin.fill", withConfiguration: imageconfig)
            let warningColor = UIColor(named: "PeachSet")
            warningLabel.textColor = warningColor
            warningImageView.tintColor = warningColor
            warningStackView.isHidden = false
        case .remove:
            warningStackView.isHidden = true
        }
    }
    
}
