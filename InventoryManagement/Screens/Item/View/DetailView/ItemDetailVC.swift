//
//  ItemDetailVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 14/09/22.
//

import UIKit
import CoreData

final class ItemDetailVC: UIViewController {

    lazy private var itemMenu: UIMenu = {
        return UIMenu(title: "", image: nil, identifier: nil, options: .destructive, children: [topMenu, bottomMenu])
    }()
    
    lazy private var topMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        
        var actions = [UIMenuElement]()
        if self?.itemModal.isActive ?? true {
            
            actions.append((self?.getPOAction())!)
            actions.append((self?.getSOAction())!)
            
        } else {
            
            let poAction = (self?.getPOAction())!
            poAction.attributes = .disabled
            let soAction = (self?.getSOAction())!
            soAction.attributes = .disabled
            actions.append(poAction)
            actions.append(soAction)
            
        }
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    lazy private var bottomMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        if self?.itemModal.isActive ?? true {
            
            actions.append((self?.getActiveAction())!)
            actions.append((self?.getLowStockAction())!)
            
        } else {
            
            let inactiveAction = (self?.getActiveAction())!
            inactiveAction.title = "Mark as Active"
            let lowStockAction = (self?.getLowStockAction())!
            lowStockAction.attributes = .disabled
            
            actions.append(inactiveAction)
            actions.append(lowStockAction)
            
        }
        
        actions.append((self?.getDeleteAction())!)
        
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    private let mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 4
        stackView.contentMode = .scaleAspectFit
        return stackView
    }()
    
    private let itemDetailView = ItemDetailView()
    
    private let detailView: UIView = {
        var view = UIView()
        return view
    }()
    
    private let segmentControl: UISegmentedControl = {
        let segment = UISegmentedControl(items: ["Details", "Transactions"])
        segment.selectedSegmentIndex = 0
        return segment
    }()
    
    private let historyStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        return stackView
    }()
    
    private let historyView = UIView()
    
    private let historyTableView: UITableView = {
        var tableView = UITableView(frame: CGRect(), style: .insetGrouped)
        tableView.register(ItemRecordCell.self, forCellReuseIdentifier: ItemRecordCell.identifier)
        tableView.register(ItemDetailCell.self, forCellReuseIdentifier: ItemDetailCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.allowsSelection = false
        tableView.sectionHeaderTopPadding = CGFloat.leastNormalMagnitude
        tableView.sectionHeaderHeight = CGFloat.leastNormalMagnitude
        return tableView
    }()
    
    private let tableViewActivityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        let image = UIImage(named: "NoTaskNotepad")?.withTintColor(.secondaryLabel)
        noDataView.noDataImage.image = image
        noDataView.titleLabel.text = "No Records Found!"
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .long
        return formatter
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private var hideDataLabel: Bool = false {
        didSet {
            backgroundView.isHidden = hideDataLabel
            historyTableView.isHidden = !hideDataLabel
        }
    }
    
    private var segmentIndex: Int = 0
    
    private let CDM = CoreDataManager.shared
    
    private var itemModal = ItemDetailModal()
    
    private var itemRecordModals: [ItemRecordModal] = []
    
    private var itemAccountModal: ItemAccountModal?
    
    private var id: UUID
    
    // MARK: - Properties & Init
    
    init(id: UUID) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemGroupedBackground
        // Do any additional setup after loading the view.
        setupConfig()
        fetchDisplayModal()
        addNotificationObservers()
        
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
//        print("deinit")
    }
    
    // MARK: - Configurations
    
    private func setupConfig() {
        historyViewConfig()
        mainViewConfig()
        navigationConfig()
        itemDetailViewConfig()
        historyTableViewConfig()
        mainStackConfig()
    }
    
    private func historyViewConfig() {
        historyView.addSubview(segmentControl)
        historyView.addSubview(historyTableView)
        historyTableView.translatesAutoresizingMaskIntoConstraints = false
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        historyTableView.addSubview(tableViewActivityIndicator)
        tableViewActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            segmentControl.topAnchor.constraint(equalTo: historyView.topAnchor),
            segmentControl.leadingAnchor.constraint(equalTo: historyView.leadingAnchor, constant: 16),
            segmentControl.heightAnchor.constraint(equalToConstant: 36),
            segmentControl.bottomAnchor.constraint(equalTo: historyTableView.topAnchor, constant: -8),
            segmentControl.trailingAnchor.constraint(equalTo: historyView.trailingAnchor, constant: -16),
            historyTableView.topAnchor.constraint(greaterThanOrEqualTo: segmentControl.bottomAnchor, constant: 8),
            historyTableView.leadingAnchor.constraint(equalTo: historyView.leadingAnchor),
            historyTableView.bottomAnchor.constraint(equalTo: historyView.bottomAnchor),
            historyTableView.trailingAnchor.constraint(lessThanOrEqualTo: historyView.trailingAnchor),
            
            tableViewActivityIndicator.topAnchor.constraint(equalTo: historyTableView.topAnchor),
            tableViewActivityIndicator.leadingAnchor.constraint(equalTo: historyTableView.leadingAnchor),
            tableViewActivityIndicator.bottomAnchor.constraint(equalTo: historyTableView.bottomAnchor),
            tableViewActivityIndicator.trailingAnchor.constraint(equalTo: historyTableView.trailingAnchor),
        ])
        segmentControl.addTarget(self, action: #selector(segmentChanged(_:)), for: .valueChanged)
    }
    
    private func mainViewConfig() {
        view.addSubview(mainStackView)
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
//        historyStackView.addArrangedSubview(historyView)
        historyTableView.dataSource = self
        historyTableView.delegate = self
    }
    
    private func navigationConfig() {
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", image: UIImage(systemName: "ellipsis.circle"), primaryAction: nil, menu: itemMenu)
    }
    
    private func itemDetailViewConfig() {
        detailView.addSubview(itemDetailView)
        itemDetailView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemDetailView.topAnchor.constraint(equalTo: detailView.topAnchor),
            itemDetailView.leadingAnchor.constraint(equalTo: detailView.leadingAnchor, constant: 16),
            itemDetailView.bottomAnchor.constraint(equalTo: detailView.bottomAnchor, constant: -8),
            itemDetailView.trailingAnchor.constraint(equalTo: detailView.trailingAnchor, constant: -16),
        ])
    }
    
    private func historyTableViewConfig() {
        historyTableView.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: historyTableView.topAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: historyTableView.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: historyTableView.trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: historyTableView.bottomAnchor),
        ])
    }
    
    private func mainStackConfig() {
        mainStackView.axis = .vertical
        mainStackView.alignment = .fill
        mainStackView.spacing = 4
        mainStackView.addArrangedSubview(detailView)
        mainStackView.addArrangedSubview(historyView)
    }
    
    // MARK: - Data Fetching
    
    private func updateItemDetailView(doReferesh: Bool = false) {
        if doReferesh {
            fetchDisplayModal()
            fetchItemRecordOfCurrentPeriod()
        } else {
            itemDetailView.setItemModal(name: itemModal.itemName ?? "Item Not Found", quantity: (itemModal.availableQuantity ?? 0), received: (itemModal.yetToReceive ?? 0 ))
            itemDetailView.showWarning(for: getWarningType(from: itemModal))
            itemDetailView.setItemImage(imageName: itemModal.itemImage)
        }
    }
    
    private func updateHistoryTableView() {
        if itemRecordModals.isEmpty {
            hideDataLabel = false
        } else {
            hideDataLabel = true
        }
        historyTableView.reloadData()
    }
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            if let item =  self?.CDM.fetchItem(for: self?.id, with: ["itemName", "itemQuantity", "lowStockLimit", "isActive", "itemImage"])?.first {
                self?.itemModal.itemName = item.itemName
                self?.itemModal.itemQuantity = Int(item.itemQuantity)
                self?.itemModal.lowStockLimit = Int(item.lowStockLimit)
                self?.itemModal.isActive = item.isActive
                self?.itemModal.itemImage = item.itemImage
                self?.itemModal.availableQuantity = item.quantityDetail[.available]
                self?.itemModal.yetToReceive = item.quantityDetail[.ordered]
                if let records = self?.CDM.fetchItemRecord(forItem: self?.id, with: ["recordDate", "netQuantity", "purchaseOrder", "salesOrder", "adjustmentRecord"], sortBy: [NSSortDescriptor(key: "recordDate", ascending: false)]) {
                    self?.itemRecordModals = records.map({
                        record in
                        return ItemRecordModal(recordDate: record.recordDate, netQuantity: Int(record.netQuantity), purchaseOrder: record.purchaseOrder, salesOrder: record.salesOrder, adjustmentRecord: record.adjustmentRecord)
                    })
                    
                }
            }
            self?.fetchItemRecordOfCurrentPeriod()
            DispatchQueue.main.async {
                self?.updateItemDetailView()
                self?.updateHistoryTableView()
            }
        }
    }
    
    #warning("Simplify this")
    private func fetchItemRecordOfCurrentPeriod() {
        DispatchQueue.main.async {
            [weak self] in
            self?.tableViewActivityIndicator.startAnimating()
        }
        DispatchQueue.global().async {
            [weak self] in
            let startDate = Date.now.startOfMonth
            let endDate = Date.now.endOfMonth
            if let records = self?.CDM.fetchItemRecord(forItem: self?.id, with: ["netQuantity", "salesOrder", "purchaseOrder", "adjustmentRecord"], check: [NSPredicate(format: "(recordDate >= %@) AND (recordDate <=%@)", startDate as CVarArg, endDate as CVarArg)], sortBy: [NSSortDescriptor(key: "recordDate", ascending: true)]) {
                let openingStock = Int(records.first?.netQuantity ?? 0)
                var costPrice = 0.0
                var sellPrice = 0.0
                let purchaseIndex = records.lastIndex(where: {
                    return $0.purchaseOrder != nil
                })
                let sellIndex = records.lastIndex(where: {
                    return $0.salesOrder != nil
                })
                if let index = purchaseIndex {
                    costPrice = Double(records[index].purchaseOrder?.purchaseItemCost ?? 0)
                }
                if let index = sellIndex {
                    sellPrice = Double(records[index].salesOrder?.salesItemCost ?? 0)
                }
                var totalSold = 0.0
                var totalPurchased = 0.0
                for record in records {
                    if let sales = record.salesOrder {
                        totalSold += Double(sales.salesTotalCost)
                    }
                    if let purchase = record.purchaseOrder {
                        totalPurchased += Double(purchase.purchaseTotalCost)
                    }
                }
                self?.itemAccountModal = ItemAccountModal(openingStock: openingStock, sellPrice: sellPrice, costPrice: costPrice, totalSold: totalSold, totalPurchased: totalPurchased, unsettledAmount: Double(self?.itemModal.availableQuantity ?? 0) * costPrice, currentStockPrice: Double(self?.itemModal.availableQuantity ?? 0) * sellPrice)
            }
            DispatchQueue.main.async {
                [weak self] in
                self?.historyTableView.reloadData()
                self?.tableViewActivityIndicator.stopAnimating()
            }
        }
    }

    // MARK: - Functions
    
    private func handleItemState() {
        if let item = CDM.fetchItem(for: id, with: ["isActive"])?.first {
            if item.isActive {
                item.isActive = false
                itemModal.isActive = false
            } else {
                item.isActive = true
                itemModal.isActive = true
            }
            do {
                try CDM.viewContext.save()
            } catch let error {
                print("\n\nItemDetailVC -> CoreData Save Failed\n\(error)\n\n")
            }
            updateItemDetailView()
            NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
        }
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTransaction), name: POKey.reloadPO, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTransaction), name: SOKey.reloadSO, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: ItemKey.deleteItem, object: nil)
    }
    
    private func getWarningType(from item: ItemDetailModal) -> ItemWarning {
        if !item.isActive! {
            return .inActive
        } else if item.availableQuantity! <= 0 {
            return .outOfStock
        } else if itemModal.availableQuantity! < itemModal.lowStockLimit! {
            return .lowStock
        } else {
            return .remove
        }
    }
    
    @objc
    private func dismissView() {
        if traitCollection.userInterfaceIdiom == .pad {
            let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
            vc?.setViewControllers([SplitViewController.blankVC], animated: true)
        } else if traitCollection.userInterfaceIdiom == .phone {
            splitViewController?.hide(.secondary)
        }
    }
    
    @objc
    private func reloadTransaction() {
        updateItemDetailView(doReferesh: true)
    }
    
    @objc
    private func segmentChanged(_ sender: UISegmentedControl) {
        segmentIndex = sender.selectedSegmentIndex
        historyTableView.reloadData()
    }
    
    private func makePurchaseOrder() {
        present(UINavigationController(rootViewController: AddPOVC(item: self.CDM.fetchItem(for: self.id)?.first)), animated: true)
    }
    
    private func makeSalesOrder() {
        present(UINavigationController(rootViewController: AddSOVC(item: self.CDM.fetchItem(for: self.id)?.first)), animated: true)
    }
    
    private func adjustLowLimit() {
        let vc = AdjustItemValueVC(id: id, type: .lowLimit, lowLimit: itemModal.lowStockLimit ?? 0, completion: {
            [weak self] newLimit in
            if let limit = newLimit {
                self?.itemModal.lowStockLimit = limit
                self?.updateItemDetailView()
                NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
            }
        })
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .formSheet
        nav.sheetPresentationController?.detents = [.medium()]
        present(nav, animated: true )
    }
    
    private func deleteItem() {
        let item = CDM.fetchItem(for: self.id, with: ["itemName"])?.first
        DispatchQueue.global().async {
            ImageHelper.deleteImage(item?.itemImage, for: .ItemImage)
        }
        self.CDM.viewContext.delete(item!)
        do {
            try CDM.viewContext.save()
        } catch {
            print("\n\nItemViewController: -> Saving to CoreData\n\(error)\n\n")
        }
        NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
        NotificationCenter.default.post(name: ItemKey.deleteItem, object: nil)
        splitViewController?.hide(.secondary)
    }
    
    private func getPOAction() -> UIAction {
        UIAction(
            title: "Make Purchase Order",
            image: UIImage(systemName: "cart.fill"),
            handler: {
                [weak self] action in
                self?.makePurchaseOrder()
            })
    }
    
    private func getSOAction() -> UIAction {
        UIAction(
            title: "Make Sales Order",
            image: UIImage(systemName: "coloncurrencysign.circle"),
            handler: {
                [weak self] action in
                self?.makeSalesOrder()
            })
    }
    
    private func getActiveAction() -> UIAction {
        UIAction(
            title: "Mark as Inactive",
            image: UIImage(systemName: "xmark.bin.fill"),
            handler: {
                [weak self] action in
                self?.handleItemState()
            })
    }
    
    private func getLowStockAction() -> UIAction {
        UIAction(
            title: "Adjust Low Stock Limit",
            handler: {
                [weak self] action in
                self?.adjustLowLimit()
            })
    }
    
    private func getDeleteAction() -> UIAction {
        UIAction(
            title: "Delete Item",
            image: {
                let image = UIImage(systemName: "trash")
                return image?.withTintColor(.systemRed)
            }(),
            attributes: .destructive,
            handler: {
                [weak self] action in
                
                if self?.traitCollection.userInterfaceIdiom == .phone {
                    
                    let alert = UIAlertController(title: "", message: "Are you sure you want to delete this Item?", preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Delete", style: .destructive){
                        action in
                        self?.deleteItem()
                    })
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                    self?.present(alert, animated: true)
                    
                } else if self?.traitCollection.userInterfaceIdiom == .pad {
                    
                    let alert = UIAlertController(title: "", message: "Are you sure you want to delete this Item?", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Delete", style: .destructive) {
                        action in
                        self?.deleteItem()
                    })
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                    self?.present(alert, animated: true)
                    
                }
                
            })
    }
    
}

extension ItemDetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch ( segmentIndex ) {
        case 0:
            return 1
        case 1:
            return itemRecordModals.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch ( segmentIndex ) {
        case 0:
            return "Value shown for current month"
        case 1:
            return nil
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch ( segmentIndex ) {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemDetailCell.identifier, for: indexPath) as! ItemDetailCell
            cell.openingStockValue.text = "\(itemAccountModal?.openingStock ?? 0)"
            cell.stockPriceValue.text = currencyFormatter.string(from: (itemAccountModal?.currentStockPrice ?? 0) as NSNumber)
            cell.sellPriceValue.text = currencyFormatter.string(from: (itemAccountModal?.sellPrice ?? 0) as NSNumber)
            cell.costPriceValue.text = currencyFormatter.string(from: (itemAccountModal?.costPrice ?? 0) as NSNumber)
            cell.totalSoldValue.text = currencyFormatter.string(from: (itemAccountModal?.totalSold ?? 0) as NSNumber)
            cell.totalPurchaseValue.text = currencyFormatter.string(from: (itemAccountModal?.totalPurchased ?? 0) as NSNumber)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemRecordCell.identifier, for: indexPath) as! ItemRecordCell
            if let date = itemRecordModals[indexPath.section].recordDate {
                cell.dateValue.text = date.formatted(date: .abbreviated, time: .omitted)
            }
            let config = UIImage.SymbolConfiguration(pointSize: 12, weight: .regular, scale: .medium)
            if let purchaseOrder = itemRecordModals[indexPath.section].purchaseOrder {
                cell.statusImageView.image = UIImage(systemName: "cart")
                cell.statusImageView.tintColor = .systemTeal
                cell.statusLabel.text = "Purchase Order"
                cell.byImageView.image = UIImage(systemName: "person.text.rectangle", withConfiguration: config)
                cell.newQuantityValue.text = "\(purchaseOrder.purchaseQuantity)"
                cell.byValue.text = purchaseOrder.vendor.displayName
                cell.netQuantityStackView.backgroundColor = UIColor(named: "LightTealSet")
                cell.newQuantityStackView.backgroundColor = UIColor(named: "LightTealSet")
            } else if let salesOrder = itemRecordModals[indexPath.section].salesOrder {
                cell.statusImageView.image = UIImage(systemName: "coloncurrencysign.circle")
                cell.statusImageView.tintColor = .systemRed
                cell.statusLabel.text = "Sales Order"
                cell.byImageView.image = UIImage(systemName: "person", withConfiguration: config)
                cell.newQuantityValue.text = "\(salesOrder.salesQuantity)"
                cell.byValue.text = salesOrder.customer.displayName
                cell.netQuantityStackView.backgroundColor = UIColor(named: "LightRedSet")
                cell.newQuantityStackView.backgroundColor = UIColor(named: "LightRedSet")
#warning("This below If case will never be true")
            } else {
                cell.statusImageView.image = UIImage(systemName: "circle.fill")
                cell.statusImageView.tintColor = .systemBlue
                cell.statusLabel.text = "Order Created"
                cell.noOrder()
            }
            cell.netQuantityValue.text = "\(itemRecordModals[indexPath.section].netQuantity ?? 0)"
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension ItemDetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 6
    }
}
