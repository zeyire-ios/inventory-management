//
//  ItemDetailCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 24/09/22.
//

import UIKit

class ItemDetailCell: BaseTableViewCell {
    
    private var mainStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.alignment = .fill
        return stackView
    }()
    
    private var openingStockView = UIView()
    
    private var openingStockLabel: UILabel = {
        var label = UILabel()
        label.text = "Opening Stock"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()

    var openingStockValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private var stockPriceView = UIView()
    
    private var stockPriceLabel: UILabel = {
        var label = UILabel()
        label.text = "Current Stock Price"
        label.font = .preferredFont(forTextStyle: .callout)
        label.textColor = .secondaryLabel
        return label
    }()
    
    var stockPriceValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = .preferredFont(forTextStyle: .callout)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private var stockPriceSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    private var accountLabel: UILabel = {
        var label = UILabel()
        label.text = "Accounting Information"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textAlignment = .natural
        return label
    }()
    
    private var sellPriceView = UIView()
    
    private var sellPriceLabel: UILabel = {
        var label = UILabel()
        label.text = "Sell Price"
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()

    var sellPriceValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private var costPriceView = UIView()
    
    private var costPriceLabel: UILabel = {
        var label = UILabel()
        label.text = "Cost Price"
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()
    
    var costPriceValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private var totalSoldView = UIView()
    
    private var totalSoldLabel: UILabel = {
        var label = UILabel()
        label.text = "Total Sold"
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()

    var totalSoldValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private var totalPurchaseView = UIView()
    
    private var totalPurchaseLabel: UILabel = {
        var label = UILabel()
        label.text = "Total Purchased"
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()

    var totalPurchaseValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()
    
    static let identifier = "com.abilash.ItemDetailCell"
    
    // MARK: - Init
    
    override func setupCell() {
        contentView.addSubview(mainStack)
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            mainStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
        ])
        openingStockViewConfig()
        stockPriceViewConfig()
        sellPriceViewConfig()
        costPriceViewConfig()
        totalSoldViewConfig()
        totalPurchaseViewConfig()
        mainStackViewConfig()
    }
    
    private func openingStockViewConfig() {
        openingStockView.addSubview(openingStockLabel)
        openingStockLabel.translatesAutoresizingMaskIntoConstraints = false
        openingStockView.addSubview(openingStockValue)
        openingStockValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            openingStockLabel.topAnchor.constraint(equalTo: openingStockView.topAnchor, constant: 4),
            openingStockLabel.leadingAnchor.constraint(equalTo: openingStockView.leadingAnchor),
            openingStockLabel.bottomAnchor.constraint(equalTo: openingStockView.bottomAnchor, constant: -4),
            openingStockLabel.trailingAnchor.constraint(lessThanOrEqualTo: openingStockValue.leadingAnchor, constant: -8),
            openingStockValue.leadingAnchor.constraint(greaterThanOrEqualTo: openingStockLabel.trailingAnchor, constant: 8),
            openingStockValue.centerYAnchor.constraint(equalTo: openingStockLabel.centerYAnchor),
            openingStockValue.trailingAnchor.constraint(equalTo: openingStockView.trailingAnchor),
        ])
        openingStockValue.setContentHuggingPriority(.init(249), for: .horizontal)
        openingStockView.setContentHuggingPriority(.init(260), for: .vertical)
    }
    
    private func stockPriceViewConfig() {
        stockPriceView.addSubview(stockPriceLabel)
        stockPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        stockPriceView.addSubview(stockPriceValue)
        stockPriceValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stockPriceLabel.topAnchor.constraint(equalTo: stockPriceView.topAnchor, constant: 4),
            stockPriceLabel.leadingAnchor.constraint(equalTo: stockPriceView.leadingAnchor, constant: 8),
            stockPriceLabel.bottomAnchor.constraint(equalTo: stockPriceView.bottomAnchor, constant: -4),
            stockPriceLabel.trailingAnchor.constraint(lessThanOrEqualTo: stockPriceValue.leadingAnchor, constant: -8),
            stockPriceValue.leadingAnchor.constraint(greaterThanOrEqualTo: stockPriceLabel.trailingAnchor, constant: 8),
            stockPriceValue.centerYAnchor.constraint(equalTo: stockPriceLabel.centerYAnchor),
            stockPriceValue.trailingAnchor.constraint(equalTo: stockPriceView.trailingAnchor),
            
        ])
        stockPriceValue.setContentHuggingPriority(.init(249), for: .horizontal)
        stockPriceView.setContentHuggingPriority(.init(258), for: .vertical)
    }
    
    private func sellPriceViewConfig() {
        sellPriceView.addSubview(sellPriceLabel)
        sellPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        sellPriceView.addSubview(sellPriceValue)
        sellPriceValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sellPriceLabel.topAnchor.constraint(equalTo: sellPriceView.topAnchor, constant: 4),
            sellPriceLabel.leadingAnchor.constraint(equalTo: sellPriceView.leadingAnchor, constant: 8),
            sellPriceLabel.bottomAnchor.constraint(equalTo: sellPriceView.bottomAnchor, constant: -4),
            sellPriceLabel.trailingAnchor.constraint(lessThanOrEqualTo: sellPriceValue.leadingAnchor, constant: -8),
            sellPriceValue.leadingAnchor.constraint(greaterThanOrEqualTo: sellPriceLabel.trailingAnchor, constant: 8),
            sellPriceValue.centerYAnchor.constraint(equalTo: sellPriceLabel.centerYAnchor),
            sellPriceValue.trailingAnchor.constraint(equalTo: sellPriceView.trailingAnchor),
        ])
        sellPriceValue.setContentHuggingPriority(.init(249), for: .horizontal)
        sellPriceView.setContentHuggingPriority(.init(260), for: .vertical)
    }
    
    private func costPriceViewConfig() {
        costPriceView.addSubview(costPriceLabel)
        costPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        costPriceView.addSubview(costPriceValue)
        costPriceValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            costPriceLabel.topAnchor.constraint(equalTo: costPriceView.topAnchor, constant: 4),
            costPriceLabel.leadingAnchor.constraint(equalTo: costPriceView.leadingAnchor, constant: 8),
            costPriceLabel.bottomAnchor.constraint(equalTo: costPriceView.bottomAnchor, constant: -4),
            costPriceLabel.trailingAnchor.constraint(lessThanOrEqualTo: costPriceValue.leadingAnchor, constant: -8),
            costPriceValue.leadingAnchor.constraint(greaterThanOrEqualTo: costPriceLabel.trailingAnchor, constant: 8),
            costPriceValue.centerYAnchor.constraint(equalTo: costPriceLabel.centerYAnchor),
            costPriceValue.trailingAnchor.constraint(equalTo: costPriceView.trailingAnchor),
        ])
        costPriceValue.setContentHuggingPriority(.init(249), for: .horizontal)
        costPriceView.setContentHuggingPriority(.init(260), for: .vertical)
    }
    
    private func totalSoldViewConfig() {
        totalSoldView.addSubview(totalSoldLabel)
        totalSoldLabel.translatesAutoresizingMaskIntoConstraints = false
        totalSoldView.addSubview(totalSoldValue)
        totalSoldValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            totalSoldLabel.topAnchor.constraint(equalTo: totalSoldView.topAnchor, constant: 4),
            totalSoldLabel.leadingAnchor.constraint(equalTo: totalSoldView.leadingAnchor, constant: 8),
            totalSoldLabel.bottomAnchor.constraint(equalTo: totalSoldView.bottomAnchor, constant: -4),
            totalSoldLabel.trailingAnchor.constraint(lessThanOrEqualTo: totalSoldValue.leadingAnchor, constant: -8),
            totalSoldValue.leadingAnchor.constraint(greaterThanOrEqualTo: totalSoldLabel.trailingAnchor, constant: 8),
            totalSoldValue.centerYAnchor.constraint(equalTo: totalSoldLabel.centerYAnchor),
            totalSoldValue.trailingAnchor.constraint(equalTo: totalSoldView.trailingAnchor),
        ])
        totalSoldValue.setContentHuggingPriority(.init(249), for: .horizontal)
        totalSoldView.setContentHuggingPriority(.init(260), for: .vertical)
    }
    
    private func totalPurchaseViewConfig() {
        totalPurchaseView.addSubview(totalPurchaseLabel)
        totalPurchaseLabel.translatesAutoresizingMaskIntoConstraints = false
        totalPurchaseView.addSubview(totalPurchaseValue)
        totalPurchaseValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            totalPurchaseLabel.topAnchor.constraint(equalTo: totalPurchaseView.topAnchor, constant: 4),
            totalPurchaseLabel.leadingAnchor.constraint(equalTo: totalPurchaseView.leadingAnchor, constant: 8),
            totalPurchaseLabel.bottomAnchor.constraint(equalTo: totalPurchaseView.bottomAnchor, constant: -4),
            totalPurchaseLabel.trailingAnchor.constraint(lessThanOrEqualTo: totalPurchaseValue.leadingAnchor, constant: -8),
            totalPurchaseValue.leadingAnchor.constraint(greaterThanOrEqualTo: totalPurchaseLabel.trailingAnchor, constant: 8),
            totalPurchaseValue.centerYAnchor.constraint(equalTo: totalPurchaseLabel.centerYAnchor),
            totalPurchaseValue.trailingAnchor.constraint(equalTo: totalPurchaseView.trailingAnchor),
        ])
        totalPurchaseValue.setContentHuggingPriority(.init(249), for: .horizontal)
        totalPurchaseView.setContentHuggingPriority(.init(260), for: .vertical)
    }
    
    private func mainStackViewConfig() {
        mainStack.addArrangedSubview(openingStockView)
        mainStack.addArrangedSubview(stockPriceView)
        mainStack.addArrangedSubview(stockPriceSeparator)
        mainStack.addArrangedSubview(accountLabel)
        mainStack.addArrangedSubview(sellPriceView)
        mainStack.addArrangedSubview(costPriceView)
        mainStack.addArrangedSubview(totalSoldView)
        mainStack.addArrangedSubview(totalPurchaseView)
    }
    
}
