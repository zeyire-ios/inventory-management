//
//  AdjustLowLimitVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 25/09/22.
//

import UIKit

class AdjustItemValueVC: UIViewController {
    
    private var lowLimitLabel: UILabel = {
        var label = UILabel()
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    private var textField: UITextField = {
        var textField = UITextField()
        textField.placeholder = "Low Stock Limit"
        textField.keyboardType = .numberPad
        textField.borderStyle = .roundedRect
        textField.clearButtonMode = .whileEditing
        return textField
    }()
    
    private var doneButton: Bool = false {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = doneButton
        }
    }
    
    private let id: UUID
    
    private let type: ItemAdjustment
    
    private var newValue: Int?
    
    private let lowLimit: Int
    
    private let dismissed: (Int?)->()
    
    private let CDM = CoreDataManager.shared
    
    private var textFieldValue: Int? {
        didSet{
            if oldValue == nil {
                doneButton = false
            } else {
                doneButton = true
            }
        }
    }
    
    init(id: UUID, type: ItemAdjustment, lowLimit: Int , completion: @escaping (Int?)->()) {
        self.id = id
        self.type = type
        self.lowLimit = lowLimit
        self.dismissed = completion
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    deinit {
        dismissed(newValue)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }
    
    private func commonInit() {
        view.backgroundColor = .systemBackground
        textFieldConfig()
        navigationControllerConfig()
    }
    
    private func textFieldConfig() {
        view.addSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lowLimitLabel)
        lowLimitLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            lowLimitLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            lowLimitLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            lowLimitLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            textField.topAnchor.constraint(equalTo: lowLimitLabel.bottomAnchor, constant: 10),
            textField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            textField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            textField.heightAnchor.constraint(equalToConstant: 36)
        ])
        textField.delegate = self
        switch (type) {
        case .lowLimit:
            textField.placeholder = "New Limit"
            lowLimitLabel.text = "Low Stock Limit \t\(lowLimit)"
        }
    }
    
    private func navigationControllerConfig() {
        switch (type) {
        case .lowLimit:
            navigationItem.title = "Adjust Low Limit"
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonPressed(_:)))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    @objc
    private func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @objc
    private func saveButtonPressed(_ sender: UIBarButtonItem) {
        guard let item = self.CDM.fetchItem(for: self.id)?.first, let value = textFieldValue else { return }
        do {
            switch (type) {
            case .lowLimit:
                item.lowStockLimit = Int64(value)
                self.newValue = value
            }
            try self.CDM.viewContext.save()
        } catch let error {
            print("\n\nAdjustLowLimitVC-> Core Data Save failed\n(\(error)\n\n")
        }
        dismiss(animated: true)
    }
    
    private func updateModal(value: String) {
        if let value = Int(value) {
            textFieldValue = value
            doneButton = true
        } else {
            doneButton = false
        }
    }
}

extension AdjustItemValueVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let _ =  (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)) {
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange,
                                                           with: string)
                updateModal(value: updatedText)
                return true
            } else {
                return false
            }
        } else if string == "" {
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange,
                                                           with: string)
                updateModal(value: updatedText)
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        updateModal(value: "")
        return true
    }
    
}

