//
//  ItemAccountModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 25/09/22.
//

import Foundation

struct ItemAccountModal {
    var openingStock: Int
    var sellPrice: Double
    var costPrice: Double
    var totalSold: Double
    var totalPurchased: Double
    var unsettledAmount: Double
    var currentStockPrice: Double
}
