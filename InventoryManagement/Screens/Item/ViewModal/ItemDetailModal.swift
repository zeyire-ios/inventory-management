//
//  ItemDetailModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/09/22.
//

import Foundation

struct ItemDetailModal {
    
    var itemName: String?
    var itemQuantity: Int?
    var lowStockLimit: Int?
    var isActive: Bool?
    var itemImage: UUID?
    var availableQuantity: Int?
    var yetToReceive: Int?

}
