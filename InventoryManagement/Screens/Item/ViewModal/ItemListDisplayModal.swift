//
//  ItemListDisplayModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/09/22.
//

import Foundation

struct ItemListDisplayModal {
    var itemID: UUID?
    var itemName: String?
    var itemQuantity: Int64?
    var isLowStock: Bool?
    var isActive: Bool?
    var itemImage: UUID?
    var quantityDetial: [ Item.QuantityType : Int]?
    var priceDetail: [ Item.PriceType : Double]?
}
