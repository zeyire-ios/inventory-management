//
//  ItemSort.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 11/10/22.
//

import Foundation

struct ItemSort: SortOptions {
    
    func setSortBy(sortBy: Any) {
        ItemSort._setSortBy(sortBy: sortBy as! SortBy)
    }
    
    func setSortOrder(ascending: Bool) {
        ItemSort._setSortOrder(ascending: ascending)
    }
    
    func setFilterBy(filterBy: Any) {
        ItemSort._setFilter(by: filterBy as! FilterBy)
    }
    
    static let sortBy: [SortBy] = [ .name, .quantity]
    
    static let filterBy: [FilterBy] = [.all, .active, .inactive, .lowStock, .outOfStock]
    
    enum SortBy: String {
        case name = "Name"
        case quantity = "Item Quantity"
    }
    
    enum FilterBy: String {
        case all = "All Items"
        case active = "Active Items"
        case inactive = "Inactive Items"
        case lowStock = "Low Stock Items"
        case outOfStock = "Out of Stock Items"
    }
    
    static func checkSortOrder() -> Bool {
        if UserDefaults.standard.value(forKey: "ItemAscending") == nil {
            UserDefaults.standard.set(true, forKey: "ItemAscending")
        }
        return UserDefaults.standard.bool(forKey: "ItemAscending")
    }
    
    static func _setSortOrder(ascending: Bool) {
        UserDefaults.standard.set(ascending, forKey: "ItemAscending")
    }
    
    static func checkSortBy() -> SortBy {
        if UserDefaults.standard.value(forKey: "ItemSortBy") == nil {
            UserDefaults.standard.set(SortBy.name.rawValue, forKey: "ItemSortBy")
        }
        return SortBy.init(rawValue: (UserDefaults.standard.string(forKey: "ItemSortBy"))!)!
        
    }
    
    static func _setSortBy(sortBy: SortBy) {
        UserDefaults.standard.set(sortBy.rawValue, forKey: "ItemSortBy")
    }
    
    static func checkFilter() -> FilterBy {
        if UserDefaults.standard.value(forKey: "ItemFilter") == nil {
            UserDefaults.standard.set(FilterBy.all.rawValue, forKey: "ItemFilter")
        }
        return FilterBy.init(rawValue: (UserDefaults.standard.string(forKey: "ItemFilter"))!)!
    }
    
    static func _setFilter(by: FilterBy) {
        UserDefaults.standard.set(by.rawValue, forKey: "ItemFilter")
    }
}
