//
//  Item+CoreDataProperties.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 23/09/22.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var itemImage: UUID?
    @NSManaged public var itemID: UUID
    @NSManaged public var isActive: Bool
    @NSManaged public var itemName: String
    @NSManaged public var lowStockLimit: Int64
    @NSManaged public var itemQuantity: Int64
    @NSManaged public var itemRecords: NSSet?
    @NSManaged public var purchaseOrders: NSSet?
    @NSManaged public var salesOrders: NSSet?

    public var itemRecordsArray: [ItemRecord] {
        let set = itemRecords as? Set<ItemRecord> ?? []
        
        return set.sorted {
            $0.recordDate > $1.recordDate
        }
    }
}

// MARK: Generated accessors for itemRecords
extension Item {

    @objc(addItemRecordsObject:)
    @NSManaged public func addToItemRecords(_ value: ItemRecord)

    @objc(removeItemRecordsObject:)
    @NSManaged public func removeFromItemRecords(_ value: ItemRecord)

    @objc(addItemRecords:)
    @NSManaged public func addToItemRecords(_ values: NSSet)

    @objc(removeItemRecords:)
    @NSManaged public func removeFromItemRecords(_ values: NSSet)

}

// MARK: Generated accessors for purchaseOrders
extension Item {

    @objc(addPurchaseOrdersObject:)
    @NSManaged public func addToPurchaseOrders(_ value: PurchaseOrder)

    @objc(removePurchaseOrdersObject:)
    @NSManaged public func removeFromPurchaseOrders(_ value: PurchaseOrder)

    @objc(addPurchaseOrders:)
    @NSManaged public func addToPurchaseOrders(_ values: NSSet)

    @objc(removePurchaseOrders:)
    @NSManaged public func removeFromPurchaseOrders(_ values: NSSet)

}

// MARK: Generated accessors for salesOrders
extension Item {

    @objc(addSalesOrdersObject:)
    @NSManaged public func addToSalesOrders(_ value: SalesOrder)

    @objc(removeSalesOrdersObject:)
    @NSManaged public func removeFromSalesOrders(_ value: SalesOrder)

    @objc(addSalesOrders:)
    @NSManaged public func addToSalesOrders(_ values: NSSet)

    @objc(removeSalesOrders:)
    @NSManaged public func removeFromSalesOrders(_ values: NSSet)

}

extension Item : Identifiable {

}
