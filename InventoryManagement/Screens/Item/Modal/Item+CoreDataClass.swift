//
//  Item+CoreDataClass.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 23/09/22.
//
//

import Foundation
import CoreData

@objc(Item)
public class Item: NSManagedObject {
    
    enum QuantityType: String {
        case available = "AvailableQuantity"
        case ordered = "YetToReceive"
    }
    
    enum PriceType: String {
        case costprice = "CostPrice"
        case sellprice = "SellPrice"
    }
    
    var isLowStock: Bool {
        get {
            return itemQuantity < lowStockLimit ? true : false
        }
    }
    
    var quantityDetail: [ QuantityType : Int] {
        get {
            var initialQuantity = 0
            var totalSold = 0
            var totalPurchased = 0
            var yetToReceive = 0
            let initialRecord = itemRecordsArray.first(where: {
                if $0.salesOrder == nil && $0.purchaseOrder == nil {
                    return true
                } else {
                    return false
                }
            })
            for record in itemRecordsArray {
                if let sales = record.salesOrder {
                    totalSold += Int(sales.salesQuantity)
                }
                if let purchase = record.purchaseOrder {
                    if purchase.isOrderReceived {
                        totalPurchased += Int(purchase.purchaseQuantity)
                    } else {
                        yetToReceive += Int(purchase.purchaseQuantity)
                    }
                }
            }
            initialQuantity = Int(initialRecord?.netQuantity ?? 0)
            return [ .available : initialQuantity + totalPurchased - totalSold, .ordered : yetToReceive]
        }
    }
    
    var priceDetail: [ PriceType : Double] {
        get {
            var sellPrice = 0.0
            var costPrice = 0.0
            let records = itemRecordsArray
            let latestSales = records.last {
                $0.salesOrder != nil
            }
            let latestPurchase = records.last {
                $0.purchaseOrder != nil
            }
            sellPrice = latestSales?.salesOrder?.salesItemCost ?? 0.0
            costPrice = latestPurchase?.purchaseOrder?.purchaseItemCost ?? 0.0
            return [ .sellprice : sellPrice, .costprice : costPrice]
        }
    }
}
