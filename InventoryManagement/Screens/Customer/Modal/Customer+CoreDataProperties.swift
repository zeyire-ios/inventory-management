//
//  Customer+CoreDataProperties.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 29/09/22.
//
//

import Foundation
import CoreData


extension Customer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Customer> {
        return NSFetchRequest<Customer>(entityName: "Customer")
    }

    @NSManaged public var address: String?
    @NSManaged public var companyName: String
    @NSManaged public var customerID: UUID
    @NSManaged public var displayName: String
    @NSManaged public var email: String?
    @NSManaged public var mobile: String
    @NSManaged public var profileImage: UUID?
    @NSManaged public var remarks: String?
    @NSManaged public var salesOrders: NSSet?

    public var salesOrdersArray: [SalesOrder] {
        let set = salesOrders as? Set<SalesOrder> ?? []
        
        return set.sorted {
            $0.salesDate > $1.salesDate
        }
    }
}

// MARK: Generated accessors for salesOrders
extension Customer {

    @objc(addSalesOrdersObject:)
    @NSManaged public func addToSalesOrders(_ value: SalesOrder)

    @objc(removeSalesOrdersObject:)
    @NSManaged public func removeFromSalesOrders(_ value: SalesOrder)

    @objc(addSalesOrders:)
    @NSManaged public func addToSalesOrders(_ values: NSSet)

    @objc(removeSalesOrders:)
    @NSManaged public func removeFromSalesOrders(_ values: NSSet)

}

extension Customer : Identifiable {

}
