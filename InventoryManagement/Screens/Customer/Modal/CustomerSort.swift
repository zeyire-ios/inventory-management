//
//  ActorSort.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 11/10/22.
//

import Foundation

struct CustomerSort: SortOptions {
    func setSortBy(sortBy: Any) {
        CustomerSort._setSortBy(sortBy: sortBy as! SortBy)
    }
    
    func setSortOrder(ascending: Bool) {
        CustomerSort._setSortOrder(ascending: ascending)
    }
    
    static let sortBy: [SortBy] = [.displayName, .companyName]
    
    static let filterBy: [FilterBy] = [.all]
    
    enum SortBy: String {
        case displayName = "Customer Name"
        case companyName = "Company Name"
    }
    
    enum FilterBy: String {
        case all
    }
    
    static func checkSortOrder() -> Bool {
        if UserDefaults.standard.value(forKey: "CustomerAscending") == nil {
            UserDefaults.standard.set(true, forKey: "CustomerAscending")
        }
        return UserDefaults.standard.bool(forKey: "CustomerAscending")
    }
    
    static func _setSortOrder(ascending: Bool) {
        UserDefaults.standard.set(ascending, forKey: "CustomerAscending")
    }
    
    static func checkSortBy() -> SortBy {
        if UserDefaults.standard.value(forKey: "CustomerSortBy") == nil {
            UserDefaults.standard.set(SortBy.displayName.rawValue, forKey: "CustomerSortBy")
        }
        return SortBy.init(rawValue: (UserDefaults.standard.string(forKey: "CustomerSortBy"))!)!
        
    }
    
    static func _setSortBy(sortBy: SortBy) {
        UserDefaults.standard.set(sortBy.rawValue, forKey: "CustomerSortBy")
    }
    
}
