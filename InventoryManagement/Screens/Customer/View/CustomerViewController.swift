//
//  CustomerViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 03/09/22.
//

import UIKit
import CoreData

class CustomerViewController: UIViewController {
    
    lazy private var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.register(ActorListCell.self, forCellReuseIdentifier: ActorListCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.backgroundColor = .systemGroupedBackground
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy private var createCustomerButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Add Customer"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(addButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let noSearchViewImage = UIImage(named: "SearchProfile")?.withTintColor(.secondaryLabel)
    
    private let noCustomerViewImage = UIImage(named: "Group")?.withTintColor(.secondaryLabel)
    
    private let searchResultText = "Sorry, No Search Results!"
    
    private let zeroCustomertext = "Sorry, No Customers Added!"
    
    lazy var searchBar = UISearchController()
    
    private var sortBy: CustomerSort.SortBy {
        CustomerSort.checkSortBy()
    }
    
    private var ascending: Bool {
        CustomerSort.checkSortOrder()
    }
    
    private var timer = Timer()
    
    private var customerListDisplayModals: [CustomerListDisplayModal]?
    
    weak var splitDelegate: SplitControllerDelegate?
    
    private let CDM = CoreDataManager.shared
    
    private var firstTime: Bool = true {
        didSet {
            selectFirstRowInSplitView()
        }
    }
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGroupedBackground

        setupConfig()
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
        
        addNotificationObservers()
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
//        print("deinit")
    }
    
    private func setupConfig() {
        
        tableViewConfig()
        splitViewConfig()
        navigationConfig()
        backgroundViewConfig()
        
        createRefreshControl()
        
    }
    
    func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(createCustomerButton)
        createCustomerButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: tableView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            
            backgroundView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            backgroundView.topAnchor.constraint(greaterThanOrEqualTo: tableView.topAnchor),
            backgroundView.bottomAnchor.constraint(lessThanOrEqualTo: tableView.bottomAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            backgroundView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            createCustomerButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createCustomerButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
        ])
    }
    
    private func splitViewConfig() {
        
        splitViewController?.delegate = self
        splitViewController?.preferredPrimaryColumnWidthFraction = 0.4
        
    }
    
    func navigationConfig() {
        navigationItem.title = "Customers"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barTintColor = .systemGroupedBackground
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.searchController = self.searchBar
        navigationItem.searchController?.searchBar.placeholder = "Search Customer"
        navigationItem.searchController?.searchBar.delegate = self
        navigationItem.searchController?.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed(_:))), UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(sortButtonPressed)) ]
    }
    
    private func backgroundViewConfig() {
        backgroundView.isHidden = true
        createCustomerButton.isHidden = true
    }
    
    // MARK: - Functions
    
    /// Fetches data from the CoreData and copies it to the view modal
    /// - Parameters:
    ///   - searchTerm: A optional String that represents the search term for searching
    ///   - sortBy: A enum that represents by which factor sorting is performed
    ///   - ascending: A bool value that represents whether to sort Ascending or Descending
    func fetchToViewModal(searchFor searchTerm: String? = nil, sortBy: CustomerSort.SortBy, ascending: Bool) {
        DispatchQueue.global().async {
            [weak self] in
            let secondary: CustomerSort.SortBy = sortBy == .displayName ? .companyName : .displayName
            var predicates: [NSPredicate] = []
            if let searchTerm = searchTerm {
                predicates.append(NSPredicate(format: "(displayName CONTAINS[c] %@) OR (companyName CONTAINS[c] %@)", searchTerm, searchTerm))
            }
            let customerCoreDataList = self?.CDM.fetchCustomer(for: nil, with: ["customerID", "displayName", "companyName", "profileImage", "mobile"], check: predicates, sortBy: [NSSortDescriptor(key: self?.convertToCoreDataAttribute(sortBy), ascending: ascending), NSSortDescriptor(key: self?.convertToCoreDataAttribute(secondary), ascending: ascending)])
            self?.customerListDisplayModals = customerCoreDataList?.map({
                customer in
                CustomerListDisplayModal(customerID: customer.customerID, displayName: customer.displayName, companyName: customer.companyName, profileImage: customer.profileImage, mobile: Int(customer.mobile))
            })
            DispatchQueue.main.async {
                self?.showBackgroundFor(emptyList: (customerCoreDataList?.isEmpty)!, withSearchTerm: searchTerm != nil)
                self?.tableView.reloadData()
                if self?.firstTime ?? false {
                    self?.firstTime = false
                }
                
            }
        }
    }
    
    private func showBackgroundFor(emptyList: Bool, withSearchTerm: Bool) {
        if emptyList && withSearchTerm {
            createCustomerButton.isHidden = false
            backgroundView.isHidden = true
            backgroundView.titleLabel.text = searchResultText
            backgroundView.noDataImage.image = noSearchViewImage
        } else {
            createCustomerButton.isHidden = false
            backgroundView.isHidden = false
            backgroundView.titleLabel.text = zeroCustomertext
            backgroundView.noDataImage.image = noCustomerViewImage
        }
    }
    
    private func selectFirstRowInSplitView() {
        if traitCollection.userInterfaceIdiom == .pad {
            if let id = customerListDisplayModals?.first?.customerID {
                let vc = CustomerDetailVC(id: id)
                splitDelegate?.itemSelected(present: vc)
            }
        }
    }
    
    private func convertToCoreDataAttribute(_ attribute: CustomerSort.SortBy) -> String {
        switch (attribute) {
        case .displayName:
            return "displayName"
        case .companyName:
            return "companyName"
        }
    }
    
    private func createRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func deleteRefreshContorl() {
        tableView.refreshControl = nil
    }
   
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.reloadCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.deleteCustomer, object: nil)
    }

    // MARK: - Objc Functions
    
    @objc
    private func pullToRefresh(_ sender: UIRefreshControl) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
        sender.endRefreshing()
    }

    @objc
    private func reloadModal() {
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
    }
    
    @objc func addButtonPressed(_ sender: UIBarButtonItem) {
        let vc = UINavigationController(rootViewController: AddCustomerVC())
        vc.modalPresentationStyle = .formSheet
        present(vc, animated: true)
    }
    
    @objc func sortButtonPressed(_ sender: UIBarButtonItem) {
        present(UINavigationController(rootViewController: SortViewController<CustomerSort.SortBy, CustomerSort.FilterBy>(sortModal: CustomerSort() as SortOptions, sortBy: CustomerSort.sortBy, sortedBy: sortBy, ascending: ascending, filterBy: nil, filter: nil, completion: {
            [weak self] in
            self?.fetchToViewModal(sortBy: (self?.sortBy) ?? CustomerSort.SortBy.displayName, ascending: (self?.ascending) ?? true)
        })), animated: true)
    }
    
}

extension CustomerViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if customerListDisplayModals?.count == 0 {
            tableView.isScrollEnabled = false
            return 0
        } else {
            backgroundViewConfig()
            tableView.isScrollEnabled = true
            return customerListDisplayModals?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let customer = customerListDisplayModals?[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorListCell.identifier, for: indexPath) as! ActorListCell
            
            cell.setNamesFor(
                actor: customer.displayName ?? "Unknown Customer",
                company: customer.companyName ?? "Unknown Company"
            )
            cell.setImage(imageName: customer.profileImage, for: .CustomerImage)
            
            return cell
        }
        return UITableViewCell()
    }
    
}

extension CustomerViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = customerListDisplayModals?[indexPath.row].customerID else { return }
        let vc = CustomerDetailVC(id: id)
        vc.hidesBottomBarWhenPushed = true
        splitDelegate?.itemSelected(present: vc)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if let mobileURL = URL(string: "tel://\((customerListDisplayModals?[indexPath.row].mobile)!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(mobileURL)) {
                application.open(mobileURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var callAction = UIContextualAction()
        
        callAction = UIContextualAction(style: .normal, title: "Call" ) {
            [weak self] (action, view, success) in
            if let mobileURL = URL(string: "tel://\((self?.customerListDisplayModals?[indexPath.row].mobile)!)") {
                let application: UIApplication = UIApplication.shared
                if (application.canOpenURL(mobileURL)) {
                    application.open(mobileURL, options: [:], completionHandler: nil)
                    success(true)
                }
            }
        }
        callAction.image = UIImage(systemName: "phone.fill")
        callAction.backgroundColor = .systemPurple
        return UISwipeActionsConfiguration(actions: [callAction])
    }
}

extension CustomerViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {
            [weak self] timer in
            if searchText.isEmpty {
                self?.fetchToViewModal(sortBy: self?.sortBy ?? CustomerSort.SortBy.displayName, ascending: self?.ascending ?? true)
            } else {
                self?.fetchToViewModal(searchFor: searchText,sortBy: self?.sortBy ?? CustomerSort.SortBy.displayName, ascending: self?.ascending ?? true)
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        timer.invalidate()
        fetchToViewModal(searchFor: searchBar.text, sortBy: sortBy, ascending: ascending)
    }
}

extension CustomerViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }
    
}

extension CustomerViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        deleteRefreshContorl()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        createRefreshControl()
    }
    
}
