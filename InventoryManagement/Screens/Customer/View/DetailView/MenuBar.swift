//
//  MenuBar.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 30/09/22.
//

import UIKit

protocol MenuBarDelegate {
    func menuBar(selectedIndex index: Int) -> ()
}

#warning("Not touched this file")
class MenuBar: UIView {

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    var horizontalBar = UIView()
    
    var delegate: MenuBarDelegate?
    
    var barStartPoint: NSLayoutConstraint?
    
    let selectedIndexPath = IndexPath(item: 0, section: 0)
    
    lazy var cellWidth = frame.width
    
    var selectedItem: Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func commonInit() {
        collectionViewConfig()
        collectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: .left)
        horizontalBarConfig()
    }

    private func collectionViewConfig() {
        collectionView.register(SegmentCell.self, forCellWithReuseIdentifier: SegmentCell.identifier)
        addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 3),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
    private func horizontalBarConfig() {
        addSubview(horizontalBar)
        horizontalBar.translatesAutoresizingMaskIntoConstraints = false
        horizontalBar.backgroundColor = .systemBlue
        barStartPoint = horizontalBar.leadingAnchor.constraint(equalTo: leadingAnchor)
        barStartPoint?.isActive = true
        NSLayoutConstraint.activate([
            horizontalBar.bottomAnchor.constraint(equalTo: bottomAnchor),
            horizontalBar.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/2 ),
            horizontalBar.heightAnchor.constraint(equalToConstant: 3),
        ])
    }
    
    func setBackground(color: UIColor) {
        backgroundColor = color
        collectionView.backgroundColor = color
        
    }
    
    func changeBarPosition() {
        let x = CGFloat(selectedItem ?? 0) * (cellWidth) / 2
        barStartPoint?.constant = x
//        UIView.animate(withDuration: 0.50, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            self.layoutIfNeeded()
//        }, completion: nil)
    }
}

extension MenuBar: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SegmentCell.identifier, for: indexPath) as! SegmentCell
        switch ( indexPath.item) {
        case 0:
            cell.title.text = "Info"
        case 1:
            cell.title.text = "Transactions"
            
        default:
            cell.title.text = ""
        }
        return cell
    }
}

extension MenuBar: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.cellWidth / 2), height: 40)
//        return UICollectionViewFlowLayout.automaticSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedItem = indexPath.item
        let x = CGFloat(selectedItem ?? indexPath.item) * (cellWidth) / 2
        barStartPoint?.constant = x
        delegate?.menuBar(selectedIndex: indexPath.item)
        UIView.animate(withDuration: 0.50, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.layoutIfNeeded()
        }, completion: nil)
    }
}
