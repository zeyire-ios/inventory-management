//
//  ActorProfileView.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 29/09/22.
//

import UIKit

protocol ActorProfileDelegate: AnyObject {
    
    func sendMail()
    func makeCall()
    func sendMessage()
    
}

final class ActorProfileCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.ActorProfileCell"
    
    private let topView = UIView()
    
    lazy private var imageHeight: CGFloat = 50
    
    lazy private var profileImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.image = UIImage(systemName: "photo")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = self.imageHeight / 2
        imageView.tintColor = .quaternaryLabel
        return imageView
    }()
    
    private let activityIndicator: UIActivityIndicatorView  = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let titleView = UIView()
    
    private let profileTitle: UILabel = {
        var label = UILabel()
        label.text = "N"
        label.numberOfLines = 2
        label.font = UIFont.preferredFont(forTextStyle: .title2)
        return label
    }()
    
    private let companySymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 14, weight: .regular, scale: .medium)
        var image = UIImage(systemName: "building.2", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    
    private let companyTitle: UILabel = {
        var label = UILabel()
        label.text = "N"
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private let actionStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 24
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)
        stackView.layer.cornerRadius = 20
        stackView.layer.masksToBounds = false
        return stackView
    }()
    
    private let callButton: UIButton = {
        let button = UIButton()
        var config = UIButton.Configuration.tinted()
        config.image = UIImage(systemName: "phone.fill")
        config.imagePlacement = .all
        config.buttonSize = .large
        config.cornerStyle = .medium
        config.imagePadding = 5
        config.preferredSymbolConfigurationForImage = UIImage.SymbolConfiguration(scale: .medium)
        button.configuration = config
        button.addTarget(self, action: #selector(handleCall), for: .touchUpInside)
        return button
    }()
    
    private let messageButton: UIButton = {
        let button = UIButton()
        var config = UIButton.Configuration.tinted()
        config.image = UIImage(systemName: "message.fill")
        config.imagePlacement = .all
        config.buttonSize = .large
        config.cornerStyle = .medium
        config.imagePadding = 5
        config.preferredSymbolConfigurationForImage = UIImage.SymbolConfiguration(scale: .medium)
        button.configuration = config
        button.addTarget(self, action: #selector(handleMessage), for: .touchUpInside)
        return button
    }()
    
    private let mailButton: UIButton = {
        let button = UIButton()
        var config = UIButton.Configuration.tinted()
        config.image = UIImage(systemName: "envelope.fill")
        config.imagePlacement = .all
        config.buttonSize = .large
        config.cornerStyle = .medium
        config.imagePadding = 5
        config.preferredSymbolConfigurationForImage = UIImage.SymbolConfiguration(scale: .medium)
        button.configuration = config
        button.addTarget(self, action: #selector(handleMail), for: .touchUpInside)
        return button
    }()
    
    weak var actionDelegate: ActorProfileDelegate?
    
    override func setupCell() {
        configurations()
    }
    
    private func configurations() {
        titleViewConfig()
        topViewConfig()
        actionStackConfig()
        mainViewConfig()
    }
    
    private func titleViewConfig() {
        titleView.addSubview(profileTitle)
        profileTitle.translatesAutoresizingMaskIntoConstraints = false
        titleView.addSubview(companySymbol)
        companySymbol.translatesAutoresizingMaskIntoConstraints = false
        titleView.addSubview(companyTitle)
        companyTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            profileTitle.topAnchor.constraint(equalTo: titleView.topAnchor, constant: 8),
            profileTitle.leadingAnchor.constraint(equalTo: titleView.leadingAnchor, constant: 8),
            profileTitle.bottomAnchor.constraint(equalTo: companyTitle.topAnchor, constant: -4),
            profileTitle.trailingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: -8),
            
            companySymbol.leadingAnchor.constraint(equalTo: titleView.leadingAnchor, constant: 8),
            companySymbol.trailingAnchor.constraint(lessThanOrEqualTo: companyTitle.leadingAnchor, constant: -4),
            companySymbol.centerYAnchor.constraint(equalTo: companyTitle.centerYAnchor),
            
            companyTitle.topAnchor.constraint(equalTo: profileTitle.bottomAnchor, constant: 4),
            companyTitle.leadingAnchor.constraint(equalTo: companySymbol.trailingAnchor, constant: 4),
            companyTitle.bottomAnchor.constraint(equalTo: titleView.bottomAnchor, constant: -8),
            companyTitle.trailingAnchor.constraint(lessThanOrEqualTo: titleView.trailingAnchor, constant: -8),
        ])
    }
    
    private func topViewConfig() {
        topView.addSubview(profileImageView)
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(titleView)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            profileImageView.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 8),
            profileImageView.trailingAnchor.constraint(equalTo: titleView.leadingAnchor, constant: -4),
            profileImageView.heightAnchor.constraint(equalToConstant: imageHeight),
            profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor),
            profileImageView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: profileImageView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: profileImageView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: profileImageView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: profileImageView.trailingAnchor),
            
            titleView.topAnchor.constraint(equalTo: topView.topAnchor),
            titleView.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor),
            titleView.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 4),
            titleView.bottomAnchor.constraint(equalTo: topView.bottomAnchor),
            titleView.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -8),
            
        ])
    }
    
    private func actionStackConfig() {
        actionStack.addArrangedSubview(callButton)
        actionStack.addArrangedSubview(messageButton)
        actionStack.addArrangedSubview(mailButton)
    }
    
    private func mainViewConfig() {
        contentView.addSubview(topView)
        topView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(actionStack)
        actionStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            topView.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 16),
            topView.bottomAnchor.constraint(equalTo: actionStack.topAnchor, constant: -8),
            topView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -16),
            topView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            actionStack.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 8),
            actionStack.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 16),
            actionStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            actionStack.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -16),
            actionStack.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        ])
    }
    
    func disableMail() {
        mailButton.isEnabled = false
    }
    
    func enableMail() {
        mailButton.isEnabled = true
    }
    
    func setTitle(forActor displayName: String, forCompany companyName: String) {
        profileTitle.text = displayName
        companyTitle.text = companyName
    }
    
    func setImage(imageName: UUID?, for actorType: ImageType ) {
        DispatchQueue.main.async {
            [weak self] in
            self?.activityIndicator.startAnimating()
        }
        DispatchQueue.global().sync {
            [weak self] in
            var imagePath: URL
            guard let imageName = imageName?.uuidString else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.profileImageView.image = UIImage(systemName: "person.circle")
                    self?.profileImageView.contentMode = .scaleAspectFit
                }
                return
            }
            if actorType == .VendorImage {
                imagePath = URLStore.vendorImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            } else {
                imagePath = URLStore.customerImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            }
            do {
                let data = try Data(contentsOf: imagePath)
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.profileImageView.image = UIImage(data: data)
                    self?.profileImageView.contentMode = .scaleAspectFill
                }
            } catch let error {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.profileImageView.image = UIImage(systemName: "person.circle")
                    self?.profileImageView.contentMode = .scaleAspectFit
                }
                print("\n\nActorProfileCell -> Loading Image to ImageView:\n\(error)\n\n")
            }
        }
    }
    
    @objc
    private func handleCall() {
        actionDelegate?.makeCall()
    }
    
    @objc
    private func handleMessage() {
        actionDelegate?.sendMessage()
    }
    
    @objc
    private func handleMail() {
        actionDelegate?.sendMail()
    }
    
}
