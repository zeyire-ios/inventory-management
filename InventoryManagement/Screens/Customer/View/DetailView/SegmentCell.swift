//
//  SegmentCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 30/09/22.
//

import UIKit

#warning("Not touched this file")
class SegmentCell: BaseCollectionViewCell {
    
    static let identifier = "com.abilash.SegmentCell"
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.text = "Segment"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textAlignment = .center
        return label
    }()
    
    var horizontalBar = UIView()
    
    override var isHighlighted: Bool{
        didSet{
            title.textColor = isHighlighted ? .systemBlue : .label
        }
    }
    
    override var isSelected: Bool{
        didSet {
            title.textColor = isSelected ? .systemBlue: .label
            
        }
    }
    
    override func commonInit() {
        segmentConfig()
    }
    
    private func segmentConfig() {
        contentView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor),
            title.centerXAnchor.constraint(equalTo: centerXAnchor),
            title.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
    
}
