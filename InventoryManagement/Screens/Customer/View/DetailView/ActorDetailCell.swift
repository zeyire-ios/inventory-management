//
//  ActorDetailCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 01/10/22.
//

import UIKit

final class ActorDetailCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.ActorDetailCell"
    
    lazy private var fieldView = UIView()
    
    private let fieldSymbol: UIImageView = {
        var imageView = UIImageView()
        return imageView
    }()
    
    private let fieldLabel: UILabel = {
        var label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        return label
    }()
    
    private let fieldText: UITextView = {
        var textView = UITextView()
        textView.isSelectable = true
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = UIFont.preferredFont(forTextStyle: .body)
        textView.backgroundColor = .clear
        textView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 4)
        return textView
    }()
    
    init(ismultiLine: Bool, style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        if ismultiLine {
            self.updateForMultiline()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupCell() {
        fieldConstraints()
        fieldViewConfig()
    }
    
    override func prepareForReuse() {
        fieldText.text = nil
        fieldText.dataDetectorTypes = .all
        fieldSymbol.image = nil
        fieldLabel.text = nil
    }
    
    private func fieldConstraints() {
        fieldView.addSubview(fieldSymbol)
        fieldSymbol.translatesAutoresizingMaskIntoConstraints = false
        fieldView.addSubview(fieldLabel)
        fieldLabel.translatesAutoresizingMaskIntoConstraints = false
        fieldView.addSubview(fieldText)
        fieldText.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            fieldSymbol.leadingAnchor.constraint(equalTo: fieldView.leadingAnchor, constant: 16),
            fieldSymbol.centerYAnchor.constraint(equalTo: fieldLabel.centerYAnchor),
            fieldSymbol.trailingAnchor.constraint(equalTo: fieldLabel.leadingAnchor, constant: -8),
            
            fieldLabel.topAnchor.constraint(equalTo: fieldView.topAnchor, constant: 4),
            fieldLabel.leadingAnchor.constraint(equalTo: fieldSymbol.trailingAnchor, constant: 8),
            fieldLabel.bottomAnchor.constraint(equalTo: fieldText.topAnchor, constant: 0),
            fieldLabel.trailingAnchor.constraint(equalTo: fieldView.trailingAnchor),
            
            fieldText.topAnchor.constraint(equalTo: fieldLabel.bottomAnchor),
            fieldText.leadingAnchor.constraint(equalTo: fieldLabel.leadingAnchor, constant: -5),
            fieldText.bottomAnchor.constraint(equalTo: fieldView.bottomAnchor),
            fieldText.trailingAnchor.constraint(equalTo: fieldView.trailingAnchor),
        ])
        fieldSymbol.setContentHuggingPriority(.init(251), for: .horizontal)
    }
    
    private func fieldViewConfig() {
        contentView.addSubview(fieldView)
        fieldView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            fieldView.topAnchor.constraint(equalTo: contentView.topAnchor),
            fieldView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            fieldView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            fieldView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        ])
    }
    
    func setLabel(image: UIImage?, label: String?) {
        fieldSymbol.image = image
        fieldLabel.text = label
    }
    
    func setViewModal(value: String? = nil, dataDetector: UIDataDetectorTypes = .all) {
        fieldText.text = value
        fieldText.dataDetectorTypes = dataDetector
    }
    
    func updateForMultiline() {
        fieldText.textContainer.maximumNumberOfLines = 5
        contentView.layoutIfNeeded()
    }
    
}
