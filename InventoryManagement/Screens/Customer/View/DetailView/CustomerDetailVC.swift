//
//  CustomerDetailVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 03/09/22.
//

import UIKit
import CoreData

class CustomerDetailVC: UIViewController {
    
    lazy private var customerMenu: UIMenu = {
        return UIMenu(title: "", image: nil, identifier: nil, options: .destructive, children: [topMenu, bottomMenu])
    }()
    
    lazy private var topMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        actions.append(UIAction(title: "Make Sales Order",
                                image: UIImage(systemName: "coloncurrencysign.circle"),
                                handler: {
            [weak self] action in
            self?.makeSalesOrder()
        }))
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    lazy private var bottomMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        actions.append(UIAction(title: "Delete Customer",
                                image: {
            let image = UIImage(systemName: "trash")
            return image?.withTintColor(.systemRed)
        }(),
                                attributes: .destructive,
                                handler: {
            [weak self] action in
            
            if self?.traitCollection.userInterfaceIdiom == .phone {
                
                let alert = UIAlertController(title: "", message: "Are you sure you want to delete this Item", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive){
                    action in
                    self?.deleteCustomer()
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                
                self?.present(alert, animated: true)
            } else if self?.traitCollection.userInterfaceIdiom == .pad {
                
                let alert = UIAlertController(title: "", message: "Are you sure you want to delete this Item", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Delete", style: .destructive){
                    action in
                    self?.deleteCustomer()
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                
                self?.present(alert, animated: true)
                
            }
        }))
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    lazy private var menuBar: MenuBar = {
        var menuBar = MenuBar()
        menuBar.delegate = self
        menuBar.setBackground(color: .systemGroupedBackground)
        return menuBar
    }()
    
    lazy private var mainTableView: UITableView = {
        var tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.register(ActorProfileCell.self, forCellReuseIdentifier: ActorProfileCell.identifier)
        tableView.register(ActorDetailCell.self, forCellReuseIdentifier: ActorDetailCell.identifier)
        tableView.register(ActorTransactionCell.self, forCellReuseIdentifier: ActorTransactionCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .systemGroupedBackground
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        let image = UIImage(named: "NoTaskNotepad")?.withTintColor(.secondaryLabel)
        noDataView.noDataImage.image = image
        noDataView.titleLabel.text = "Sorry, no sales are made!"
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy private var createSalesButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Make Sales Order"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(createSalesOrder), for: .touchUpInside)
        return button
    }()
    
    private let topView: ActorProfileCell = {
        var view = ActorProfileCell()
        view.layer.masksToBounds = true
        view.clipsToBounds = false
        return view
    }()
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .long
        return formatter
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    // MARK: - Properties
    
    private var selectedIndex: Int = -1 {
        didSet {
            mainTableView.reloadData()
        }
    }
    
    private var customerModal = CustomerDetailModal()
    
    private var customerTransactionModal = [CustomerSalesModal]()
    
    private let CDM = CoreDataManager.shared
    
    private var id: UUID
    
    // MARK: - Init
    
    init(id: UUID) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConfig()
        
        addNotificationObservers()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            menuBar.cellWidth = size.width
            menuBar.changeBarPosition()
            menuBar.collectionView.collectionViewLayout.invalidateLayout()
            menuBar.collectionView.layoutIfNeeded()
        } else if UIDevice.current.orientation.isPortrait {
            menuBar.cellWidth = size.width
            menuBar.changeBarPosition()
            menuBar.collectionView.collectionViewLayout.invalidateLayout()
            menuBar.collectionView.layoutIfNeeded()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupConfig() {
        view.backgroundColor = .systemGroupedBackground
        configuration()
        fetchDisplayModal()
    }
    
    private func configuration() {
        navigationConfig()
        menuBarConfig()
        backGroundViewConfig()
        mainTableViewConfig()
    }
    
    func navigationConfig() {
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(title: "", image: UIImage(systemName: "ellipsis.circle"), primaryAction: nil, menu: customerMenu),
            UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonPressed)),
        ]
    }
    
    private func menuBarConfig() {
        view.addSubview(menuBar)
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            menuBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            menuBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8),
            menuBar.heightAnchor.constraint(equalToConstant: 40),
            menuBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8),
        ])
    }
    
    private func backGroundViewConfig() {
        backgroundView.isHidden = true
        createSalesButton.isHidden = true
    }
    
    private func mainTableViewConfig() {
        view.addSubview(mainTableView)
        mainTableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(createSalesButton)
        createSalesButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainTableView.topAnchor.constraint(equalTo: menuBar.bottomAnchor),
            mainTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mainTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            
            backgroundView.centerYAnchor.constraint(equalTo: mainTableView.centerYAnchor),
            backgroundView.topAnchor.constraint(greaterThanOrEqualTo: mainTableView.topAnchor),
            backgroundView.bottomAnchor.constraint(lessThanOrEqualTo: mainTableView.bottomAnchor),
            backgroundView.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor),
            backgroundView.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            createSalesButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createSalesButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
        ])
    }
    
    // MARK: - Functions
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            if let customer = self?.CDM.fetchCustomer(for: self?.id, with: ["displayName", "companyName", "mobile", "email", "address", "remarks", "profileImage"])?.first {
                self?.customerModal = CustomerDetailModal(customerID: customer.customerID, displayName: customer.displayName, companyName: customer.companyName, mobile: customer.mobile, email: customer.email, address: customer.address, remarks: customer.remarks, profileImage: customer.profileImage)
                self?.fetchTransactions(only: false)
            }
            DispatchQueue.main.sync {
                if self?.selectedIndex == -1 {
                    self?.selectedIndex = 0
                }
                self?.mainTableView.reloadData()
            }
        }
    }
    
    private func fetchTransactions(only: Bool) {
        DispatchQueue.global().async {
            [weak self] in
            if let customer = self?.CDM.fetchCustomer(for: self?.id)?.first {
                self?.customerTransactionModal.removeAll(keepingCapacity: true)
                let _ = customer.salesOrdersArray.map({
                    sale in
                    self?.customerTransactionModal.append(CustomerSalesModal(item: sale.item, itemName: sale.item.itemName, itemCost: sale.salesItemCost, totalCost: sale.salesTotalCost, salesDate: sale.salesDate, itemImage: sale.item.itemImage, quantity: Int(sale.salesQuantity)))
                })
            }
            DispatchQueue.main.async {
                if only {
                    self?.mainTableView.reloadData()
                }
            }
        }
    }
    
    private func makeSalesOrder(item: Item? = nil) {
        present(UINavigationController(rootViewController: AddSOVC(item: item, customer: self.CDM.fetchCustomer(for: self.id)?.first)), animated: true)
    }
    
    func deleteCustomer() {
        let customer = CDM.fetchCustomer(for: self.id, with: ["displayName"])?.first
        DispatchQueue.global().async {
            ImageHelper.deleteImage(customer?.profileImage, for: .CustomerImage)
        }
        self.CDM.viewContext.delete(customer!)
        do {
            try CDM.viewContext.save()
        } catch {
            print("\n\nItemViewController: -> Saving to CoreData\n\(error)\n\n")
        }
        NotificationCenter.default.post(name: CustomerKey.deleteCustomer, object: nil)
        dismissView()
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.reloadCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTransaction), name: SOKey.reloadSO, object: nil)
    }
    
    private func handleCallAction() {
        if let mobileURL = URL(string: "tel://\(customerModal.mobile!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(mobileURL)) {
                application.open(mobileURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    private func handleMessageAction() {
        if let messageURL = URL(string: "sms://\(customerModal.mobile!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(messageURL)) {
                application.open(messageURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    private func handleEmailAction() {
        if let emailURL = URL(string: "mailto://\(customerModal.email!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(emailURL)) {
                application.open(emailURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    // MARK: - Objc Functions
    
    @objc
    private func editButtonPressed() {
        present(UINavigationController(rootViewController: AddCustomerVC(id: self.id)), animated: true)
    }
    
    @objc
    private func createSalesOrder(_ sender: UIButton) {
        makeSalesOrder()
    }
    
    @objc
    private func reloadModal() {
        fetchDisplayModal()
    }
    
    @objc
    private func reloadTransaction() {
        fetchTransactions(only: true)
    }
    
    @objc
    private func dismissView() {
        if traitCollection.userInterfaceIdiom == .phone {
            splitViewController?.hide(.secondary)
        } else if traitCollection.userInterfaceIdiom == .pad {
            let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
            vc?.setViewControllers([SplitViewController.blankVC], animated: true)
        }
    }
    
    
}

extension CustomerDetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedIndex == 0 {
            backGroundViewConfig()
            return 5
        } else {
            if customerTransactionModal.count == 0 && selectedIndex == 1 {
                backgroundView.isHidden = false
                createSalesButton.isHidden = false
                return 0
            } else {
                backGroundViewConfig()
                return customerTransactionModal.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0 {
            return 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch ( selectedIndex ) {
            
        case 0:
            // Profile Info Column
            switch (indexPath.section) {
            case 0:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ActorProfileCell.identifier, for: indexPath) as! ActorProfileCell
                cell.backgroundColor = .secondarySystemGroupedBackground
                
                cell.actionDelegate = self
                
                cell.setTitle(
                    forActor: customerModal.displayName ?? "Unknown Customer",
                    forCompany: customerModal.companyName ?? "Unknown Company"
                )
                
                cell.setImage(imageName: customerModal.profileImage, for: .CustomerImage)
                if let email = customerModal.email, !email.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    cell.enableMail()
                } else {
                    cell.disableMail()
                }
                
                return cell
                
            default:
                // Used default here as all other cells are common
                let cell = tableView.dequeueReusableCell(withIdentifier: ActorDetailCell.identifier, for: indexPath) as! ActorDetailCell
                
                let config = UIImage.SymbolConfiguration(pointSize: 14, weight: .regular, scale: .medium)
                cell.backgroundColor = .secondarySystemGroupedBackground
                switch (indexPath.section) {
                case 1:
                    cell.setLabel(
                        image: UIImage(systemName: "phone", withConfiguration: config),
                        label: "Mobile"
                    )
                    cell.setViewModal(
                        value: customerModal.mobile,
                        dataDetector: .phoneNumber
                    )
                    return cell
                case 2:
                    cell.setLabel(
                        image: UIImage(systemName: "envelope", withConfiguration: config),
                        label: "Email"
                    )
                    if let email = customerModal.email {
                        cell.setViewModal(
                            value: email,
                            dataDetector: .link
                        )
                    }
                   return cell
                case 3:
                    cell.setLabel(
                        image: UIImage(systemName: "map", withConfiguration: config),
                        label: "Address"
                    )
                    if let address = customerModal.address {
                        cell.setViewModal(
                            value: address,
                            dataDetector: .address
                        )
                    }
                    return cell
                case 4:
                    cell.setLabel(
                        image: UIImage(systemName: "note.text", withConfiguration: config),
                        label: "Remarks"
                    )
                    if let remarks = customerModal.remarks {
                        cell.setViewModal(
                            value: remarks
                        )
                    }
                    return cell
                default:
                    return UITableViewCell()
                }
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorTransactionCell.identifier, for: indexPath) as! ActorTransactionCell
            let sale = customerTransactionModal[indexPath.section]
            
            cell.setValuesFor(
                itemName: sale.itemName ?? "Item Not Found",
                itemPrice: currencyFormatter.string(from: (sale.itemCost ?? 0.0) as NSNumber),
                totalPrice: currencyFormatter.string(from: (sale.totalCost ?? 0.0) as NSNumber),
                date: dateFormatter.string(from: sale.salesDate ?? Date(timeIntervalSince1970: 0)),
                quantity: String(sale.quantity ?? 0)
            )
            
            cell.reorderDelegate = self
            cell.setImage(imageName: sale.itemImage)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension CustomerDetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
}

extension CustomerDetailVC: ActorProfileDelegate {
    func sendMail() {
        handleCallAction()
    }
    
    func makeCall() {
        handleCallAction()
    }
    
    func sendMessage() {
        handleMessageAction()
    }
    
}

extension CustomerDetailVC: MenuBarDelegate {
    
    func menuBar(selectedIndex index: Int) {
        selectedIndex = index
    }
    
}

extension CustomerDetailVC: ReorderDelegate {
    
    func reorder(_ cell: ActorTransactionCell) {
        guard let indexPath = mainTableView.indexPath(for: cell) else {
            return
        }
        let item = customerTransactionModal[indexPath.section].item
        makeSalesOrder(item: item)
        
    }
    
}
