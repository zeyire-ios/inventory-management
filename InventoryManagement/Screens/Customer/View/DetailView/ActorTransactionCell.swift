//
//  ActorTransactionCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 02/10/22.
//

import UIKit

final class ActorTransactionCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.ActorTransactionCell"
    
    private let mainStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        return stackView
    }()
    
    private let itemImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.image = UIImage(systemName: "photo")
        imageView.tintColor = .secondaryLabel
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let topStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 4
        stackView.alignment = .top
        stackView.distribution = .fill
        return stackView
    }()
    
    private let priceStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.alignment = .fill
        return stackView
    }()
    
    private let itemLabel: UILabel = {
        var label = UILabel()
        label.text = "Item Name"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private let itemPriceView = UIView()
    
    private let itemPriceSymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 14, weight: .regular, scale: .medium)
        var image = UIImage(named: "CashOut")
        var imageView = UIImageView(image: image)
        return imageView
    }()
    
    private let itemPriceLabel: UILabel = {
        var label = UILabel()
        label.text = "0.0"
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        return label
    }()
    
    private let totalPriceView = UIView()
    
    private let totalPriceSymbol: UIImageView = {
        var image = UIImage(named: "Receipt")
        var imageView = UIImageView(image: image)
        return imageView
    }()
    
    private let totalPriceLabel: UILabel = {
        var label = UILabel()
        label.text = "0.0"
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        return label
    }()
    
    private let bottomView = UIView()
    
    private let dateSymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 14, weight: .regular, scale: .medium)
        var image = UIImage(systemName: "calendar", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    
    private let dateLabel: UILabel = {
        var label = UILabel()
        label.text = "Year and Month"
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.tintColor = .secondaryLabel
        return label
    }()
    
    private let quantitySymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 14, weight: .semibold, scale: .medium)
        var image = UIImage(systemName: "shippingbox", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .label
        return imageView
    }()
    
    private let quantityLabel: UILabel = {
        var label = UILabel()
        label.text = "50"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private let reorderButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderedTinted()
        config.title = "Reorder Item"
        button.configuration = config
        button.addTarget(self, action: #selector(handleReorder), for: .touchUpInside)
        return button
    }()
    
    weak var reorderDelegate: ReorderDelegate?
    
    override func setupCell() {
        setConstraints()
    }
    
    private func setConstraints() {
        itemImageViewConfig()
        itemPriceViewConfig()
        totalPriceViewConfig()
        priceStackViewConfig()
        topStackViewConfig()
        bottomViewConfig()
        mainStackViewConfig()
    }
    
    private func itemImageViewConfig() {
        itemImageView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicator.topAnchor.constraint(equalTo: itemImageView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: itemImageView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: itemImageView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: itemImageView.trailingAnchor),
        ])
    }
    
    private func itemPriceViewConfig() {
        itemPriceView.addSubview(itemPriceSymbol)
        itemPriceSymbol.translatesAutoresizingMaskIntoConstraints = false
        itemPriceView.addSubview(itemPriceLabel)
        itemPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemPriceSymbol.leadingAnchor.constraint(equalTo: itemPriceView.leadingAnchor),
            itemPriceSymbol.trailingAnchor.constraint(equalTo: itemPriceLabel.leadingAnchor, constant: -4),
            itemPriceSymbol.centerYAnchor.constraint(equalTo: itemPriceLabel.centerYAnchor),
            itemPriceSymbol.heightAnchor.constraint(equalToConstant: 14),
            itemPriceSymbol.widthAnchor.constraint(equalTo: itemPriceSymbol.heightAnchor),
            
            itemPriceLabel.topAnchor.constraint(equalTo: itemPriceView.topAnchor),
            itemPriceLabel.leadingAnchor.constraint(equalTo: itemPriceSymbol.trailingAnchor, constant: 4),
            itemPriceLabel.bottomAnchor.constraint(equalTo: itemPriceView.bottomAnchor),
            itemPriceLabel.trailingAnchor.constraint(equalTo: itemPriceView.trailingAnchor, constant: 8),
        ])
    }
    
    private func totalPriceViewConfig() {
        totalPriceView.addSubview(totalPriceSymbol)
        totalPriceSymbol.translatesAutoresizingMaskIntoConstraints = false
        totalPriceView.addSubview(totalPriceLabel)
        totalPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            totalPriceSymbol.leadingAnchor.constraint(equalTo: totalPriceView.leadingAnchor),
            totalPriceSymbol.trailingAnchor.constraint(equalTo: totalPriceLabel.leadingAnchor, constant: -4),
            totalPriceSymbol.centerYAnchor.constraint(equalTo: totalPriceLabel.centerYAnchor),
            totalPriceSymbol.heightAnchor.constraint(equalToConstant: 14),
            totalPriceSymbol.widthAnchor.constraint(equalTo: totalPriceSymbol.heightAnchor),
            
            totalPriceLabel.topAnchor.constraint(equalTo: totalPriceView.topAnchor),
            totalPriceLabel.leadingAnchor.constraint(equalTo: totalPriceSymbol.trailingAnchor, constant: 4),
            totalPriceLabel.bottomAnchor.constraint(equalTo: totalPriceView.bottomAnchor),
            totalPriceLabel.trailingAnchor.constraint(equalTo: totalPriceView.trailingAnchor, constant: 8),
        ])
    }
    
    private func priceStackViewConfig() {
        priceStackView.addArrangedSubview(itemLabel)
        priceStackView.addArrangedSubview(itemPriceView)
        priceStackView.addArrangedSubview(totalPriceView)
    }
    
    private func topStackViewConfig() {
        topStackView.addArrangedSubview(itemImageView)
        topStackView.addArrangedSubview(priceStackView)
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemImageView.heightAnchor.constraint(equalToConstant: 60),
            itemImageView.widthAnchor.constraint(equalToConstant: 80),
        ])
    }
    
    private func bottomViewConfig() {
        bottomView.addSubview(dateSymbol)
        dateSymbol.translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(dateLabel)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(quantitySymbol)
        quantitySymbol.translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(quantityLabel)
        quantityLabel.translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(reorderButton)
        reorderButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dateSymbol.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor, constant: 8),
            dateSymbol.trailingAnchor.constraint(equalTo: dateLabel.leadingAnchor, constant: -4),
            dateSymbol.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor),
            
            dateLabel.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 8),
            dateLabel.leadingAnchor.constraint(equalTo: dateSymbol.trailingAnchor, constant: 4),
            dateLabel.bottomAnchor.constraint(equalTo: reorderButton.topAnchor, constant: -12),
            dateLabel.trailingAnchor.constraint(lessThanOrEqualTo: quantitySymbol.leadingAnchor, constant: -8),
            
            quantitySymbol.leadingAnchor.constraint(greaterThanOrEqualTo: dateLabel.trailingAnchor, constant: 8),
            quantitySymbol.trailingAnchor.constraint(equalTo: quantityLabel.leadingAnchor, constant: -4),
            quantitySymbol.centerYAnchor.constraint(equalTo: quantityLabel.centerYAnchor),
            
            quantityLabel.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor),
            quantityLabel.leadingAnchor.constraint(equalTo: quantitySymbol.trailingAnchor, constant: 4),
            quantityLabel.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor, constant: -8),
            
            reorderButton.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 12),
            reorderButton.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor, constant: 8),
            reorderButton.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor, constant: -8),
            reorderButton.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor, constant: -8),
        ])
        dateSymbol.setContentHuggingPriority(.init(260), for: .horizontal)
        
    }
    
    private func mainStackViewConfig() {
        contentView.addSubview(mainStackView)
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
        ])
        
        mainStackView.addArrangedSubview(topStackView)
        mainStackView.addArrangedSubview(bottomView)
    }
    
    @objc
    private func handleReorder() {
        reorderDelegate?.reorder(self)
    }
    
    func setValuesFor(itemName: String?, itemPrice: String?, totalPrice: String?, date: String?, quantity: String?) {
        itemLabel.text = itemName
        itemPriceLabel.text = itemPrice
        totalPriceLabel.text = totalPrice
        dateLabel.text = date
        quantityLabel.text = quantity
    }
    
    func setImage(imageName: UUID?) {
        DispatchQueue.main.async {
            [weak self] in
            self?.activityIndicator.startAnimating()
        }
        DispatchQueue.global().sync {
            [weak self] in
            guard let imageName = imageName?.uuidString else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "photo")
                    self?.itemImageView.contentMode = .scaleAspectFit
                }
                return
            }
            let imagePath = URLStore.itemImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            do {
                let data = try Data(contentsOf: imagePath)
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(data: data)
                    self?.itemImageView.contentMode = .scaleAspectFill
                }
            } catch let error {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.itemImageView.image = UIImage(systemName: "person.circle")
                    self?.itemImageView.contentMode = .scaleAspectFit
                }
                print("\n\nCustomerDetailVC -> Loading Image to ImageView:\n\(error)\n\n")
            }
        }
    }
    
}

protocol ReorderDelegate: AnyObject {
    
    func reorder(_ cell: ActorTransactionCell)
    
}
