//
//  AddCustomerVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 04/09/22.
//

import UIKit
import Photos
import PhotosUI
import CoreData

class AddCustomerVC: UIViewController {
    
    lazy private var tableView: UITableView = {
        var tableView = UITableView(frame: CGRect(), style: .insetGrouped)
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.keyboardDismissMode = .interactive
        tableView.register(TextFieldCell.self, forCellReuseIdentifier: TextFieldCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()

    private let pickImage: UIImageView = {
        let imageConfig = UIImage.SymbolConfiguration(pointSize: 80, weight: .bold, scale: .medium)
        let image = UIImage(systemName: "person.circle.fill", withConfiguration: imageConfig)
        let imageView = UIImageView(image: image)
        imageView.tintColor = .systemGray3
        imageView.layer.borderWidth = 0
        imageView.layer.masksToBounds = false
        imageView.layer.cornerRadius = 80 / 2
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()

    private let pickImageButton: UIButton = {
        let button = UIButton()
        button.setTitle("Add Photo", for: .normal)
        button.setTitleColor(UIColor.systemBlue, for: .normal)
        return button
    }()

    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()

    private let customerFormList = ActorFormList()
    
    private var customerModal = CustomerDetailModal()
    
    private var doneButton: Bool = false {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = doneButton
        }
    }
    
    private let CDM = CoreDataManager.shared

    private var id: UUID?
    
    private var selectedImage: UIImage?
    
    // MARK: - Init
    
    init(id: UUID? = nil) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        isModalInPresentation = true
        navigationController?.presentationController?.delegate = self
        
        if let _ = self.id {
            fetchDisplayModal()
        }
        
        setupConfig()
        
        scrollViewAboveKeyboard()
//        SOGenerator.checkPersistance()
//        POGenerator.checkPersistance()
    }
    
    private func setupConfig() {
        tableViewConfig()
        navigationConfig()
        imageViewConfig()
    }
    
    private func tableViewConfig() {
        view.addSubview(pickImage)
        pickImage.translatesAutoresizingMaskIntoConstraints = false
        pickImage.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pickImageButton)
        pickImageButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pickImage.widthAnchor.constraint(equalToConstant: 80),
            pickImage.heightAnchor.constraint(equalTo: pickImage.widthAnchor),
            pickImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pickImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            
            activityIndicator.topAnchor.constraint(equalTo: pickImage.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: pickImage.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: pickImage.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: pickImage.trailingAnchor),
            
            pickImageButton.centerXAnchor.constraint(equalTo: pickImage.centerXAnchor),
            pickImageButton.topAnchor.constraint(equalTo: pickImage.bottomAnchor, constant: 8),
            pickImageButton.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8),
            pickImageButton.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8),
            
            tableView.topAnchor.constraint(equalTo: pickImageButton.bottomAnchor, constant: 8),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
        pickImageButton.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        pickImageButton.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func imageViewConfig() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(activateImagePicker(_:)))
        pickImage.addGestureRecognizer(tap)
        pickImageButton.addTarget(self, action: #selector(activateImagePicker(_:)), for: .touchUpInside)
    }
    
    private func navigationConfig() {
        navigationItem.title = self.id != nil ? "Edit Customer" : "Add Customer"
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(_:)))
        navigationItem.rightBarButtonItem?.isEnabled = false
        doneButton = self.id != nil ? true : false
    }
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            if let customer = self?.CDM.fetchCustomer(for: self?.id)?.first {
                self?.customerModal = CustomerDetailModal(customerID: customer.customerID, displayName: customer.displayName, companyName: customer.companyName, mobile: customer.mobile, email: customer.email, address: customer.address, remarks: customer.remarks, profileImage: customer.profileImage)
            }
            DispatchQueue.main.async {
                self?.updatePickImage()
            }
        }
    }
    
    private func updatePickImage() {
        activityIndicator.startAnimating()
        DispatchQueue.global().async {
            [weak self] in
            guard let imageName = self?.customerModal.profileImage?.uuidString else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.pickImage.image = UIImage(systemName: "person")
                    self?.pickImage.contentMode = .scaleAspectFit
                }
                return
            }
            let imagePath = URLStore.customerImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            do {
                let data = try Data(contentsOf: imagePath)
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.pickImage.image = UIImage(data: data)
                    self?.pickImage.contentMode = .scaleAspectFill
                }
            } catch let error {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.pickImage.image = UIImage(systemName: "person")
                    self?.pickImage.contentMode = .scaleAspectFit
                }
                print("\n\nAddOrEditCustomer -> Loading Image to ImageView:\n\(error)\n\n")
            }
        }
        DispatchQueue.main.async {
            [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    private func validateModal() -> Bool {
        guard let displayName = customerModal.displayName, let companyName = customerModal.companyName, let mobile = customerModal.mobile, !displayName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty, !companyName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty, !mobile.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            return false
        }
        if let email = customerModal.email, !email.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            if isValid(mobile: mobile) && isValid(email: email) {
                return true
            } else {
                return false
            }
        } else if isValid(mobile: mobile) {
            return true
        } else {
            return false
        }
    }
    
    private func saveData() {
        if let id = self.id {
            DispatchQueue.global().async {
                [weak self] in
                guard let customer = self?.CDM.fetchCustomer(for: id)?.first, let displayName = self?.customerModal.displayName, let companyName = self?.customerModal.companyName, let mobile = self?.customerModal.mobile else { return }
                do {
                    customer.setValue(displayName, forKey: "displayName")
                    customer.setValue(companyName, forKey: "companyName")
                    customer.setValue(mobile, forKey: "mobile")
                    customer.setValue(self?.customerModal.email, forKey: "email")
                    customer.setValue(self?.customerModal.address, forKey: "address")
                    customer.setValue(self?.customerModal.remarks, forKey: "remarks")
                    if let image = self?.selectedImage {
                        if let imageName = customer.profileImage?.uuidString {
                            ImageHelper.writeImage(image, for: .CustomerImage, to: URLStore.vendorImageURL, with: imageName)
                        } else {
                            customer.profileImage = UUID()
                            let imageName = customer.profileImage?.uuidString
                            ImageHelper.writeImage(image, for: .CustomerImage, to: URLStore.customerImageURL, with: imageName!)
                        }
                    }
                    try self?.CDM.viewContext.save()
                } catch let error {
                    print("\n\nEditCustomerVC: -> Saving Customer Data:\n\(error)\n\n")
                }
            }
        } else {
            let customer = Customer(context: self.CDM.viewContext)
            customer.customerID = UUID()
            customer.displayName = customerModal.displayName!
            customer.companyName = customerModal.companyName!
            customer.mobile = customerModal.mobile!
            customer.email = customerModal.email
            customer.address = customerModal.address
            customer.remarks = customerModal.remarks
            do {
                if let image = selectedImage {
                    customer.profileImage = UUID()
                    guard let imageName = customer.profileImage?.uuidString else { fatalError() }
                    ImageHelper.writeImage(image, for: .CustomerImage, to: URLStore.customerImageURL, with: imageName)
                }
                try self.CDM.viewContext.save()
            } catch let error {
                print("\n\nAddCustomerVC: ->> Saving Customer Data:\n\(error)\n\n")
            }
        }
        NotificationCenter.default.post(name: CustomerKey.reloadCustomer, object: nil)
    }
    
    private func scrollViewAboveKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    private func isValid(email: String) -> Bool {
      do {
        let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let range = NSRange(location: 0, length: email.count)
        let matches = detector.matches(in: email, options: .anchored, range: range)
        guard matches.count == 1 else { return false }
        return matches[0].url?.scheme == "mailto"
      } catch {
        return false
      }
    }
    
    private func isValid(mobile: String) -> Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let range = NSRange(location: 0, length: mobile.count)
            let matches = detector.matches(in: mobile, options: [], range: range)
            if let res = matches.first {
                return res.resultType == .phoneNumber && range.location == 0 && range.length == mobile.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    @objc
    private func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @objc
    private func doneButtonPressed(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        if validateModal() {
            DispatchQueue.global().async {
                [weak self] in
                self?.saveData()
            }
            dismiss(animated: true)
        }
    }
    
    // Using PhotoPicker from Photo Kit
    @objc
    private func activateImagePicker(_ sender: UITapGestureRecognizer) {
        var config = PHPickerConfiguration(photoLibrary: .shared())
        config.selectionLimit = 1
        config.filter = .images
        config.selection = .default
        let imagePicker = PHPickerViewController(configuration: config)
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    private func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        tableView.scrollIndicatorInsets = tableView.contentInset
        
    }
    
}


extension AddCustomerVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return customerFormList.forms.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customerFormList.forms[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let placeHolder = customerFormList.forms[indexPath.section][indexPath.row]
        
        switch( placeHolder ){
            
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldCell.identifier, for: indexPath) as! TextFieldCell
            cell.setTextfieldPlaceholder(attributedString: NSMutableAttributedString(string: placeHolder.rawValue))
            cell.selectionStyle = .none
            cell.setHeightForFeild(30)
            let requiredText = NSMutableAttributedString(string: "*", attributes: [.foregroundColor: UIColor.systemRed])
            
            switch (placeHolder) {
                
            case .displayName:
                cell.requiredField = true
                cell.setKeyboardType(.alphabet)
                if let text = customerModal.displayName, !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeHolder.rawValue)
                    cell.requiredSymbol.isHidden = false
                } else {
                    requiredText.append(cell.getTextfieldAttributedText())
                    cell.setTextfieldPlaceholder(attributedString: requiredText)
                }
                
            case .companyName:
                cell.requiredField = true
                cell.setKeyboardType(.alphabet)
                if let text = customerModal.companyName, !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeHolder.rawValue)
                    cell.requiredSymbol.isHidden = false
                } else {
                    requiredText.append(cell.getTextfieldAttributedText())
                    cell.setTextfieldPlaceholder(attributedString: requiredText)
                }
                
            case .mobile:
                cell.requiredField = true
                cell.setKeyboardType(.phonePad)
                if let text = customerModal.mobile, !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeHolder.rawValue)
                    cell.requiredSymbol.isHidden = false
                } else {
                    requiredText.append(cell.getTextfieldAttributedText())
                    cell.setTextfieldPlaceholder(attributedString: requiredText)
                }
                
            case .email:
                cell.setKeyboardType(.emailAddress)
                if let text = customerModal.email, !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)  {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeHolder.rawValue)
                } else {
                    cell.requiredField = false
                }
                
            case .address:
                cell.setKeyboardType(.alphabet)
                if let text = customerModal.address, !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeHolder.rawValue)
                } else {
                    cell.requiredField = false
                }
                
            case .remarks:
                cell.setKeyboardType(.alphabet)
                if let text = customerModal.remarks, !(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    cell.setTextfieldText(text: text)
                    cell.setPlaceholderLabel(text: placeHolder.rawValue)
                } else {
                    cell.requiredField = false
                }
                
            case .error:
                break
            }
            cell.textFieldHelper = self
            cell.textFieldSaveData = self
            return cell
        }
    }
}

extension AddCustomerVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? TextFieldCell else { return }
        cell.makeTextfieldFirstResponder()
    }
    
}

extension AddCustomerVC: UISheetPresentationControllerDelegate {
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alert = UIAlertController(title: "", message: "Are you sure, you want to discard the changes?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Discard Changes", style: .destructive){
            action in
            self.dismiss(animated: true)
        })
        alert.addAction(UIAlertAction(title: "Keep Editing", style: .cancel))
        
        // For showing action sheet in iPad, we have to make it presented as popoverController.
        if let popoverController = alert.popoverPresentationController {
//          popoverController.sourceView = self.view
//          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//          popoverController.permittedArrowDirections = []
            popoverController.barButtonItem = navigationItem.leftBarButtonItem
        }
        
        self.present(alert, animated: true)
    }
}

extension AddCustomerVC: TextFieldHelper {
    
    func reconfigRow(for cell: TextFieldCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        tableView.reconfigureRows(at: [indexPath])
    }
    
    func assignNextResponder(from cell: TextFieldCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        let nextIndexPath = customerFormList.getNextIndex(for: indexPath)
        guard let nextIndexPath = nextIndexPath else {
            cell.makeTextfieldResignFirstResponder()
            return
        }
        let cell = tableView.cellForRow(at: nextIndexPath) as? TextFieldCell
        cell?.makeTextfieldFirstResponder()
    }
    
    func getPlaceholder(for cell: TextFieldCell) -> String {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return ActorFormCellType.error.rawValue
        }
        return customerFormList.forms[indexPath.section][indexPath.row].rawValue
    }
    
}

extension AddCustomerVC: TextFieldSaveData {
    
    func updateModal(for type: String, value: String) {
        
        guard let type = ActorFormCellType(rawValue: type) else { return }
        
        if let _ = self.id {
            customerModal.assignValue(type: type, value: value)
            if let displayName = customerModal.displayName, let companyName = customerModal.companyName, let mobile = customerModal.mobile {
                if displayName.isEmpty || companyName.isEmpty || mobile.isEmpty {
                    doneButton = false
                } else {
                    doneButton = true
                }
            } else {
                doneButton = false
            }
        } else {
            customerModal.assignValue(type: type, value: value)
            
            if let displayName = customerModal.displayName, let companyName = customerModal.companyName, let mobile = customerModal.mobile {
                if displayName.isEmpty || companyName.isEmpty || mobile.isEmpty {
                    doneButton = false
                } else {
                    doneButton = true
                }
            } else {
                doneButton = false
            }
        }
    }
    
}

extension AddCustomerVC: PHPickerViewControllerDelegate {
    
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        // When cancel is pressed an empty array is passed.
        if results.isEmpty {
            DispatchQueue.main.async {
                picker.dismiss(animated: true)
            }
        }
        // Otherwise we process this.
        results.first?.itemProvider.loadObject(ofClass: UIImage.self) {
            [weak self] reading, error in
            guard let image = reading as? UIImage, error == nil else { return }
            DispatchQueue.main.async {
                self?.pickImage.image = image
                self?.pickImage.contentMode = .scaleAspectFill
                self?.pickImageButton.setTitle("Edit", for: .normal)
                self?.selectedImage = image
                // Have to dismiss youself
                picker.dismiss(animated: true)
            }
        }
    }
}
