//
//  CustomerTableViewCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 03/09/22.
//

import UIKit

class ActorListCell: BaseTableViewCell {
    
    enum LabelType {
        case displayName
        case companyName
    }

    static var identifier = "com.abilash.customerCell"
    
    private let imageHeight: CGFloat = 40
    
    lazy private var actorImageView: UIImageView = {
        var imageView = UIImageView(image: UIImage(systemName: "person.circle"))
        imageView.layer.cornerRadius = imageHeight / 2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .quaternaryLabel
        return imageView
    }()
    
    private let activityIndicator: UIActivityIndicatorView  = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let displayName: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.text = "Customer Name"
        return label
    }()
    
    private let companySymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 14, weight: .regular, scale: .medium)
        var image = UIImage(systemName: "building.2", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    
    private let companyName: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.textColor = .secondaryLabel
        label.text = "Company Name"
        return label
    }()
    
    private let separatorView: UIView = {
        var view = UIView()
        view.backgroundColor = .separator
        view.isHidden = true
        return view
    }()
    
    override func setupCell() {
        contentView.addSubview(actorImageView)
        actorImageView.addSubview(activityIndicator)
        contentView.addSubview(displayName)
        contentView.addSubview(companySymbol)
        contentView.addSubview(companyName)
        actorImageView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        displayName.translatesAutoresizingMaskIntoConstraints = false
        companySymbol.translatesAutoresizingMaskIntoConstraints = false
        companyName.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            actorImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            actorImageView.trailingAnchor.constraint(lessThanOrEqualTo: displayName.trailingAnchor, constant: -8),
            actorImageView.heightAnchor.constraint(equalToConstant: imageHeight),
            actorImageView.widthAnchor.constraint(equalToConstant: imageHeight),
            actorImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: actorImageView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: actorImageView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: actorImageView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: actorImageView.trailingAnchor),
            
            displayName.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            displayName.leadingAnchor.constraint(equalTo: actorImageView.trailingAnchor, constant: 8),
            displayName.bottomAnchor.constraint(equalTo: companyName.topAnchor, constant: -6),
            displayName.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            companySymbol.leadingAnchor.constraint(equalTo: displayName.leadingAnchor),
            companySymbol.trailingAnchor.constraint(lessThanOrEqualTo: companyName.leadingAnchor, constant: -4),
            companySymbol.centerYAnchor.constraint(equalTo: companyName.centerYAnchor),
            
            companyName.topAnchor.constraint(equalTo: displayName.bottomAnchor, constant: 6),
            companyName.leadingAnchor.constraint(equalTo: companySymbol.trailingAnchor, constant: 4),
            companyName.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            companyName.trailingAnchor.constraint(lessThanOrEqualTo: displayName.trailingAnchor),
        ])
        displayName.setContentHuggingPriority(UILayoutPriority(251), for: .vertical)
        companyName.setContentCompressionResistancePriority(UILayoutPriority(751), for: .vertical)
        
        addSubview(separatorView)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            separatorView.heightAnchor.constraint(equalToConstant: 0.33),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    func setNamesFor(actor displayName: String, company: String) {
        self.displayName.text = displayName
        self.companyName.text = company
    }
    
    func setColor(_ label: LabelType, color: UIColor) {
        switch (label) {
            
        case .displayName:
            displayName.textColor = color
        case .companyName:
            companyName.textColor = color
        }
    }
    
    func setImage(imageName: UUID?, for actorType: ImageType) {
        DispatchQueue.main.async {
            [weak self] in
            self?.activityIndicator.startAnimating()
        }
        DispatchQueue.global().async {
            [weak self] in
            var imagePath: URL
            guard let imageName = imageName?.uuidString else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.actorImageView.image = UIImage(systemName: "person.circle")
                    self?.actorImageView.contentMode = .scaleAspectFit
                }
                return
            }
            if actorType == .VendorImage {
                imagePath = URLStore.vendorImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            } else {
                imagePath = URLStore.customerImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            }
            do {
                let data = try Data(contentsOf: imagePath)
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.actorImageView.image = UIImage(data: data)
                    self?.actorImageView.contentMode = .scaleAspectFill
                }
            } catch let error {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.actorImageView.image = UIImage(systemName: "person.circle")
                    self?.actorImageView.contentMode = .scaleAspectFit
                }
                print("\n\nActorListCell -> Loading Image to ImageView:\n\(error)\n\n")
            }
        }
    }
}
