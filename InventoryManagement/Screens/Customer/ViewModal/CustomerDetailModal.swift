//
//  CustomerDetailModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 04/09/22.
//

import Foundation

struct CustomerDetailModal {
    
    var customerID: UUID?
    var displayName: String?
    var companyName: String?
    var mobile: String?
    var email: String?
    var address: String?
    var remarks: String?
    var profileImage: UUID?
    
    mutating func assignValue(type: ActorFormCellType, value: String) {
        switch (type) {
        case .displayName:
            displayName = value
        case .companyName:
            companyName = value
        case .mobile:
            mobile = value
        case .email:
            email = value
        case .address:
            address = value
        case .remarks:
            remarks = value
        default:
            break
        }
    }
}
