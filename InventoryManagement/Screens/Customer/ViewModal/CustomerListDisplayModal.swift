//
//  CustomerListDisplayModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 04/09/22.
//

import Foundation

struct CustomerListDisplayModal {
    var customerID: UUID?
    var displayName: String?
    var companyName: String?
    var profileImage: UUID?
    var mobile: Int?
}
