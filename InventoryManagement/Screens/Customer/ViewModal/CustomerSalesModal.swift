//
//  CustomerSalesModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 03/10/22.
//

import Foundation

struct CustomerSalesModal {
    var item: Item?
    var itemName: String?
    var itemCost: Double?
    var totalCost: Double?
    var salesDate: Date?
    var itemImage: UUID?
    var quantity: Int?
}
