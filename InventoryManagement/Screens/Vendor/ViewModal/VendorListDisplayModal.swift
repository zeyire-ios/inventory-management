//
//  VendorListModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 30/08/22.
//

import Foundation


struct VendorListDisplayModal {
    var vendorId: UUID?
    var displayName: String?
    var companyName: String?
    var profileImage: UUID?
    var mobile: Int?
}
