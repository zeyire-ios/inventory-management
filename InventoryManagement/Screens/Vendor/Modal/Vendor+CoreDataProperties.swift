//
//  Vendor+CoreDataProperties.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 01/10/22.
//
//

import Foundation
import CoreData


extension Vendor {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Vendor> {
        return NSFetchRequest<Vendor>(entityName: "Vendor")
    }

    @NSManaged public var address: String?
    @NSManaged public var companyName: String
    @NSManaged public var displayName: String
    @NSManaged public var email: String?
    @NSManaged public var mobile: String
    @NSManaged public var profileImage: UUID?
    @NSManaged public var remarks: String?
    @NSManaged public var vendorID: UUID
    @NSManaged public var purchaseOrders: NSSet?

    public var purchaseOrdersArray: [PurchaseOrder] {
        let set = purchaseOrders as? Set<PurchaseOrder> ?? []
        
        return set.sorted {
            $0.purchaseDate > $1.purchaseDate
        }
    }
}

// MARK: Generated accessors for purchaseOrders
extension Vendor {

    @objc(addPurchaseOrdersObject:)
    @NSManaged public func addToPurchaseOrders(_ value: PurchaseOrder)

    @objc(removePurchaseOrdersObject:)
    @NSManaged public func removeFromPurchaseOrders(_ value: PurchaseOrder)

    @objc(addPurchaseOrders:)
    @NSManaged public func addToPurchaseOrders(_ values: NSSet)

    @objc(removePurchaseOrders:)
    @NSManaged public func removeFromPurchaseOrders(_ values: NSSet)

}

extension Vendor : Identifiable {

}
