//
//  VendorSort.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 11/10/22.
//

import Foundation

struct VendorSort: SortOptions {
    func setSortBy(sortBy: Any) {
        VendorSort._setSortBy(sortBy: sortBy as! SortBy)
    }
    
    func setSortOrder(ascending: Bool) {
        VendorSort._setSortOrder(ascending: ascending)
    }
    
    static let sortBy: [SortBy] = [.displayName, .companyName]
    
    static let filterBy: [FilterBy] = [.all]
    
    enum SortBy: String {
        case displayName = "Vendor Name"
        case companyName = "Company"
    }
    
    enum FilterBy: String {
        case all
    }
    
    static func checkSortOrder() -> Bool {
        if UserDefaults.standard.value(forKey: "VendorAscending") == nil {
            UserDefaults.standard.set(true, forKey: "VendorAscending")
        }
        return UserDefaults.standard.bool(forKey: "VendorAscending")
    }
    
    static func _setSortOrder(ascending: Bool) {
        UserDefaults.standard.set(ascending, forKey: "VendorAscending")
    }
    
    static func checkSortBy() -> SortBy {
        if UserDefaults.standard.value(forKey: "VendorSortBy") == nil {
            UserDefaults.standard.set(SortBy.displayName.rawValue, forKey: "VendorSortBy")
        }
        return SortBy.init(rawValue: (UserDefaults.standard.string(forKey: "VendorSortBy"))!)!
        
    }
    
    static func _setSortBy(sortBy: SortBy) {
        UserDefaults.standard.set(sortBy.rawValue, forKey: "VendorSortBy")
    }
}
