//
//  VendorViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 29/08/22.
//

import UIKit
import CoreData

class VendorViewController: UIViewController {
    
    lazy private var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.register(ActorListCell.self, forCellReuseIdentifier: ActorListCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.backgroundColor = .systemGroupedBackground
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy private var createVendorButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Add Vendor"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(addButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let noSearchViewImage = UIImage(named: "SearchProfile")?.withTintColor(.secondaryLabel)
    
    private let noVendorViewImage = UIImage(named: "Group")?.withTintColor(.secondaryLabel)
    
    private let searchResultText = "Sorry, No Search Results!"
    
    private let zeroVendortext = "Sorry, No Vendors Added!"
    
    lazy var searchBar = UISearchController()
    
    private var vendorListDisplayModals: [VendorListDisplayModal]?
    
    private let CDM = CoreDataManager.shared
    
    private var sortBy: VendorSort.SortBy {
        VendorSort.checkSortBy()
    }
    
    private var ascending: Bool {
        VendorSort.checkSortOrder()
    }
    
    private var timer = Timer()
    
    weak var splitDelegate: SplitControllerDelegate?
    
    private var firstTime: Bool = true {
        didSet {
            selectFirstRowInSplitView()
        }
    }
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGroupedBackground
        
        setupConfig()
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
        
        addNotificationObservers()
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadVendor), name: VendorKey.reloadVendor, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadVendor), name: VendorKey.deleteVendor, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    private func reloadVendor() {
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
    }
    
    private func setupConfig() {
        tableViewConfig()
        splitViewConfig()
        navigationConfig()
        backgroundViewConfig()
        createRefreshControl()
    }
    
    private func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(createVendorButton)
        createVendorButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: tableView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            
            backgroundView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
            backgroundView.topAnchor.constraint(greaterThanOrEqualTo: tableView.topAnchor),
            backgroundView.bottomAnchor.constraint(lessThanOrEqualTo: tableView.bottomAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            createVendorButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createVendorButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
        ])
    }
    
    private func splitViewConfig() {
        
        splitViewController?.delegate = self
        splitViewController?.preferredPrimaryColumnWidthFraction = 0.4
        
    }
    
    private func navigationConfig() {
        navigationItem.title = "Vendors"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barTintColor = .systemGroupedBackground
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.searchController = self.searchBar
        navigationItem.searchController?.searchBar.placeholder = "Search Vendor"
        navigationItem.searchController?.searchBar.delegate = self
        navigationItem.searchController?.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.rightBarButtonItems = [ UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed(_:))), UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(sortButtonPressed)) ]
    }
    
    private func backgroundViewConfig() {
        backgroundView.isHidden = true
        createVendorButton.isHidden = true
    }
    
    // MARK: - Functions
    
    // This function creates the list modal to display at table view
    private func fetchToViewModal(searchFor searchTerm: String? = nil, sortBy: VendorSort.SortBy, ascending: Bool) {
        DispatchQueue.global().async {
            [weak self] in
            let secondary: VendorSort.SortBy = (sortBy == .displayName ? .companyName : .displayName)
            var predicates: [NSPredicate] = []
            if let searchTerm = searchTerm {
                predicates.append(NSPredicate(format: "(displayName CONTAINS[c] %@) OR (companyName CONTAINS[c] %@)", searchTerm, searchTerm))
            }
            let vendorCoreDataList = self?.CDM.fetchVendor(for: nil, with: ["vendorID", "displayName", "companyName", "profileImage", "mobile"], check: predicates, sortBy: [NSSortDescriptor(key: self?.convertToCoreDataAttribute(sortBy), ascending: ascending), NSSortDescriptor(key: self?.convertToCoreDataAttribute(secondary), ascending: ascending)])
            self?.vendorListDisplayModals = vendorCoreDataList?.map({
                vendor in
                VendorListDisplayModal(vendorId: vendor.vendorID, displayName: vendor.displayName, companyName: vendor.companyName, profileImage: vendor.profileImage, mobile: Int(vendor.mobile))
            })
            DispatchQueue.main.async {
                self?.showBackgroundFor(emptyList: (vendorCoreDataList?.isEmpty)!, withSearchTerm: searchTerm != nil)
                self?.tableView.reloadData()
                if self?.firstTime ?? false {
                    self?.firstTime = false
                }
            }
        }
    }
    
    private func showBackgroundFor(emptyList: Bool, withSearchTerm: Bool) {
        if emptyList && withSearchTerm {
            createVendorButton.isHidden = false
            backgroundView.isHidden = true
            backgroundView.titleLabel.text = searchResultText
            backgroundView.noDataImage.image = noSearchViewImage
        } else {
            createVendorButton.isHidden = false
            backgroundView.isHidden = false
            backgroundView.titleLabel.text = zeroVendortext
            backgroundView.noDataImage.image = noVendorViewImage
        }
    }
    
    private func selectFirstRowInSplitView() {
        if traitCollection.userInterfaceIdiom == .pad {
            if let id = vendorListDisplayModals?.first?.vendorId {
                let vc = VendorDetailVC(id: id )
                splitDelegate?.itemSelected(present: vc)
            }
        }
    }
    
    private func convertToCoreDataAttribute(_ attribute: VendorSort.SortBy) -> String {
        switch (attribute) {
        case .displayName:
            return "displayName"
        case .companyName:
            return "companyName"
        }
    }
    
    private func createRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func deleteRefreshContorl() {
        tableView.refreshControl = nil
    }
    
    // MARK: - Objc Functions
    
    @objc
    private func addButtonPressed(_ sender: UIBarButtonItem) {
        present(UINavigationController(rootViewController: AddVendorVC()), animated: true)
    }
    
    @objc
    private func sortButtonPressed(_ sender: UIBarButtonItem) {
        present(UINavigationController(rootViewController: SortViewController<VendorSort.SortBy, VendorSort.FilterBy>(sortModal: VendorSort() as SortOptions, sortBy: VendorSort.sortBy, sortedBy: sortBy, ascending: ascending, filterBy: nil, filter: nil, completion: {
            [weak self] in
            self?.fetchToViewModal(sortBy: (self?.sortBy) ?? VendorSort.SortBy.displayName, ascending: (self?.ascending) ?? true)
        })), animated: true)
    }

    @objc
    private func pullToRefresh(_ sender: UIRefreshControl) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
        sender.endRefreshing()
    }
    
}

extension VendorViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if vendorListDisplayModals?.count == 0 {
            tableView.isScrollEnabled = false
            return 0
        } else {
            backgroundViewConfig()
            tableView.isScrollEnabled = true
            return vendorListDisplayModals?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let vendor = vendorListDisplayModals?[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorListCell.identifier, for: indexPath) as! ActorListCell
            
            cell.setNamesFor(
                actor: vendor.displayName ?? "Unknown Vendor",
                company: vendor.companyName ?? "Unknown Company"
            )
            cell.setImage(imageName: vendor.profileImage, for: .VendorImage)
            
            return cell
        }
        return UITableViewCell()
    }
    
}

extension VendorViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = vendorListDisplayModals?[indexPath.row].vendorId else { return }
        let vc = VendorDetailVC(id: id)
        vc.hidesBottomBarWhenPushed = true
        splitDelegate?.itemSelected(present: vc)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("Hi")
        if let mobileURL = URL(string: "tel://\((vendorListDisplayModals?[indexPath.row].mobile)!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(mobileURL)) {
                application.open(mobileURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var callAction = UIContextualAction()
        
        callAction = UIContextualAction(style: .normal, title: "Call" ) {
            [weak self] (action, view, success) in
            if let mobileURL = URL(string: "tel://\((self?.vendorListDisplayModals?[indexPath.row].mobile)!)") {
                let application: UIApplication = UIApplication.shared
                if (application.canOpenURL(mobileURL)) {
                    application.open(mobileURL, options: [:], completionHandler: nil)
                    success(true)
                }
            }
        }
        callAction.image = UIImage(systemName: "phone.fill")
        callAction.backgroundColor = .systemPurple
        return UISwipeActionsConfiguration(actions: [callAction])
    }
}

extension VendorViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {
            [weak self] timer in
            if searchText.isEmpty {
                self?.fetchToViewModal(sortBy: self?.sortBy ?? VendorSort.SortBy.displayName, ascending: self?.ascending ?? true)
            } else {
                self?.fetchToViewModal(searchFor: searchText,sortBy: self?.sortBy ?? VendorSort.SortBy.displayName, ascending: self?.ascending ?? true)
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        timer.invalidate()
        fetchToViewModal(searchFor: searchBar.text, sortBy: sortBy, ascending: ascending)
    }
}

extension VendorViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }
    
}

extension VendorViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        deleteRefreshContorl()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        createRefreshControl()
    }
    
}
