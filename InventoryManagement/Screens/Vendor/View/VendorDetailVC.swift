//
//  VendorDetailVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 29/08/22.
//

import UIKit
import CoreData


class VendorDetailVC: UIViewController {
    
    lazy private var vendorMenu: UIMenu = {
        return UIMenu(title: "", image: nil, identifier: nil, options: .destructive, children: [topMenu, bottomMenu])
    }()
    
    lazy private var topMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        actions.append(UIAction(title: "Make Purchase Order",
                                image: UIImage(systemName: "cart.fill"),
                                handler: {
            [weak self] action in
            self?.makePurchaseOrder()
        }))
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    lazy private var bottomMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        actions.append(UIAction(title: "Delete Vendor",
                                image: {
            let image = UIImage(systemName: "trash")
            return image?.withTintColor(.systemRed)
        }(),
                                attributes: .destructive,
                                handler: {
            [weak self] action in
            let alert = UIAlertController(title: "", message: "Are you sure you want to delete this Item", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive){
                action in
                self?.deleteVendor()
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            
            // For showing action sheet in iPad, we have to make it presented as popoverController.
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self?.view
                popoverController.sourceRect = CGRect(x: (self?.view.bounds.midX)!, y: (self?.view.bounds.midY)!, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self?.present(alert, animated: true)
        }))
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    lazy private var menuBar: MenuBar = {
        var menuBar = MenuBar()
        menuBar.delegate = self
        menuBar.setBackground(color: .systemGroupedBackground)
        return menuBar
    }()
    
    lazy private var mainTableView: UITableView = {
        var tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.register(ActorProfileCell.self, forCellReuseIdentifier: ActorProfileCell.identifier)
        tableView.register(ActorDetailCell.self, forCellReuseIdentifier: ActorDetailCell.identifier)
        tableView.register(ActorTransactionCell.self, forCellReuseIdentifier: ActorTransactionCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        let image = UIImage(named: "NoTaskNotepad")?.withTintColor(.secondaryLabel)
        noDataView.noDataImage.image = image
        noDataView.titleLabel.text = "Sorry, no purchases are made!"
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    lazy private var createPurchaseButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Make Purchase Order"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(createPurchaseOrder), for: .touchUpInside)
        return button
    }()
    
    private let topView: ActorProfileCell = {
        var view = ActorProfileCell()
        view.layer.masksToBounds = true
        view.clipsToBounds = false
        return view
    }()
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .long
        return formatter
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private var selectedIndex: Int = -1 {
        didSet {
            mainTableView.reloadData()
        }
    }
    
    private var vendorModal = VendorDetailModal()
    
    private var vendorTransactionModal = [VendorPurchaseModal]()
    
    private let CDM = CoreDataManager.shared
    
    private var id: UUID
    
    // MARK: - Init
    
    init(id: UUID) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        setupConfig()
        
        addNotificationObservers()
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupConfig() {
        view.backgroundColor = .systemGroupedBackground
        configuration()
        fetchDisplayModal()
    }
    
    func configuration() {
        navigationConfig()
        menuBarConfig()
        backGroundViewConfig()
        mainTableViewConfig()
    }
    
    func navigationConfig() {
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(title: "", image: UIImage(systemName: "ellipsis.circle"), primaryAction: nil, menu: vendorMenu),
            UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonPressed)),
        ]
    }
    
    private func menuBarConfig() {
        view.addSubview(menuBar)
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            menuBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            menuBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8),
            menuBar.heightAnchor.constraint(equalToConstant: 40),
            menuBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8),
        ])
    }
    
    private func mainTableViewConfig() {
        view.addSubview(mainTableView)
        mainTableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(createPurchaseButton)
        createPurchaseButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainTableView.topAnchor.constraint(equalTo: menuBar.bottomAnchor),
            mainTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mainTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            
            backgroundView.centerYAnchor.constraint(equalTo: mainTableView.centerYAnchor),
            backgroundView.topAnchor.constraint(greaterThanOrEqualTo: mainTableView.topAnchor),
            backgroundView.bottomAnchor.constraint(lessThanOrEqualTo: mainTableView.bottomAnchor),
            backgroundView.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor),
            backgroundView.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            createPurchaseButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createPurchaseButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
        ])
    }
    
    private func backGroundViewConfig() {
        backgroundView.isHidden = true
        createPurchaseButton.isHidden = true
    }
    
    // MARK: - Functions
    
    func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            if let vendor = self?.CDM.fetchVendor(for: self?.id, with: ["displayName", "companyName", "mobile", "email", "address", "remarks", "profileImage"])?.first {
                self?.vendorModal = VendorDetailModal(vendorID: vendor.vendorID, displayName: vendor.displayName, companyName: vendor.companyName, mobile: vendor.mobile, email: vendor.email, address: vendor.address, remarks: vendor.remarks, profileImage: vendor.profileImage)
                self?.fetchTransactions(only: false)
            }
            DispatchQueue.main.async {
                if self?.selectedIndex == -1 {
                    self?.selectedIndex = 0
                }
                self?.mainTableView.reloadData()
            }
        }
    }
    
    func fetchTransactions(only: Bool) {
        DispatchQueue.global().async {
            [weak self] in
            if let vendor = self?.CDM.fetchVendor(for: self?.id)?.first {
                self?.vendorTransactionModal.removeAll(keepingCapacity: true)
                let _ = vendor.purchaseOrdersArray.map({
                    purchase in
                    self?.vendorTransactionModal.append(VendorPurchaseModal(item: purchase.item, itemName: purchase.item.itemName, itemCost: purchase.purchaseItemCost, totalCost: purchase.purchaseTotalCost, purchaseDate: purchase.purchaseDate, itemImage: purchase.item.itemImage, quantity: Int(purchase.purchaseQuantity)))
                })
            }
            DispatchQueue.main.async {
                if only {
                    self?.mainTableView.reloadData()
                }
            }
        }
    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: VendorKey.reloadVendor, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTransaction), name: POKey.reloadPO, object: nil)
    }
    
    func makePurchaseOrder(item: Item? = nil) {
        present(UINavigationController(rootViewController: AddPOVC(item: item, vendor: self.CDM.fetchVendor(for: self.id)?.first)), animated: true)
    }
    
    func deleteVendor() {
        let vendor = CDM.fetchVendor(for: self.id, with: ["displayName"])?.first
        DispatchQueue.global().async {
            if let imageName = vendor!.profileImage?.uuidString {
                let imagePath = URLStore.vendorImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
                if FileManager.default.fileExists(atPath: imagePath.path) {
                    do {
                        try FileManager.default.removeItem(at: imagePath)
                    } catch let error {
                        print("\n\nItemViewController: -> Deleting ImageFile from local:\n\(error)\n\n")
                    }
                }
            }
        }
        #warning("No notification")
        self.CDM.viewContext.delete(vendor!)
        do {
            try CDM.viewContext.save()
        } catch {
            print("\n\nItemViewController: -> Saving to CoreData\n\(error)\n\n")
        }
        NotificationCenter.default.post(name: VendorKey.deleteVendor, object: nil)
        dismissView()
    }
    
    private func handleCallAction() {
        if let mobileURL = URL(string: "tel://\(vendorModal.mobile!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(mobileURL)) {
                application.open(mobileURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    private func handleMessageAction() {
        if let messageURL = URL(string: "sms://\(vendorModal.mobile!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(messageURL)) {
                application.open(messageURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    private func handleEmailAction() {
        if let emailURL = URL(string: "mailto://\(vendorModal.email!)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(emailURL)) {
                application.open(emailURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    // MARK: - Objc Functions
    
    @objc
    func createPurchaseOrder(_ sender: UIButton) {
        makePurchaseOrder()
    }
    
    @objc
    private func editButtonPressed() {
        present(UINavigationController(rootViewController: AddVendorVC(id: self.id)), animated: true)
    }
    
    @objc
    private func reloadModal() {
        fetchDisplayModal()
    }
    
    @objc
    private func reloadTransaction() {
        fetchTransactions(only: true)
    }
    
    @objc
    private func dismissView() {
        if traitCollection.userInterfaceIdiom == .phone {
            splitViewController?.hide(.secondary)
        } else if traitCollection.userInterfaceIdiom == .pad {
            let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
            vc?.setViewControllers([SplitViewController.blankVC], animated: true)
        }
    }
    
}

extension VendorDetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedIndex == 0 {
            backGroundViewConfig()
            return 5
        } else {
            if vendorTransactionModal.count == 0 && selectedIndex == 1 {
                backgroundView.isHidden = false
                createPurchaseButton.isHidden = false
                return 0
            } else {
                backGroundViewConfig()
                return vendorTransactionModal.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0 {
            return 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch ( selectedIndex ) {
            // Selection of Column
        case 0:
            switch (indexPath.section) {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: ActorProfileCell.identifier, for: indexPath) as! ActorProfileCell
                cell.backgroundColor = .secondarySystemGroupedBackground
                cell.actionDelegate = self
                
                cell.setTitle(
                    forActor: vendorModal.displayName ?? "Unknown Vendor",
                    forCompany: vendorModal.companyName ?? "Unknown Company"
                )
                cell.setImage(imageName: vendorModal.profileImage, for: .VendorImage)
                if let email = vendorModal.email, !email.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    cell.enableMail()
                } else {
                    cell.disableMail()
                }
                
                return cell
            default:
                // Because rest of the cells are common
                let cell = tableView.dequeueReusableCell(withIdentifier: ActorDetailCell.identifier, for: indexPath) as! ActorDetailCell
                let config = UIImage.SymbolConfiguration(pointSize: 14, weight: .regular, scale: .medium)
                cell.backgroundColor = .secondarySystemGroupedBackground
                
                switch (indexPath.section) {
                case 1:
                    cell.setLabel(
                        image: UIImage(systemName: "phone", withConfiguration: config),
                        label: "Mobile"
                    )
                    cell.setViewModal(
                        value: vendorModal.mobile,
                        dataDetector: .phoneNumber
                    )
                    return cell
                case 2:
                    cell.setLabel(
                        image: UIImage(systemName: "envelope", withConfiguration: config),
                        label: "Email"
                    )
                    if let email = vendorModal.email {
                        cell.setViewModal(
                            value: email,
                            dataDetector: .link
                        )
                    }
                   return cell
                case 3:
                    cell.setLabel(
                        image: UIImage(systemName: "map", withConfiguration: config),
                        label: "Address"
                    )
                    if let address = vendorModal.address {
                        cell.setViewModal(
                            value: address,
                            dataDetector: .address
                        )
                    }
                    return cell
                case 4:
                    cell.setLabel(
                        image: UIImage(systemName: "note.text", withConfiguration: config),
                        label: "Remarks"
                    )
                    if let remarks = vendorModal.remarks {
                        cell.setViewModal(
                            value: remarks
                        )
                    }
                    return cell
                default:
                    return UITableViewCell()
                }
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorTransactionCell.identifier, for: indexPath) as! ActorTransactionCell
            let purchase = vendorTransactionModal[indexPath.section]
            
            
            cell.setValuesFor(
                itemName: purchase.itemName ?? "Item Not Found",
                itemPrice: currencyFormatter.string(from: (purchase.itemCost ?? 0.0) as NSNumber),
                totalPrice: currencyFormatter.string(from: (purchase.totalCost ?? 0.0) as NSNumber),
                date: dateFormatter.string(from: purchase.purchaseDate ?? Date(timeIntervalSince1970: 0)),
                quantity: String(purchase.quantity ?? 0)
            )
            
            cell.reorderDelegate = self
            cell.setImage(imageName: purchase.itemImage)
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension VendorDetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
}

extension VendorDetailVC: ActorProfileDelegate {
    
    func sendMail() {
        handleEmailAction()
    }
    
    func makeCall() {
        handleCallAction()
    }
    
    func sendMessage() {
        handleMessageAction()
    }
    
}

extension VendorDetailVC: MenuBarDelegate {
    
    func menuBar(selectedIndex index: Int) {
        selectedIndex = index
    }
    
}

extension VendorDetailVC: ReorderDelegate {
    
    func reorder(_ cell: ActorTransactionCell) {
        
        guard let indexPath = mainTableView.indexPath(for: cell) else {
            return
        }
        let item = vendorTransactionModal[indexPath.section].item
        makePurchaseOrder(item: item)
        
    }
    
}
