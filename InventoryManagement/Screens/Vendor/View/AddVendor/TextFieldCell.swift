//
//  AddVendorView.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 29/08/22.
//

import UIKit

#warning("Not touched yet")
protocol TextFieldHelper: AnyObject {
    
    func assignNextResponder(from cell: TextFieldCell)
    
    func getPlaceholder(for cell: TextFieldCell) -> String
    
    func reconfigRow(for cell: TextFieldCell)
    
}

protocol TextFieldSaveData: AnyObject {
    
    func updateModal(for type: String, value: String)
    
}

class TextFieldCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.textFieldCell"
    
    weak var textFieldHelper: TextFieldHelper?
    
    var requiredField: Bool?
    
    weak var textFieldSaveData: TextFieldSaveData?
    
    private let placeHolderStack: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 2
        stackView.alignment = .top
        stackView.distribution = .fill
        return stackView
    }()
    
    let requiredSymbol: UIImageView = {
        var config = UIImage.SymbolConfiguration.init(pointSize: 6, weight: .semibold, scale: .medium)
        var image = UIImage(systemName: "asterisk", withConfiguration: config)
        var imageView = UIImageView(image: image)
        imageView.tintColor = .systemRed
        imageView.isHidden = true
        return imageView
    }()
    
    private let placeHolderLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .secondaryLabel
        return label
    }()
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
        textField.font = UIFont.preferredFont(forTextStyle: .body)
        textField.clearButtonMode = .whileEditing
        textField.backgroundColor = .systemBackground
        return textField
    }()
    
    private let warningLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .systemRed
        label.textAlignment = .right
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
            if traitCollection.userInterfaceStyle == .dark {
                textField.backgroundColor = .secondarySystemBackground
            }
            else {
                textField.backgroundColor = .systemBackground
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func setupCell() {
        placeHolderStack.addArrangedSubview(requiredSymbol)
        placeHolderStack.addArrangedSubview(placeHolderLabel)
        requiredSymbol.setContentHuggingPriority(.init(251), for: .horizontal)
        
        contentView.addSubview(placeHolderStack)
        placeHolderStack.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(textField)
        contentView.addSubview(warningLabel)
        textField.translatesAutoresizingMaskIntoConstraints = false
        warningLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            placeHolderStack.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 8),
            placeHolderStack.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            placeHolderStack.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            textField.topAnchor.constraint(equalTo: placeHolderStack.bottomAnchor, constant: 4),
            textField.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            textField.bottomAnchor.constraint(equalTo: warningLabel.topAnchor, constant: -5),
            textField.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            warningLabel.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 5),
            warningLabel.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 4),
            warningLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -4),
            warningLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -16),
        ])
        
        textField.delegate = self
        
    }
    
    override func prepareForReuse() {
        placeHolderLabel.text = nil
        textField.text = nil
        requiredSymbol.isHidden = true
        warningLabel.isHidden = true
        requiredField = false
    }
}

extension TextFieldCell {
    
    func getTextfieldAttributedText() -> NSAttributedString {
        textField.attributedPlaceholder ?? NSAttributedString()
    }
    
    func setHeightForFeild(_ value: CGFloat) {
        textField.heightAnchor.constraint(greaterThanOrEqualToConstant: value).isActive = true
    }
    
    func setTextfieldPlaceholder(string: String? = nil, attributedString: NSAttributedString? = nil) {
        if let text = string {
            textField.placeholder = text
        } else if let text = attributedString {
            textField.attributedPlaceholder = text
        }
    }
    
    func setKeyboardType(_ type: UIKeyboardType ) {
        textField.keyboardType = type
    }
    
    func setTextfieldText( text: String? ) {
        textField.text = text
    }
    
    func setPlaceholderLabel(text: String? ) {
        placeHolderLabel.text = text
    }
    
    func makeTextfieldFirstResponder() {
        textField.becomeFirstResponder()
    }
    
    func makeTextfieldResignFirstResponder() {
        textField.resignFirstResponder()
    }
    
    func callTextFieldDidEndEditing() {
        textFieldDidEndEditing(self.textField)
    }
    
    private func isValid(email: String) -> Bool {
      do {
        let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let range = NSRange(location: 0, length: email.count)
        let matches = detector.matches(in: email, options: .anchored, range: range)
        guard matches.count == 1 else { return false }
        return matches[0].url?.scheme == "mailto"
      } catch {
        return false
      }
    }
    
    private func isValid(mobile: String) -> Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let range = NSRange(location: 0, length: mobile.count)
            let matches = detector.matches(in: mobile, options: [], range: range)
            if let res = matches.first {
                return res.resultType == .phoneNumber && range.location == 0 && range.length == mobile.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
}

extension TextFieldCell: UITextFieldDelegate {
    
    /**
     ### Functionality
     This function mainly handles the placeholder of the Cell
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        /// Usually the text in textField will be nil if thats the case we would like to
        /// pass through as it means the text is empty.
        if ((textField.text?.isEmpty) ?? true) {
            /// The default value is false here, so whenever we forget to mention the required Field
            /// We can assume that the field is not required.
            if requiredField ?? false {
                
                var newPlaceholder = textField.placeholder
                if !(newPlaceholder?.isEmpty)! {
                    newPlaceholder?.removeFirst()
                }
                placeHolderLabel.text = newPlaceholder
                requiredSymbol.isHidden = false
            } else {
                placeHolderLabel.text = textField.placeholder
                requiredSymbol.isHidden = true
            }
            self.warningLabel.text = ""
            textFieldHelper?.reconfigRow(for: self)
        }
        
        textField.placeholder = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let placeholder = placeHolderLabel.text else { return false }
        switch (ActorFormCellType.init(rawValue: placeholder)) {
        case .displayName, .companyName:
            if let _ = (string.rangeOfCharacter(from: NSCharacterSet.alphanumerics)) {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    if updatedText.count > 30 {
                        return false
                    }
                    textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                    return true
                } else { return false }
            } else if string == "" || string == " " {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    if updatedText.count > 30 {
                        return false
                    }
                    textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                    return true
                } else {return false}
            } else {
                return false
            }
        case .address, .remarks:
            if let text = textField.text,
               let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange,
                                                           with: string)
                if updatedText.count > 200 {
                    return false
                }
                textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                return true
            } else {return false}
        case .mobile:
            if let _ = (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)) {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    if updatedText.count > 20 {
                        return false
                    }
                    textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                    return true
                } else { return false }
            } else if string == "" {
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                    return true
                } else {return false}
            } else {
                return false
            }
        default:
            switch (ItemFormCellType(rawValue: placeholder)) {
            case .itemName:
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    if updatedText.count > 25 {
                        return false
                    }
                    textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                    return true
                } else {return false}
            case .itemQuantity:
                if let _ = (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)) {
                    if let text = textField.text,
                       let textRange = Range(range, in: text) {
                        let updatedText = text.replacingCharacters(in: textRange,
                                                                   with: string)
                        if Int(updatedText) ?? 0 > 99999 {
                            return false
                        }
                        textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                        return true
                    } else { return false }
                } else if string == "" {
                    if let text = textField.text,
                       let textRange = Range(range, in: text) {
                        let updatedText = text.replacingCharacters(in: textRange,
                                                                   with: string)
                        textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                        return true
                    } else {return false}
                } else {
                    return false
                }
            case .lowStockLimit:
                if let _ = (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits)) {
                    if let text = textField.text,
                       let textRange = Range(range, in: text) {
                        let updatedText = text.replacingCharacters(in: textRange,
                                                                   with: string)
                        if Int(updatedText) ?? 0 > 99999 {
                            return false
                        }
                        textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                        return true
                    } else { return false }
                } else if string == "" {
                    if let text = textField.text,
                       let textRange = Range(range, in: text) {
                        let updatedText = text.replacingCharacters(in: textRange,
                                                                   with: string)
                        textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                        return true
                    } else {return false}
                } else {
                    return false
                }
            default:
                if let text = textField.text,
                   let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    
                    textFieldSaveData?.updateModal(for: placeholder, value: updatedText)
                    return true
                } else {return false}
            }
        }
    }
    
    // FIXME: Pressing tab in textField shows all placeholder labels
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        guard let text = textField.text, let placeholder = placeHolderLabel.text else { return false}
        if requiredField ?? false {
            if placeholder == ActorFormCellType.mobile.rawValue {
                if isValid(mobile: text) {
                    textFieldSaveData?.updateModal(for: placeholder, value: text)
                }
            } else {
                textFieldSaveData?.updateModal(for: placeholder, value: text)
            }
        } else {
            switch (ActorFormCellType(rawValue: placeholder)) {
            case .email:
                if isValid(email: text) {
                    textFieldSaveData?.updateModal(for: placeholder, value: text)
                }
            default:
                textFieldSaveData?.updateModal(for: placeholder, value: text)
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
            
        if let placeholder = placeHolderLabel.text {
            switch ( ActorFormCellType(rawValue: placeholder)) {
            case .mobile:
                if !isValid(mobile: textField.text ?? "" ) {
                    UIView.animate(withDuration: 1, delay: 0, options: .transitionFlipFromLeft, animations: {
                        self.warningLabel.text = "Enter valid mobile number"
                    }, completion: nil)
                } else {
                    self.warningLabel.text = ""
                }
            case .email:
                if !isValid(email: (textField.text) ?? "") && (textField.text ?? "" ) != "" {
                    UIView.animate(withDuration: 1, delay: 0, options: .transitionFlipFromLeft, animations: {
                        self.warningLabel.text = "Enter valid email"
                    }, completion: nil)
                } else {
                    self.warningLabel.text = ""
                }
            default:
                break
            }
        }
        if let empty = textField.text?.isEmpty {
            if empty {
                placeHolderLabel.text = ""
                requiredSymbol.isHidden = true
                textField.placeholder = (textFieldHelper?.getPlaceholder(for: self))
                if let required = requiredField, required {
                    UIView.animate(withDuration: 1, delay: 0, options: .transitionFlipFromLeft, animations: {
                        self.warningLabel.text = "This field is required"
                    }, completion: nil)
                }
            }
        }
        textFieldHelper?.reconfigRow(for: self)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        guard let placeholder = placeHolderLabel.text else { return false }
        switch(placeholder) {
        default:
            textFieldSaveData?.updateModal(for: placeholder, value: "")
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldHelper?.assignNextResponder(from: self)
        return true
    }
    
}
