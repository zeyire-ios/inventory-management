//
//  VendorHelper.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 07/09/22.
//

import Foundation

struct ActorFormList {
    
    var forms: [[ActorFormCellType]] = [
        [.displayName, .companyName],
        [.mobile, .email, .address],
        [.remarks]
    ]
    
    func getNextIndex(for currentIndex: IndexPath) -> IndexPath?{
        var nextIndexPath: IndexPath = currentIndex
        switch (currentIndex.section) {
        case 0:
            if currentIndex.row < forms[0].count - 1 {
                nextIndexPath.row = currentIndex.row + 1
            } else {
                nextIndexPath.section += 1
                nextIndexPath.row = 0
            }
        case 1:
            if currentIndex.row < forms[1].count - 1 {
                nextIndexPath.row = currentIndex.row + 1
            } else {
                nextIndexPath.section += 1
                nextIndexPath.row = 0
            }
        case 2:
            if currentIndex.row < forms[2].count - 1 {
                nextIndexPath.row = currentIndex.row + 1
            } else {
                return nil
            }
        default:
            return nil
        }
        return nextIndexPath
    }
}
