//
//  ItemRecordCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 19/09/22.
//

import UIKit

class ItemRecordCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.ItemRecordCell"
    
    var mainStack: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 4
        return stackView
    }()
    
    var statusImageView: UIImageView = {
        var imageView = UIImageView(image: UIImage(systemName: "coloncurrencysign.circle"))
        imageView.tintColor = .systemTeal
        return imageView
    }()
    
    var statusLabel: UILabel = {
        var label = UILabel()
        label.text = "Order"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    var newItemView = UIView()
    
    var byImageView: UIImageView = {
        let config = UIImage.SymbolConfiguration(pointSize: 12, weight: .regular, scale: .medium)
        var imageView = UIImageView(image: UIImage(systemName: "person.text.rectangle"))
        imageView.tintColor = .label
        return imageView
    }()
    
    var byValue: UILabel = {
        var label = UILabel()
        label.text = "Unknown Person"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    var byView: UIView = {
        var view = UIView()
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
        return view
    }()
        
    var statusStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 1
        return stackView
    }()
    
    private var statusView = UIView()
    
    var topStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    var newQuantityValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()
    
    var newQuantityLabel: UILabel = {
        var label = UILabel()
        label.text = "Quantity"
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    var newQuantityStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.alignment = .center
        stackView.layer.cornerRadius = 8
        stackView.layer.masksToBounds = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 3, left: 2, bottom: 3, right: 2)
        stackView.clipsToBounds = false
        stackView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
        stackView.layer.shadowRadius = 1
        stackView.layer.shadowOpacity = 0.2
        return stackView
    }()
    
    var netQuantityLabel: UILabel = {
        var label = UILabel()
        label.text = "Total Stock "
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    var netQuantityValue: UILabel = {
        var label = UILabel()
        label.text = "0"
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.textColor = .label
        return label
    }()
    
    var netQuantityStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.alignment = .center
        stackView.layer.cornerRadius = 8
        stackView.layer.masksToBounds = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 3, left: 2, bottom: 3, right: 2)
        stackView.clipsToBounds = false
        stackView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
        stackView.layer.shadowRadius = 1
        stackView.layer.shadowOpacity = 0.2
        return stackView
    }()
    
    var dateImageView: UIImageView = {
        var config = UIImage.SymbolConfiguration(pointSize: 16, weight: .regular, scale: .medium)
        var imageView = UIImageView(image: UIImage(systemName: "calendar", withConfiguration: config))
        imageView.tintColor = .secondaryLabel
        return imageView
    }()
    
    var dateValue: UILabel = {
        var label = UILabel()
        label.text = "Not Available"
        label.font = UIFont.preferredFont(forTextStyle: .callout)
        label.textColor = .secondaryLabel
        label.textAlignment = .right
        return label
    }()
    
    // MARK: - Init
    
    override func prepareForReuse() {
        topStackView.isHidden = false
        byView.isHidden = false
        byImageView.image = nil
    }
    
    override func setupCell() {
        newItemViewConfig()
        byViewConfig()
        statusViewConfig()
        topStackViewConfig()
        mainStackConfig()
    }
    
    private func newItemViewConfig() {
        newItemView.addSubview(statusImageView)
        statusImageView.translatesAutoresizingMaskIntoConstraints = false
        newItemView.addSubview(statusLabel)
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            statusImageView.leadingAnchor.constraint(equalTo: newItemView.leadingAnchor),
            statusImageView.heightAnchor.constraint(equalTo: statusLabel.heightAnchor),
            statusImageView.widthAnchor.constraint(equalTo: statusImageView.heightAnchor),
            statusImageView.trailingAnchor.constraint(equalTo: statusLabel.leadingAnchor, constant: -4),
            statusImageView.centerYAnchor.constraint(equalTo: statusLabel.centerYAnchor),
            statusLabel.topAnchor.constraint(equalTo: newItemView.topAnchor, constant: 4),
            statusLabel.leadingAnchor.constraint(equalTo: statusImageView.trailingAnchor, constant: 4),
            statusLabel.bottomAnchor.constraint(equalTo: newItemView.bottomAnchor, constant: -4),
            statusLabel.trailingAnchor.constraint(lessThanOrEqualTo: newItemView.trailingAnchor),
        ])
        newItemView.setContentHuggingPriority(.init(251), for: .vertical)
    }
    
    private func byViewConfig() {
        byView.addSubview(byImageView)
        byImageView.translatesAutoresizingMaskIntoConstraints = false
        byView.addSubview(byValue)
        byValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            byImageView.topAnchor.constraint(equalTo: byView.topAnchor, constant: 4),
            byImageView.leadingAnchor.constraint(equalTo: byView.leadingAnchor),
            byImageView.bottomAnchor.constraint(equalTo: byView.bottomAnchor, constant: -4),
            byImageView.trailingAnchor.constraint(equalTo: byValue.leadingAnchor, constant: -4),
            byValue.centerYAnchor.constraint(equalTo: byImageView.centerYAnchor),
            byValue.leadingAnchor.constraint(equalTo: byImageView.trailingAnchor, constant: 4),
            byValue.trailingAnchor.constraint(lessThanOrEqualTo: byView.trailingAnchor),
        ])
        byValue.setContentHuggingPriority(.init(249), for: .horizontal)
    }
    
    private func statusViewConfig() {
        statusStackView.addArrangedSubview(newItemView)
        statusStackView.addArrangedSubview(byView)
        statusStackView.setContentHuggingPriority(.init(251), for: .vertical)
        statusView.addSubview(statusStackView)
        statusStackView.translatesAutoresizingMaskIntoConstraints = false
        statusView.addSubview(dateImageView)
        dateImageView.translatesAutoresizingMaskIntoConstraints = false
        statusView.addSubview(dateValue)
        dateValue.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            statusStackView.topAnchor.constraint(equalTo: statusView.topAnchor),
            statusStackView.leadingAnchor.constraint(equalTo: statusView.leadingAnchor),
            statusStackView.bottomAnchor.constraint(equalTo: statusView.bottomAnchor),
            statusStackView.trailingAnchor.constraint(lessThanOrEqualTo: dateImageView.leadingAnchor, constant: -8),
            dateImageView.topAnchor.constraint(greaterThanOrEqualTo: statusView.topAnchor),
            dateImageView.leadingAnchor.constraint(greaterThanOrEqualTo: statusStackView.trailingAnchor, constant: 8),
            dateImageView.bottomAnchor.constraint(lessThanOrEqualTo: statusView.bottomAnchor),
            dateImageView.centerYAnchor.constraint(equalTo: dateValue.centerYAnchor),
            dateImageView.trailingAnchor.constraint(equalTo: dateValue.leadingAnchor, constant: -4),
            dateValue.topAnchor.constraint(greaterThanOrEqualTo: statusView.topAnchor),
            dateValue.leadingAnchor.constraint(equalTo: dateImageView.trailingAnchor, constant: 4),
            dateValue.centerYAnchor.constraint(equalTo: statusStackView.centerYAnchor),
            dateValue.bottomAnchor.constraint(lessThanOrEqualTo: statusView.bottomAnchor),
            dateValue.trailingAnchor.constraint(equalTo: statusView.trailingAnchor),
        ])
        dateValue.setContentHuggingPriority(.init(251), for: .horizontal)
    }
    
    private func topStackViewConfig() {
        newQuantityStackView.addArrangedSubview(newQuantityValue)
        newQuantityStackView.addArrangedSubview(newQuantityLabel)
        netQuantityStackView.addArrangedSubview(netQuantityValue)
        netQuantityStackView.addArrangedSubview(netQuantityLabel)
        topStackView.addArrangedSubview(newQuantityStackView)
        topStackView.addArrangedSubview(netQuantityStackView)
    }
    
    private func mainStackConfig() {
        contentView.addSubview(mainStack)
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            mainStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
        ])
        mainStack.addArrangedSubview(statusView)
        mainStack.addArrangedSubview(topStackView)
    }
    
    func noOrder() {
        topStackView.isHidden = true
        byView.isHidden = true
    }
}
