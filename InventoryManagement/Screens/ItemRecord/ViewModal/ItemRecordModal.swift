//
//  File.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 19/09/22.
//

import Foundation

struct ItemRecordModal {
    var recordDate: Date?
    var netQuantity: Int?
    var purchaseOrder: PurchaseOrder?
    var salesOrder: SalesOrder?
    var adjustmentRecord: Bool?
}
