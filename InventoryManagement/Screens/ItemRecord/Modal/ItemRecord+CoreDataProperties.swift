//
//  ItemRecord+CoreDataProperties.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 25/09/22.
//
//

import Foundation
import CoreData


extension ItemRecord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemRecord> {
        return NSFetchRequest<ItemRecord>(entityName: "ItemRecord")
    }

    @NSManaged public var netQuantity: Int64
    @NSManaged public var recordDate: Date
    @NSManaged public var adjustmentRecord: Bool
    @NSManaged public var item: Item
    @NSManaged public var purchaseOrder: PurchaseOrder?
    @NSManaged public var salesOrder: SalesOrder?

}

extension ItemRecord : Identifiable {

}
