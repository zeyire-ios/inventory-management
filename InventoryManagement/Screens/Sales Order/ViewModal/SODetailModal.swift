//
//  SODetailModal.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 19/09/22.
//

import Foundation

struct SODetailModal {
    var salesID: UUID?
    var salesNo: String?
    var customer: Customer?
    var item: Item?
    var itemCost: Double?
    var itemQuantity: Int?
    var discount: Double?
    var totalCost: Double?
    var salesDate: Date?
    var itemRecord: ItemRecord?
}
