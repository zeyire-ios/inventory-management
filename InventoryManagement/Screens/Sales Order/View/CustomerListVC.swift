//
//  CustomerListVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 19/09/22.
//

import UIKit
import CoreData

class CustomerListVC: UIViewController {
    
    private lazy var tableView: UITableView = {
        var tableView = UITableView()
        tableView.register(ActorListCell.self, forCellReuseIdentifier: ActorListCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.noDataImage.image = UIImage(named: "Group")?.withTintColor(.secondaryLabel)
        noDataView.titleLabel.text = "No Customers are Added!"
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
    
    private lazy var createCustomerButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Add Customer"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(createCustomerPressed), for: .touchUpInside)
        return button
    }()
    
    private let CDM = CoreDataManager.shared
    
    private var customerListDisplayModals: [CustomerListDisplayModal]?
    
    var delegate: EntityListDelegate?
    
    var indexPath: IndexPath?
    
    var selected: UUID?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        isModalInPresentation = true
        
        setupConfig()
        fetchDisplayModal()
        
        addNotificationObservers()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupConfig() {
        tableViewConfig()
        navigationConfig()
        backgroundViewConfig()
    }
    
    private func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(createCustomerButton)
        createCustomerButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: tableView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            
            backgroundView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            backgroundView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            backgroundView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            createCustomerButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createCustomerButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
        ])
    }
    
    private func navigationConfig() {
        navigationItem.title = "Customers"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createCustomerPressed))
    }
    
    private func backgroundViewConfig() {
        backgroundView.isHidden = true
        createCustomerButton.isHidden = true
    }
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            let customerCoreDataList = self?.CDM.fetchCustomer(with: ["customerID", "displayName", "companyName", "profileImage"], sortBy: [NSSortDescriptor(key: "displayName", ascending: true)])
            self?.customerListDisplayModals = customerCoreDataList?.map({
                customer in
                CustomerListDisplayModal(customerID: customer.customerID, displayName: customer.displayName, companyName: customer.companyName, profileImage: customer.profileImage)
            })
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.selectPrevious()
                
            }
        }
    }
    
    private func selectPrevious() {
        if let id = selected {
            if let row = selectedRow(id: id) {
                let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0))
                cell?.accessoryType = .checkmark
            }
        }
    }
    
    private func selectedRow(id: UUID) -> Int? {
        guard let customerList = customerListDisplayModals else { return nil}
        for i in 0..<(customerList.count) {
            if id == customerList[i].customerID {
                return i
            }
        }
        return nil
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.reloadCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.deleteCustomer, object: nil)
    }
    
    @objc
    private func reloadModal() {
        fetchDisplayModal()
    }
    
    @objc
    private func createCustomerPressed() {
        present(UINavigationController(rootViewController: AddCustomerVC()), animated: true)
    }
}

extension CustomerListVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if customerListDisplayModals?.count == 0 || customerListDisplayModals == nil {
            backgroundView.isHidden = false
            createCustomerButton.isHidden = false
            return 0
        } else {
            backgroundViewConfig()
            return customerListDisplayModals?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let customer = customerListDisplayModals?[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorListCell.identifier, for: indexPath) as! ActorListCell
            
            cell.setNamesFor(
                actor: customer.displayName ?? "Unknown Customer",
                company: customer.companyName ?? "Unkown Company"
            )
            
            cell.setImage(imageName: customer.profileImage, for: .CustomerImage)
            
            return cell
        }
        return UITableViewCell()
    }
    
}

extension CustomerListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = customerListDisplayModals?[indexPath.row].customerID else {
            return
        }
        
        delegate?.selectedVendor(id: id, at: self.indexPath!)
        navigationController?.popViewController(animated: true)
    }
    
}
