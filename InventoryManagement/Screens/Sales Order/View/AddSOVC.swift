//
//  AddSOVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 19/09/22.
//

import UIKit
import CoreData

struct SOFormList {
    
    let forms: [OrderFormCellType] = [.customerID,
                                      .itemID,
                                      .itemQuantity,
                                      .itemCost,
                                      .orderDiscount,
                                      .purchaseDate,
                                      .orderSummary]
}

class AddSOVC: UIViewController {
 
    private let mainStack: UIStackView = {
        var stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var tableView: UITableView = {
        var tableView = UITableView()
        tableView.register(SelectionCell.self, forCellReuseIdentifier: SelectionCell.identifier)
        tableView.register(DatePickerCell.self, forCellReuseIdentifier: DatePickerCell.identifier)
        tableView.register(TextFieldNormCell.self, forCellReuseIdentifier: TextFieldNormCell.identifier)
        tableView.register(SummaryViewCell.self, forCellReuseIdentifier: SummaryViewCell.identifier)
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private let orderForm = SOFormList().forms
    
    private var soModal = SODetailModal()
   
    private var summaryIndex: IndexPath?
    
    private var doneButton: Bool = false {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = doneButton
        }
    }
    
    private var itemNetQuantity: Int {
        get {
            Int(soModal.item?.itemQuantity ?? 0) - (soModal.itemQuantity ?? 0)
        }
    }
    
    private var allCheck: Bool {
        get {
            if let _ = soModal.item, let _ = soModal.customer, let _ = soModal.itemQuantity, let _ = soModal.itemCost {
                return true
            } else {
                return false
            }
        }
    }
    
    private let CDM = CoreDataManager.shared
    
    // MARK: - Init
    
    init(item: Item? = nil, customer: Customer? = nil, quantity: Int? = nil, itemCost: Double? = nil, discount: Double? = nil) {
        self.soModal.item = item
        self.soModal.customer = customer
        self.soModal.itemQuantity = quantity
        self.soModal.itemCost = itemCost
        self.soModal.discount = discount
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        isModalInPresentation = true
        navigationController?.presentationController?.delegate = self
        // Do any additional setup after loading the view.
        setupConfig()
        scrollViewAboveKeyboard()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if mainStack.axis == .vertical && UIDevice.current.orientation.isLandscape {
            mainStack.axis = .horizontal
            mainStack.alignment = .fill
            mainStack.distribution = .fillEqually
            mainStack.addArrangedSubview(tableView)
        } else if mainStack.axis == .horizontal && UIDevice.current.orientation.isPortrait {
            mainStack.axis = .vertical
            mainStack.alignment = .fill
            mainStack.distribution = .fill
            mainStack.addArrangedSubview(tableView)
      }
    }
    
    deinit {
//        print("Dismissed")
    }
    
    private func setupConfig() {
        mainStackConfig()
        navigationConfig()
        updateSummary()
    }
    
    private func mainStackConfig() {
        view.addSubview(mainStack)
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainStack.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mainStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let size = UIScreen.main.bounds
        if size.width < size.height {
            mainStack.axis = .vertical
            mainStack.alignment = .fill
            mainStack.addArrangedSubview(tableView)
        } else {
            mainStack.axis = .horizontal
            mainStack.alignment = .fill
            mainStack.distribution = .fillEqually
            mainStack.addArrangedSubview(tableView)
        }
    }
    
    private func navigationConfig() {
        navigationItem.title = "Add Sales Order"
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.backButtonTitle = "Back"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(_:)))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    private func updateSummary() {
        if allCheck {
            doneButton = true
        } else {
            doneButton = false
        }
        if let indexPath = summaryIndex {
            tableView.reconfigureRows(at: [indexPath])
        }
    }
    
    // MARK: - Functions
    
    private func validateModal() -> Bool {
        guard let _ = soModal.customer, let item = soModal.item, let quantity = soModal.itemQuantity, let _ = soModal.itemCost else {
            return false
        }
        if (quantity - Int(item.itemQuantity)) > 0 {
            return false
        }
        return true
    }
    
    private func saveData(with modal: SODetailModal) {
        guard let customer = modal.customer, let item = modal.item, let cost = modal.itemCost, let quantity = modal.itemQuantity else { return }
        let order = SalesOrder(context: CDM.viewContext)
        order.salesNo = SOGenerator.getSO()
        print(order.salesNo)
        order.salesID = UUID()
        order.salesDate = modal.salesDate ?? Date()
        order.salesItemCost = cost
        order.salesQuantity = Int64(quantity)
        order.discount = Double(modal.discount ?? 0)
        let netCost = (cost * Double(quantity))
        order.salesTotalCost = netCost - (netCost * (order.discount / 100))
        order.item = item
        order.customer = customer
        let record = ItemRecord(context: CDM.viewContext)
        order.itemRecord = record
        DispatchQueue.global().async {
            [weak self] in
            do {
                guard let quantity = self?.itemNetQuantity else { return }
                item.itemQuantity = Int64(quantity)
                record.item = item
                record.salesOrder = order
                record.recordDate = order.salesDate
                record.netQuantity = Int64(quantity)
                
                try self?.CDM.viewContext.save()
                NotificationCenter.default.post(name: SOKey.reloadSO, object: nil)
                NotificationCenter.default.post(name: ItemKey.reloadItem, object: nil)
            } catch {
                print(error)
            }
        }
    }
    
    // This function makes the view move above the keyboard
    private func scrollViewAboveKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // MARK: - Objc function
    
    @objc
    private func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @objc
    private func doneButtonPressed(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        if validateModal() {
            DispatchQueue.global().async {
                [weak self] in
                self?.saveData(with: (self?.soModal)!)
            }
            dismiss(animated: true)
        }
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        tableView.scrollIndicatorInsets = tableView.contentInset
        
    }
}

extension AddSOVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderForm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let placeholder = orderForm[indexPath.row]
        switch (placeholder) {
            
        case .customerID:
            let cell = tableView.dequeueReusableCell(withIdentifier: SelectionCell.identifier, for: indexPath) as! SelectionCell
            cell.setValueFor(label: "Select Customer", value: soModal.customer?.displayName )
            cell.accessoryType = .disclosureIndicator
            return cell
        case .itemID:
            let cell = tableView.dequeueReusableCell(withIdentifier: SelectionCell.identifier, for: indexPath) as! SelectionCell
            cell.setValueFor(label: "Select Item", value: soModal.item?.itemName )
            cell.accessoryType = .disclosureIndicator
            return cell
        case .itemQuantity:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldNormCell.identifier, for: indexPath) as! TextFieldNormCell
            cell.setLabel("Item Quantity")
            cell.setTextFieldProperties(keyboardType: .numberPad, clearButtonMode: .whileEditing)
            cell.placeholder = placeholder
            if let limit = soModal.item?.quantityDetail[.available] {
                cell.showInstruction(with: "Item has \(limit) units available")
            }
            cell.textFieldSaveData = self
            cell.showWarningDelegate = self
            return cell
        case .itemCost:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldNormCell.identifier, for: indexPath) as! TextFieldNormCell
            cell.setLabel("Item Cost")
            cell.setTextFieldProperties(keyboardType: .decimalPad, clearButtonMode: .whileEditing)
            if let cost = soModal.item?.priceDetail[.sellprice] {
                cell.showInstruction(with: "Lastest price \(currencyFormatter.string(from: cost as NSNumber) ?? "")")
            }
            cell.placeholder = placeholder
            cell.textFieldSaveData = self
            if let text = soModal.itemCost {
                cell.setValueFor(textFieldValue: String(text))
            }
            return cell
        case .orderDiscount:
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldNormCell.identifier, for: indexPath) as! TextFieldNormCell
            cell.setLabel("Discount")
            cell.setTextFieldProperties(keyboardType: .decimalPad, clearButtonMode: .whileEditing)
            cell.showInstruction(with: "(Optional)")
            cell.placeholder = placeholder
            cell.textFieldSaveData = self
            if let text = soModal.discount {
                cell.setValueFor(textFieldValue: String(text))
            }
            return cell
        case .purchaseDate:
            let cell = tableView.dequeueReusableCell(withIdentifier: DatePickerCell.identifier, for: indexPath) as! DatePickerCell
            cell.setLabel("Sales Date")
            cell.datePickerDelegate = self
            cell.selectionStyle = .none
            return cell
        case .orderSummary:
            let cell = tableView.dequeueReusableCell(withIdentifier: SummaryViewCell.identifier, for: indexPath) as! SummaryViewCell
            cell.setValueFor(discount: "\(soModal.discount ?? 0)%")
            let subtotal = Double(soModal.itemQuantity ?? 0) * (soModal.itemCost ?? 0.0)
            let total = subtotal - (subtotal * (soModal.discount ?? 0)/100)
            if let subtotalCurrency = currencyFormatter.string(from: subtotal as NSNumber), let totalCurrency = currencyFormatter.string(from: total as NSNumber), subtotalCurrency.count < 20 {
                cell.setValueFor(
                    subtotal: subtotalCurrency,
                    total: totalCurrency
                )
            } else {
                cell.setValueFor(
                    subtotal: "-",
                    total: "-"
                )
            }
            cell.setValueFor(netQuantity: String(itemNetQuantity))
            self.summaryIndex = indexPath
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension AddSOVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeholder = orderForm[indexPath.row]
        
        switch(placeholder) {
        case .customerID:
            let vc = CustomerListVC()
            vc.delegate = self
            vc.indexPath = indexPath
            vc.selected = soModal.customer?.customerID
            navigationController?.pushViewController(vc, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        case .itemID:
            let vc = ItemListVC(hideZeroStock: true)
            vc.delegate = self
            vc.indexPath = indexPath
            vc.selected = soModal.item?.itemID
            navigationController?.pushViewController(vc, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        case .itemQuantity, .itemCost, .orderDiscount:
            let cell = tableView.cellForRow(at: indexPath) as! TextFieldNormCell
            cell.makeTextFieldFirstResponder()
            tableView.deselectRow(at: indexPath, animated: true)
        default:
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

extension AddSOVC: UISheetPresentationControllerDelegate {
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alert = UIAlertController(title: "", message: "Are you sure, you want to discard the changes?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Discard Changes", style: .destructive){
            action in
            self.dismiss(animated: true)
        })
        alert.addAction(UIAlertAction(title: "Keep Editing", style: .cancel))
        
        // For showing action sheet in iPad, we have to make it presented as popoverController.
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.leftBarButtonItem
        }
        
        self.present(alert, animated: true)
    }
}

extension AddSOVC: EntityListDelegate {
    
    func selectedVendor(id: UUID, at indexPath: IndexPath) {
        self.soModal.customer = CDM.fetchCustomer(for: id, with: [])?.first
        self.tableView.reconfigureRows(at: [indexPath])
        updateSummary()
    }
    
    func selectedItem(id: UUID, at indexPath: IndexPath) {
        self.soModal.item = CDM.fetchItem(for: id, with: [])?.first
        let nextIndex = IndexPath(item: indexPath.row + 1, section: indexPath.section)
        let costIndex = IndexPath(row: indexPath.row + 2, section: indexPath.section)
        if let cell = tableView.cellForRow(at: nextIndex) as? TextFieldNormCell {
            cell.showWarningDelegate?.checkWarning(for: cell)
        }
        self.tableView.reconfigureRows(at: [indexPath, costIndex, nextIndex])
        updateSummary()
    }
}

extension AddSOVC: TextFieldSaveData {
    
    func updateModal(for type: String, value: String) {
        switch( OrderFormCellType.init(rawValue: type)) {
        case.itemQuantity:
            soModal.itemQuantity = Int(value)
        case .itemCost:
            soModal.itemCost = Double(value)
        case .orderDiscount:
            soModal.discount = Double(value)
        default:
            return
        }
        updateSummary()
    }
    
}

extension AddSOVC: DatePickerDelegate {
    func dateSelected(date: Date) {
        soModal.salesDate = date
    }
}

extension AddSOVC: CheckWarningDelegate {
    func checkWarning(for cell: TextFieldNormCell) {
        if let index = tableView.indexPath(for: cell) {
            if cell.getLabel() == "Item Quantity" {
                if let filledText = cell.getTextFieldValue(), !(filledText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                    if let currentItemQuantity = soModal.item?.quantityDetail[.available] {
                        if (soModal.itemQuantity ?? 0) - Int(currentItemQuantity) > 0 {
                            cell.showWarning(with: "Not enough units in stock! (Available: \(currentItemQuantity))")
                            tableView.reconfigureRows(at: [index])
                            updateModal(for: cell.placeholder!.rawValue, value: cell.getTextFieldValue() ?? "")
                        } else {
                            cell.setWarningLabel(color: .secondaryLabel)
                            tableView.reconfigureRows(at: [index])
                        }
                    } else {
                        cell.showWarning(with: "Select an Item")
                        tableView.reconfigureRows(at: [index])
                    }
                } else {
                    cell.setWarningLabel(color: .secondaryLabel)
                    tableView.reconfigureRows(at: [index])
                }
            }
        }
    }
}
