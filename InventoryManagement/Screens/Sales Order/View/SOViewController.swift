//
//  SOViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 19/09/22.

import UIKit
import CoreData

class SOViewController: UIViewController {
    
    private lazy var tableView: UITableView = {
        var tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.register(OrderListCell.self, forCellReuseIdentifier: OrderListCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.backgroundColor = .systemGroupedBackground
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        activityView.hidesWhenStopped = true
        return activityView
    }()
    
    private let backgroundView: NoDataView = {
        var noDataView = NoDataView()
        noDataView.titleLabel.textColor = .secondaryLabel
        noDataView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        return noDataView
    }()
   
    private lazy var createSalesButton: UIButton = {
        var button = UIButton()
        var config = UIButton.Configuration.borderless()
        config.title = "Create Sales Order"
        config.buttonSize  = .medium
        config.titleAlignment = .center
        button.configuration = config
        button.addTarget(self, action:#selector(addButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter
    }()
    
    private var searchBar: UISearchController = {
        var searchController = UISearchController()
        searchController.searchBar.backgroundColor = .systemGroupedBackground
        return searchController
    }()
    
    private let noFilterViewImage = UIImage(named: "FilterCross")?.withTintColor(.secondaryLabel)
    
    private let noSearchViewImage = UIImage(named: "SearchProfile")?.withTintColor(.secondaryLabel)
    
    private let noOrderViewImage = UIImage(named: "NoTaskNotepad")?.withTintColor(.secondaryLabel)
    
    private let searchResultText = "Sorry, No Search Results!"
    
    private let emptyOrderText = "Sorry, No Orders are made yet!"
    
    private let CDM = CoreDataManager.shared
    
    private var filter: SOSort.FilterBy {
        SOSort.checkFilter()
    }
    
    private var soListModal: [SODetailModal]?
    
    private var sortBy: SOSort.SortBy {
        SOSort.checkSortBy()
    }
    
    private var ascending: Bool {
        SOSort.checkSortOrder()
    }
    
    private var timer = Timer()
    
    weak var splitDelegate: SplitControllerDelegate?
    
    private var firstTime: Bool = true {
        didSet {
            selectFirstRowInSplitView()
        }
    }
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .systemGroupedBackground
        
        setupConfig()
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
        
        addNotificationObservers()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupConfig() {
        tableViewConfig()
        splitViewConfig()
        navigationConfig()
        backgroundViewConifg()
        createRefreshControl()
    }
    
    func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        view.addSubview(createSalesButton)
        createSalesButton.translatesAutoresizingMaskIntoConstraints = false
        
        backgroundView.setAnchor(.centerY, to: view.centerYAnchor)
        backgroundView.setAnchor(.top, as: .greaterThanEqualTo, to: tableView.topAnchor)
        backgroundView.setAnchor(.bottom, as: .lessThanEqualTo, to: tableView.bottomAnchor)
        backgroundView.setAnchor(.leading, to: view.safeAreaLayoutGuide.leadingAnchor, padding: 16)
        backgroundView.setAnchor(.trailing, to: view.safeAreaLayoutGuide.trailingAnchor, padding: -16)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            activityIndicator.topAnchor.constraint(equalTo: tableView.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: tableView.trailingAnchor),
            
            createSalesButton.topAnchor.constraint(equalTo: backgroundView.bottomAnchor),
            createSalesButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            
        ])
    }
    
    private func splitViewConfig() {
        
        splitViewController?.delegate = self
        splitViewController?.preferredPrimaryColumnWidthFraction = 0.4
        
    }
    
    func navigationConfig() {
        navigationItem.title = "Sales Order"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barTintColor = .systemGroupedBackground
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.searchController = self.searchBar
        navigationItem.searchController?.searchBar.placeholder = "Search Orders"
        navigationItem.searchController?.searchBar.delegate = self
        navigationItem.searchController?.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.backButtonTitle = "Back"
        navigationItem.backButtonTitle = "Back"
        navigationItem.rightBarButtonItems = [ UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed(_:))), UIBarButtonItem(image: UIImage(systemName: "slider.horizontal.3"), style: .plain, target: self, action: #selector(sortButtonPressed)) ]
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: SOKey.reloadSO, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: ItemKey.deleteItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: ItemKey.deleteItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.reloadCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.deleteCustomer, object: nil)
    }
    
    private func backgroundViewConifg() {
        backgroundView.isHidden = true
        createSalesButton.isHidden = true
    }
    
    // MARK: - Functions
    
    // This function creates the list modal to display at table view
    func fetchToViewModal(searchFor searchTerm: String? = nil, sortBy: SOSort.SortBy, ascending: Bool, filterBy: SOSort.FilterBy) {
        DispatchQueue.global().async {
            [weak self] in
            let secondary: SOSort.SortBy = (sortBy == .salesNo ? .customer : .salesNo)
            var predicates: [NSPredicate] = []
            if let searchTerm = searchTerm {
                predicates.append(NSPredicate(format: "(salesNo CONTAINS[c] %@) OR (customer.displayName CONTAINS[c] %@)", searchTerm, searchTerm))
            }
            if filterBy != .all {
                predicates.append((self?.convertToCoreDataPredicate(filter: filterBy))!)
            }
            let soCoreDataList = self?.CDM.fetchSalesOrder(forItem: nil, check: predicates, sortBy: [NSSortDescriptor(key: self?.convertToCoreDataAttribute(sortBy), ascending: ascending), NSSortDescriptor(key: self?.convertToCoreDataAttribute(secondary), ascending: ascending)])
                self?.soListModal = soCoreDataList?.map({
                    so in
                    SODetailModal(salesID: so.salesID, salesNo: so.salesNo, customer: so.customer, item: so.item, itemCost: so.salesItemCost, itemQuantity: Int(so.salesQuantity), discount: so.discount, totalCost: so.salesTotalCost, salesDate: so.salesDate)
                })
            DispatchQueue.main.async {
                
                self?.showResultBackgroundFor(emptyList: (soCoreDataList?.isEmpty)!, withSearchTerm: searchTerm != nil, withFilter: filterBy != .all)
                
                self?.tableView.reloadData()
                if self?.firstTime ?? false {
                    self?.firstTime = false
                }
            }
        }
    }
    
    /// Generates a background view when the database can't fetch any results based on few conditions
    /// - Parameters:
    ///   - emptyList: A bool value that represents the database fetched zero entities
    ///   - withSearchTerm: A bool value that represents if a search term is used when fetching data
    ///   - withFilter: A bool value that represents whether any filters are applied.
    private func showResultBackgroundFor(emptyList: Bool, withSearchTerm: Bool, withFilter: Bool) {
        switch (emptyList, withSearchTerm, withFilter) {
            
            // When Searching produces no results
            // All Search results are applied with no filters, hence this case
        case (true, true, false):
            createSalesButton.isHidden = true
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noSearchViewImage
            backgroundView.titleLabel.text = searchResultText
            
            // When applying filters produces no results
        case (true, false, true):
            createSalesButton.isHidden = true
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noFilterViewImage
            switch (filter) {
            case .newLastWeek:
                backgroundView.titleLabel.text = "Sorry, No Orders made Last week!"
            case .newThisWeek:
                backgroundView.titleLabel.text = "Sorry, No Orders made this week!"
                #warning("Have to decide on this")
            case .recentlyCreated:
                backgroundView.titleLabel.text = "Sorry, No Orders created recently!"
            default:
                break
            }
            
            // When there are no more Items in Inventory.
        case (true, false, false):
            createSalesButton.isHidden = false
            backgroundView.isHidden = false
            backgroundView.noDataImage.image = noOrderViewImage
            backgroundView.titleLabel.text = emptyOrderText
            
        default:
            createSalesButton.isHidden = true
            backgroundView.isHidden = true
        }
    }
    
    private func createRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToReferesh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func deleteRefreshContorl() {
        tableView.refreshControl = nil
    }
    
    private func convertToCoreDataAttribute(_ attribute: SOSort.SortBy) -> String {
        switch (attribute) {
        case .salesNo:
            return "salesNo"
        case .customer:
            return "customer.displayName"
        case .date:
            return "salesDate"
        }
    }
    
    private func convertToCoreDataPredicate(filter: SOSort.FilterBy) -> NSPredicate {
        let calendar = Calendar.current
        let startOfToday = calendar.startOfDay(for: Date())
        let lastWeekEndDate = calendar.date(byAdding: .day, value: -7, to: startOfToday)
        let lastWeekStartDate = calendar.date(byAdding: .day, value: -7, to: lastWeekEndDate!)
        switch (filter) {
            
        case .all:
            return NSPredicate()
        case .newThisWeek:
            return NSPredicate(format: "salesDate > %@", lastWeekEndDate! as CVarArg)
        case .newLastWeek:
            return NSPredicate(format: "(%@ < salesDate) AND (salesDate < %@)", lastWeekStartDate! as CVarArg, lastWeekEndDate! as CVarArg)
        case .recentlyCreated:
            return NSPredicate(format: "salesDate > %@", startOfToday as CVarArg)
        }
    }
    
    private func selectFirstRowInSplitView() {
        if traitCollection.userInterfaceIdiom == .pad {
            if let id = soListModal?.first?.salesID {
                let vc = SODetailVC(id: id)
                splitDelegate?.itemSelected(present: vc)
            } else {
                let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
                vc?.setViewControllers([SplitViewController.blankVC], animated: true)
            }
        }
    }
    
    // MARK: - Objc fucntions
    
    @objc
    private func dismissView() {
        if traitCollection.userInterfaceIdiom == .pad {
            let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
            vc?.setViewControllers([SplitViewController.blankVC], animated: true)
        } else if traitCollection.userInterfaceIdiom == .phone {
            splitViewController?.hide(.secondary)
        }
    }
    
    @objc
    private func reloadModal() {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
    
    @objc
    private func addButtonPressed(_ sender: UIBarButtonItem) {
        let vc = UINavigationController(rootViewController: AddSOVC())
        vc.modalPresentationStyle = .formSheet
        present(vc, animated: true)
    }
    
    @objc
    private func sortButtonPressed(_ sender: UIBarButtonItem) {
        present(UINavigationController(rootViewController: SortViewController<SOSort.SortBy, SOSort.FilterBy>(sortModal: SOSort() as SortOptions, sortBy: SOSort.sortBy, sortedBy: sortBy, ascending: ascending, filterBy: SOSort.filterBy, filter: filter, completion: {
            [weak self] in
            self?.fetchToViewModal(sortBy: (self?.sortBy) ?? SOSort.SortBy.salesNo, ascending: (self?.ascending) ?? true, filterBy: self?.filter ?? SOSort.FilterBy.all)
        })), animated: true)
    }
    
    @objc
    private func pullToReferesh(_ sender: UIRefreshControl) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
        sender.endRefreshing()
    }
    
}

extension SOViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if soListModal?.count == 0 || soListModal == nil {
            tableView.isScrollEnabled = false
            return 0
        } else {
            tableView.isScrollEnabled = true
            backgroundViewConifg()
            return soListModal?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let modal = soListModal?[indexPath.row] else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderListCell.identifier, for: indexPath) as! OrderListCell
        
        cell.setValuesFor(
            orderNo: modal.salesNo ?? "000",
            actorName: modal.customer?.displayName ?? "Unknown Customer!",
            amount: currencyFormatter.string(from: (modal.totalCost ?? 0) as NSNumber),
            date: dateFormatter.string(from: (modal.salesDate) ?? Date(timeIntervalSince1970: 0))
        )
        
        cell.setForSales()
        
        return cell
    }
    
}

extension SOViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = soListModal?[indexPath.row].salesID else { return }
        let vc = SODetailVC(id: id)
        vc.hidesBottomBarWhenPushed = true
        splitDelegate?.itemSelected(present: vc)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions: [UIContextualAction] = [UIContextualAction]()
        let cloneAction = UIContextualAction(style: .normal,
                                             title: "Clone Order") {
                                            [weak self] (action, view, completionHandler) in
                                            self?.cloneOrder(indexPath)
                                            completionHandler(true)
        }
        cloneAction.image = UIImage(systemName: "doc.on.doc")
        actions.append(cloneAction)
        return UISwipeActionsConfiguration(actions: actions)
    }
    
    private func cloneOrder(_ indexPath: IndexPath) {
        if let order = soListModal?[indexPath.row] {
            let vc = UINavigationController(rootViewController: AddSOVC(item: order.item, customer: order.customer, itemCost: order.itemCost, discount: order.discount))
            vc.modalPresentationStyle = .formSheet
            present(vc, animated: true)
        }
    }
}

extension SOViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }
    
}

extension SOViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {
            [weak self] timer in
            if searchText.isEmpty {
                self?.fetchToViewModal(sortBy: self?.sortBy ?? SOSort.SortBy.salesNo, ascending: self?.ascending ?? true, filterBy: self?.filter ?? SOSort.FilterBy.all)
            } else {
                self?.fetchToViewModal(searchFor: searchText, sortBy: self?.sortBy ?? SOSort.SortBy.salesNo, ascending: self?.ascending ?? true, filterBy: self?.filter ?? SOSort.FilterBy.all)
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        fetchToViewModal(sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        timer.invalidate()
        fetchToViewModal(searchFor: searchBar.text, sortBy: sortBy, ascending: ascending, filterBy: filter)
    }
}

extension SOViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        deleteRefreshContorl()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        createRefreshControl()
    }
    
}
