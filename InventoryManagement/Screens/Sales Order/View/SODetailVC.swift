//
//  SODetailVC.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 10/10/22.
//

import UIKit

class SODetailVC: UIViewController {
    
    private lazy var orderMenu: UIMenu = {
        return UIMenu(title: "", image: nil, identifier: nil, options: .destructive, children: [bottomMenu])
    }()
    
    private lazy var bottomMenu = UIDeferredMenuElement.uncached( {
        [weak self] completion in
        var actions = [UIMenuElement]()
        actions.append(UIAction(title: "Clone Order",
                                image: {
            let image = UIImage(systemName: "doc.on.doc")
            return image
        }(),
                                handler: {
            [weak self] action in
            self?.cloneOrder()
        }))
        completion([UIMenu(title: "", options: .displayInline, children: actions)])
    })
    
    private lazy var mainTableView: UITableView = {
        var tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.backgroundColor = .systemGroupedBackground
        tableView.register(AmountCell.self, forCellReuseIdentifier: AmountCell.identifier)
        tableView.register(ActorListCell.self, forCellReuseIdentifier: ActorListCell.identifier)
        tableView.register(ItemQuantityCell.self, forCellReuseIdentifier: ItemQuantityCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.register(TotalDisplayCell.self, forCellReuseIdentifier: TotalDisplayCell.identifier)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .long
        return formatter
    }()
    
    private let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = NumberFormatter.Style.currency
        return formatter
    }()
    
    private var salesModal = SODetailModal()
    
    private let CDM = CoreDataManager.shared
    
    private var id: UUID
    
    // MARK: - Init
    
    init(id: UUID) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemGroupedBackground
        // Do any additional setup after loading the view.
        
        setupConfig()
        
        addNotificationObservers()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupConfig() {
        tableViewConfig()
        navigationConfig()
        fetchDisplayModal()
    }
    
    private func tableViewConfig() {
        view.addSubview(mainTableView)
        mainTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            mainTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func navigationConfig() {
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.backgroundColor = .systemGroupedBackground
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(title: "", image: UIImage(systemName: "ellipsis.circle"), primaryAction: nil, menu: orderMenu),
        ]
    }
    
    private func fetchDisplayModal() {
        DispatchQueue.global().async {
            [weak self] in
            if let salesOrder = self?.CDM.fetchSalesOrder(forOrder: self?.id, with: [], sortBy: [])?.first {
                self?.salesModal = SODetailModal( salesNo: salesOrder.salesNo, customer: salesOrder.customer, item: salesOrder.item, itemCost: salesOrder.salesItemCost, itemQuantity: Int(salesOrder.salesQuantity), discount: salesOrder.discount, totalCost: salesOrder.salesTotalCost, salesDate: salesOrder.salesDate)
            }
            DispatchQueue.main.async {
                self?.updateVC()
            }
        }
    }
    
    private func updateVC() {
        mainTableView.reloadData()
    }
    
    private func cloneOrder() {
        let vc = UINavigationController(rootViewController: AddSOVC(item: salesModal.item, customer: salesModal.customer, itemCost: salesModal.itemCost, discount: salesModal.discount))
        vc.modalPresentationStyle = .formSheet
        present(vc, animated: true)
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadModal), name: CustomerKey.reloadCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: ItemKey.deleteItem, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissView), name: CustomerKey.deleteCustomer, object: nil)
    }
    
    @objc
    private func reloadModal() {
        fetchDisplayModal()
    }
    
    @objc
    private func dismissView() {
        if traitCollection.userInterfaceIdiom == .phone {
            splitViewController?.hide(.secondary)
        } else if traitCollection.userInterfaceIdiom == .pad {
            let vc = splitViewController?.viewController(for: .secondary) as? UINavigationController
            vc?.setViewControllers([SplitViewController.blankVC], animated: true)
        }
    }
    
}

extension SODetailVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: AmountCell.identifier, for: indexPath) as! AmountCell
            cell.setValueFor(
                orderNo: salesModal.salesNo,
                dateLabel:  dateFormatter.string(from: salesModal.salesDate ?? Date(timeIntervalSince1970: 0))
            )
            cell.hideSeparator()
            cell.hideReceivedView()
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ActorListCell.identifier, for: indexPath) as! ActorListCell
            
            cell.setNamesFor(
                actor: salesModal.customer?.displayName ?? "Unknown Customer",
                company: salesModal.customer?.companyName ?? "Unknown Company"
            )
            if ((salesModal.customer) != nil) {
                cell.accessoryType = .detailButton
            } else {
                cell.accessoryType = .none
            }
            cell.setImage(imageName: salesModal.customer?.profileImage, for: .CustomerImage)
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemQuantityCell.identifier, for: indexPath) as! ItemQuantityCell
            cell.setValueFor(itemName: salesModal.item?.itemName)
            
            let itemCost = currencyFormatter.string(from: (salesModal.itemCost ?? 0) as NSNumber)
            let subtotal = (salesModal.itemCost ?? 0) * Double(salesModal.itemQuantity ?? 0)
            
            cell.setValueFor(
                denomination: "\(salesModal.itemQuantity ?? 0) x \(itemCost ?? "0")",
                subtotal: currencyFormatter.string(from: subtotal as NSNumber)
            )
            
            cell.hideSeparator()
            cell.setImage(imageName: salesModal.item?.itemImage)
            
            cell.accessoryType = .detailButton
            
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: TotalDisplayCell.identifier, for: indexPath) as! TotalDisplayCell
            let subtotal = (salesModal.itemCost ?? 0) * Double(salesModal.itemQuantity ?? 0)
            
            cell.setValueFor(
                subtotal: currencyFormatter.string(from: subtotal as NSNumber),
                discount: "\(salesModal.discount ?? 0)%",
                total: currencyFormatter.string(from: (salesModal.totalCost ?? 0) as NSNumber)
            )
            
            cell.hideLastSeparator()
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension SODetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let id = salesModal.customer?.customerID else {
                return }
            navigationController?.pushViewController(CustomerDetailVC(id: id), animated: true)
        }
        if indexPath.section == 2 {
            guard let id = salesModal.item?.itemID else { return }
            navigationController?.pushViewController(ItemDetailVC(id: id), animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch (section) {
        case 1:
            return "Customer"
        case 2:
            return "Item"
        case 3:
            return ""
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (section) {
        case 0:
            return 0.0
        case 1:
            return UITableView.automaticDimension
        case 2:
            return UITableView.automaticDimension
        case 3:
            return UITableView.automaticDimension
        default:
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
}
