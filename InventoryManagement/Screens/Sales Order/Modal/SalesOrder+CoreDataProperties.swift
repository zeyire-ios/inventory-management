//
//  SalesOrder+CoreDataProperties.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/10/22.
//
//

import Foundation
import CoreData


extension SalesOrder {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SalesOrder> {
        return NSFetchRequest<SalesOrder>(entityName: "SalesOrder")
    }

    @NSManaged public var salesDate: Date
    @NSManaged public var salesID: UUID
    @NSManaged public var salesItemCost: Double
    @NSManaged public var salesQuantity: Int64
    @NSManaged public var salesTotalCost: Double
    @NSManaged public var salesNo: String
    @NSManaged public var customer: Customer
    @NSManaged public var item: Item
    @NSManaged public var itemRecord: ItemRecord
    @NSManaged public var discount: Double

}

extension SalesOrder : Identifiable {

}
