//
//  SOSort.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 11/10/22.
//

import Foundation

struct SOSort: SortOptions {
    
    func setSortBy(sortBy: Any) {
        SOSort._setSortBy(sortBy: sortBy as! SortBy)
    }
    
    func setSortOrder(ascending: Bool) {
        SOSort._setSortOrder(ascending: ascending)
    }
    
    func setFilterBy(filterBy: Any) {
        SOSort._setFilter(by: filterBy as! FilterBy)
    }
    
    static let sortBy: [SortBy] = [.salesNo, .customer, .date]
    
    static let filterBy: [FilterBy] = [.all, .recentlyCreated, .newThisWeek, .newLastWeek]
    
    enum SortBy: String {
        case salesNo = "Sales Order"
        case customer = "Customer Name"
        case date = "Sales Date"
    }
    
    enum FilterBy: String {
        case all = "All Orders"
        case newThisWeek = "New This Week"
        case newLastWeek = "New Last Week"
        case recentlyCreated = "Recently Created Order"
    }
    
    static func checkSortOrder() -> Bool {
        if UserDefaults.standard.value(forKey: "SOAscending") == nil {
            UserDefaults.standard.set(true, forKey: "SOAscending")
        }
        return UserDefaults.standard.bool(forKey: "SOAscending")
    }
    
    static func _setSortOrder(ascending: Bool) {
        UserDefaults.standard.set(ascending, forKey: "SOAscending")
    }
    
    static func checkSortBy() -> SortBy {
        if UserDefaults.standard.value(forKey: "SOSortBy") == nil {
            UserDefaults.standard.set(SortBy.salesNo.rawValue, forKey: "SOSortBy")
        }
        return SortBy.init(rawValue: (UserDefaults.standard.string(forKey: "SOSortBy"))!)!
        
    }
    
    static func _setSortBy(sortBy: SortBy) {
        UserDefaults.standard.set(sortBy.rawValue, forKey: "SOSortBy")
    }
    
    static func checkFilter() -> FilterBy {
        if UserDefaults.standard.value(forKey: "SOFilter") == nil {
            UserDefaults.standard.set(FilterBy.all.rawValue, forKey: "SOFilter")
        }
        return FilterBy.init(rawValue: UserDefaults.standard.string(forKey: "SOFilter")!)!
    }
    
    static func _setFilter(by: FilterBy) {
        UserDefaults.standard.set(by.rawValue, forKey: "SOFilter")
    }
    
}
