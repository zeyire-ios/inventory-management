//
//  ItemSplitViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 17/10/22.
//

import UIKit

protocol SplitControllerDelegate: AnyObject {
    
    func itemSelected(present viewController: UIViewController)
    
}

class SplitViewController: UISplitViewController {
    
    static var blankVC: UIViewController {
        get {
            let vc = UIViewController()
            vc.view.backgroundColor = .systemGroupedBackground
            return vc
        }
    }
    
    lazy var itemVC: ItemViewController = {
        let vc = ItemViewController()
        vc.splitDelegate = self
        return vc
    }()
    
    lazy var customerVC: CustomerViewController = {
        let vc = CustomerViewController()
        vc.splitDelegate = self
        return vc
    }()
    
    lazy var vendorVC: VendorViewController = {
        let vc = VendorViewController()
        vc.splitDelegate = self
        return vc
    }()
    
    lazy var salesVC: SOViewController = {
        let vc = SOViewController()
        vc.splitDelegate = self
        return vc
    }()
    
    lazy var purchaseVC: POViewController = {
        let vc = POViewController()
        vc.splitDelegate = self
        return vc
    }()
    
    private var viewControllerType: TabBarSelectedType
    
    init(type: TabBarSelectedType, style: UISplitViewController.Style) {
        self.viewControllerType = type
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        preferredSplitBehavior = .tile
        preferredDisplayMode = .oneBesideSecondary
        maximumPrimaryColumnWidth = view.bounds.size.width/2;
        setupConfig()
        checkClassSize()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        let horizontal = traitCollection.horizontalSizeClass
        let vertical = traitCollection.verticalSizeClass
        switch (horizontal, vertical) {
        case (.regular, .regular):
            print("\nViewController Trait Change -> wRhR\n")
        case (.compact, .regular):
//            let _ = splitViewController(self, topColumnForCollapsingToProposedTopColumn: .primary)
            print("\nViewController Trait Change -> wChR\n")
        case (.regular, .compact):
            print("\nViewController Trait Change -> wRhC\n")
        case (.compact, .compact):
            print("\nViewController Trait Change -> wChC\n")
        default:
            print("\nViewController Trait Change -> Default\n")
        }
    }
    
    func checkClassSize() {
        let horizontal = traitCollection.horizontalSizeClass
        let vertical = traitCollection.verticalSizeClass
        switch (horizontal, vertical) {
        case (.regular, .regular):
            print("wRhR")
        case (.compact, .regular):
//            let _ = splitViewController(self, topColumnForCollapsingToProposedTopColumn: .primary)
            print("wChR")
        case (.regular, .compact):
            print("wRhC")
        case (.compact, .compact):
            print("wChC")
        default:
            print("Default")
        }
    }
    
    func setupConfig() {
        setViewController(SplitViewController.blankVC, for: .secondary)
        switch (viewControllerType) {
            
        case .salesOrder:
            setViewController(salesVC, for: .primary)
        case .purchaseOrder:
            setViewController(purchaseVC, for: .primary)
        case .vendor:
            setViewController(vendorVC, for: .primary)
        case .customer:
            setViewController(customerVC, for: .primary)
        case .inventory:
            setViewController(itemVC, for: .primary)
            
        }
        
    }
    
}

extension SplitViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        let horizontal = traitCollection.horizontalSizeClass
        let vertical = traitCollection.verticalSizeClass
        switch (horizontal, vertical) {
        case (.regular, .regular):
            print("\ntopColumnForCollopsing -> wRhR\n")
            return .primary
        case (.compact, .regular):
            print("\ntopColumnForCollopsing -> wChR\n")
            return .primary
        case (.regular, .compact):
            print("\ntopColumnForCollopsing -> wRhC\n")
            return .primary
        case (.compact, .compact):
            print("\ntopColumnForCollopsing -> wChC\n")
            return .primary
        default:
            print("\ntopColumnForCollopsing -> Default\n")
            return .primary
        }
        
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        let horizontal = traitCollection.horizontalSizeClass
        let vertical = traitCollection.verticalSizeClass
        switch (horizontal, vertical) {
        case (.regular, .regular):
            print("\ncollapable -> wRhR\n")
            return false
        case (.compact, .regular):
            print("\ncollapable -> wChR\n")
            return false
        case (.regular, .compact):
            print("\ncollapable -> wRhC\n")
            return false
        case (.compact, .compact):
            print("\ncollapable -> wChC\n")
            return false
        default:
            print("\ncollapable -> Default\n")
            return false
        }
        
    }
    
}

extension SplitViewController: SplitControllerDelegate {
    
    func itemSelected(present viewController: UIViewController) {
        let vc = UINavigationController(rootViewController: viewController)
        
        showDetailViewController(vc, sender: nil)
    }
    
}
