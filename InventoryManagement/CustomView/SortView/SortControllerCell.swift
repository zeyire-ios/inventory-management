//
//  SortControllerCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 12/10/22.
//

import UIKit

final class SortControllerCell: BaseTableViewCell {
    
    static let identifier = "com.abilash.SortControllerCell"
    
    private let ascendingImage = UIImage(named: "SortAscending")
    
    private let descendingImage = UIImage(named: "SortDescending")
    
    var title: UILabel = {
        var label = UILabel()
        return label
    }()
    
    var sortImage: UIImageView = {
        var imageView = UIImageView()
        imageView.tintColor = .label
        return imageView
    }()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            let config = UIImage.SymbolConfiguration.init(pointSize: 21, weight: .regular, scale: .default)
            self.accessoryView = UIImageView(image: UIImage(systemName: "checkmark.circle.fill", withConfiguration: config))
        } else {
            self.sortImage.image = nil
            self.accessoryView = nil
        }
        
    }
    
    override func setupCell() {
        contentView.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(sortImage)
        sortImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 14),
            title.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            title.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -14),
            
            sortImage.centerYAnchor.constraint(equalTo: title.centerYAnchor),
            sortImage.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            sortImage.heightAnchor.constraint(equalToConstant: 23.76),
            sortImage.widthAnchor.constraint(equalToConstant: 25),
        ])
    }
    
    override func prepareForReuse() {
        title.text = nil
        sortImage.image = nil
    }
}
