//
//  SortViewController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 11/10/22.
//

import UIKit

/// A Generic ViewController that provides sort and filter option to list views
final class SortViewController<SortType, FilterType>: UIViewController, UITableViewDataSource, UITableViewDelegate where SortType: RawRepresentable , SortType:Equatable, FilterType: RawRepresentable, FilterType: Equatable {
    
    lazy private var tableView: UITableView = {
        var tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(SortControllerCell.self, forCellReuseIdentifier: SortControllerCell.identifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.allowsMultipleSelection = true
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private let sortSectionHeaderView: SortSectionHeader = {
        let view = SortSectionHeader()
        let image = UIImage(named: "SortSymbol")?.withTintColor(.secondaryLabel)
        view.imageView.image = image
        view.title.text = "Sort By"
        view.title.font = .preferredFont(forTextStyle: .subheadline)
        view.title.textColor = .secondaryLabel
        return view
    }()
    
    private let filterSectionHeaderView: SortSectionHeader = {
        let view = SortSectionHeader()
        let image = UIImage(named: "FilterSymbol")?.withTintColor(.secondaryLabel)
        view.imageView.image = image
        view.title.text = "Filter By"
        view.title.font = .preferredFont(forTextStyle: .subheadline)
        view.title.textColor = .secondaryLabel
        return view
    }()
    
    private var filterBy: [FilterType]?

    private var filter: FilterType? {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    private var sortBy: [SortType]
    
    private var sortedBy: SortType {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    private var ascending: Bool {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    private let sortAscending = UIImage(named: "SortAscending")
    
    private let sortDescending = UIImage(named: "SortDescending")
    
    private let sortModal: SortOptions
    
    private let completion: ()->()
    
    // MARK: - Init
    init(sortModal: SortOptions, sortBy: [SortType], sortedBy: SortType, ascending: Bool, filterBy: [FilterType]?, filter: FilterType?, completion: @escaping ()->() ) {
        self.sortModal = sortModal
        self.sortBy = sortBy
        self.filterBy = filterBy
        self.ascending = ascending
        self.sortedBy = sortedBy
        self.filter = filter
        self.completion = completion
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("SortViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        isModalInPresentation = true
        
        setupConfig()
    }
    
    private func setupConfig() {
        
        tableViewConfig()
        navigationConfig()
        
    }
    
    private func tableViewConfig() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    private func navigationConfig() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelButtonPressed))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Apply", style: .done, target: self, action: #selector(applyButtonPressed))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    // MARK: - Functions
    @objc
    private func cancelButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @objc
    private func applyButtonPressed(_ sender: UIBarButtonItem) {
        sortModal.setSortBy(sortBy: sortedBy)
        sortModal.setSortOrder(ascending: ascending)
        sortModal.setFilterBy(filterBy: filter as Any)
        completion()
        dismiss(animated: true)
    }
    // MARK: - TableView DataSource
    /// This was necessary as the Generic class can't have extensions with objc members in it.
    func numberOfSections(in tableView: UITableView) -> Int {
        if filterBy == nil {
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return sortSectionHeaderView
        } else {
            return filterSectionHeaderView
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return sortBy.count
        } else {
            return filterBy?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SortControllerCell.identifier, for: indexPath) as! SortControllerCell
        cell.selectionStyle = .none
        
        if indexPath.section == 0 {
            cell.title.text = sortBy[indexPath.row].rawValue as? String
            /// Selecting the current sort option
            if sortedBy == sortBy[indexPath.row] {
                cell.sortImage.image = ascending ? sortAscending : sortDescending
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
        } else if indexPath.section == 1 {
            cell.title.text = filterBy?[indexPath.row].rawValue as? String
            /// Selecting the current filter option
            if filter == filterBy?[indexPath.row] {
                cell.sortImage.image = nil
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
        }
        return cell
    }
    
    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        /// When another sort option is selected, the previous sort option will be deselected; the accessory view is handled inside cell
        if indexPath.section == 0, let row = sortBy.firstIndex(of: sortedBy), indexPath.row != row {
            
            tableView.deselectRow(at: IndexPath(row: row, section: 0), animated: false)
            
        }
        /// When another filter option is selected, the previous filter option will be deselected;
        /// the accessory view is handled inside cell
        else if let row = filterBy?.firstIndex(of: filter!), indexPath.row != row {
            
            tableView.deselectRow(at: IndexPath(row: row, section: 1), animated: false)
            
        }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
            let cell = tableView.cellForRow(at: indexPath) as! SortControllerCell
            /// At first time of running this line, the appropriate sort setting is selected for user.
            cell.sortImage.image = ascending ? sortAscending : sortDescending
            
            sortedBy = sortBy[indexPath.row]
            
        } else {
            
            filter = filterBy?[indexPath.row]
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        /// Whenever the same selected cell is been selected again, this check makes sure it doesn't get
        /// deselected, as the tableview has multi-select enabled; For sort option, we toggle the setting
        /// when same cell is reselected, which takes effect through the image view.
        if indexPath.section == 0, let row = sortBy.firstIndex(of: sortedBy), indexPath.row == row {
            
                let cell = tableView.cellForRow(at: indexPath) as! SortControllerCell
                ascending.toggle()
                cell.sortImage.image = ascending ? sortAscending : sortDescending
                return nil
            
        } else if let row = filterBy?.firstIndex(of: filter!), indexPath.row == row {
            
                return nil
            
        }
        return indexPath
    }
    
}
