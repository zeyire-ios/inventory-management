//
//  SortOptions.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 16/10/22.
//

import Foundation

protocol SortOptions {
    
    func setSortBy(sortBy: Any)
    
    func setSortOrder(ascending: Bool)
    
    func setFilterBy(filterBy: Any)
    
}

extension SortOptions {
    func setFilterBy(filterBy: Any) {
        
    }
}
