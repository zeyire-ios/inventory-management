//
//  NoDataView.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 15/10/22.
//

import UIKit

final class NoDataView: UIView {
    
    var noDataImage: UIImageView = {
        var imageView = UIImageView(image: UIImage(named: "SearchProfile"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    var titleLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle: .title2)
        return label
    }()
    
    var messageLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()
    
    init() {
        super.init(frame: CGRect())
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func setupView() {
        addSubview(noDataImage)
        noDataImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(messageLabel)
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            noDataImage.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            noDataImage.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 8),
            noDataImage.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -8),
            noDataImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            noDataImage.heightAnchor.constraint(equalToConstant: 150),
            noDataImage.widthAnchor.constraint(equalToConstant: 120),

            titleLabel.topAnchor.constraint(equalTo: noDataImage.bottomAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -8),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),

            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16),
            messageLabel.leadingAnchor.constraint(lessThanOrEqualTo: leadingAnchor, constant: 8),
            messageLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -8),
            messageLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            
        ])
    }
    
}
