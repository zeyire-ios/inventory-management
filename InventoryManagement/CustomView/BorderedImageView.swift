//
//  BorderedImageView.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 23/09/22.
//

import UIKit

class BorderedImageView: UIImageView {

    let _border = CAShapeLayer()
    
    init() {
        super.init(frame: CGRect())
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    func setup() {
        _border.strokeColor = UIColor.black.cgColor
        _border.fillColor = nil
        _border.lineDashPattern = [4, 4]
        self.layer.addSublayer(_border)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        _border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius:10).cgPath
        _border.frame = self.bounds
    }
    
}
