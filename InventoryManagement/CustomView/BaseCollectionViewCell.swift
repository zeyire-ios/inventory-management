//
//  BaseCollectionViewCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 30/09/22.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    func commonInit() {
        
    }
    
}
