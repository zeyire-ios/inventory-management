//
//  SortSectionHeader.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 16/10/22.
//

import UIKit

class SortSectionHeader: UIView {
    
    var imageView: UIImageView = {
        var imageView = UIImageView()
        return imageView
    }()
    
    var title: UILabel = {
        var label = UILabel()
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func commonInit() {
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.centerYAnchor.constraint(equalTo: title.centerYAnchor),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            imageView.heightAnchor.constraint(equalToConstant: 18),
            imageView.widthAnchor.constraint(equalToConstant: 18),
            
            title.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            title.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 6),
            title.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            
        ])
    }
    
}
