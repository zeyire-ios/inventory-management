//
//  BaseTableViewCell.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 01/10/22.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    func setupCell() {
        
    }

}
