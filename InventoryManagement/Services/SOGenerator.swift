//
//  SOGenerator.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/10/22.
//

import Foundation

struct SOGenerator {
    private static let prefix: String = "SO-"
    
    private static var id: String {
        get {
            let number = UserDefaults.standard.integer(forKey: "SOGenerator")
            if number != 0 {
                return prefix + "\(number)"
            } else {
                return ""
            }
        }
    }
    
    static func getSO() -> String {
        let id = SOGenerator.id
        var number = UserDefaults.standard.integer(forKey: "SOGenerator")
        number += 1
        UserDefaults.standard.set(number, forKey: "SOGenerator")
        return id
    }
    
    static func checkPersistance() {
        if UserDefaults.standard.integer(forKey: "SOGenerator") == 0 {
            UserDefaults.standard.set(1, forKey: "SOGenerator")
        } else {
            
        }
    }
    
    static func resetSOGenerator() {
        UserDefaults.standard.set(0, forKey: "SOGenerator")
    }
}
