//
//  URLStore.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 20/09/22.
//

import Foundation

struct URLStore {
    static let itemImageURL: URL = FileManager.documentDirectoryURL.appendingPathComponent("Item_Image")
    static let customerImageURL: URL = FileManager.documentDirectoryURL.appendingPathComponent("Customer_Image")
    static let vendorImageURL: URL = FileManager.documentDirectoryURL.appendingPathComponent("Vendor_Image")
    
    static func createDirectory(for type: ImageType) {
        do {
            switch (type) {
            case .ItemImage:
                try FileManager.default.createDirectory(at: URLStore.itemImageURL, withIntermediateDirectories: true, attributes: nil)
            case .CustomerImage:
                try FileManager.default.createDirectory(at: URLStore.customerImageURL, withIntermediateDirectories: true, attributes: nil)
            case .VendorImage:
                try FileManager.default.createDirectory(at: URLStore.vendorImageURL, withIntermediateDirectories: true, attributes: nil)
            }
        } catch (let error) {
            print("\n\nCan't create Directory:\n\(error)\n\n")
        }
    }
}
