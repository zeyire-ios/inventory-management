//
//  ImageHelper.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 29/09/22.
//

import Foundation
import UIKit

enum ImageType {
    case ItemImage
    case CustomerImage
    case VendorImage
}

struct ImageHelper {
    
    static func writeImage(_ image: UIImage,for type: ImageType, to url: URL, with name: String) {
        switch (type) {
        case .CustomerImage:
            if !FileManager.default.fileExists(atPath: URLStore.customerImageURL.path) {
                URLStore.createDirectory(for: .CustomerImage)
            }
        case .ItemImage:
            if !FileManager.default.fileExists(atPath: URLStore.itemImageURL.path) {
                URLStore.createDirectory(for: .ItemImage)
            }
        case .VendorImage:
            if !FileManager.default.fileExists(atPath: URLStore.vendorImageURL.path) {
                URLStore.createDirectory(for: .VendorImage)
            }
        }
        let imagePath = url.appendingPathComponent(name).appendingPathExtension("jpeg")
        if let data = image.jpegData(compressionQuality: 0.5) {
            do {
                try data.write(to: imagePath, options: .atomic)
            } catch let error {
                print("\n\nUnable to Write Image Data to Disk:\n\(error)\n\n")
            }
        }
    }
    
    static func deleteImage(_ name: UUID?, for type: ImageType ) {
        if let imageName = name?.uuidString {
            var imagePath: URL
            switch (type) {
                
            case .ItemImage:
                imagePath = URLStore.itemImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            case .CustomerImage:
                imagePath = URLStore.customerImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            case .VendorImage:
                imagePath = URLStore.vendorImageURL.appendingPathComponent(imageName).appendingPathExtension("jpeg")
            }
            
            if FileManager.default.fileExists(atPath: imagePath.path) {
                do {
                    try FileManager.default.removeItem(at: imagePath)
                } catch let error {
                    print("\n\nImageHelper: -> Deleting ImageFile from local:\n\(error)\n\n")
                }
            }
        }
    }
    
}
