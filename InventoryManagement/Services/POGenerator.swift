//
//  POGenerator.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 05/10/22.
//

import Foundation

struct POGenerator {
    private static let prefix: String = "PO-"
    
    private static var id: String {
        get {
            let number = UserDefaults.standard.integer(forKey: "POGenerator")
            if number != 0 {
                return prefix + "\(number)"
            } else {
                return ""
            }
        }
    }
    
    static func getPO() -> String {
        let id = POGenerator.id
        var number = UserDefaults.standard.integer(forKey: "POGenerator")
        number += 1
        UserDefaults.standard.set(number, forKey: "POGenerator")
        return id
    }
    
    static func checkPersistance() {
        if UserDefaults.standard.integer(forKey: "POGenerator") == 0 {
            UserDefaults.standard.set(1, forKey: "POGenerator")
        } else {
            
        }
    }
    
    static func resetPOGenerator() {
        UserDefaults.standard.set(0, forKey: "POGenerator")
    }
}
