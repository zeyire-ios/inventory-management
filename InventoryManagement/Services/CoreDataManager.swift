//
//  CoreDataManager.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 31/08/22.
//

import Foundation
import CoreData


class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    private init() {
        self.viewContext.mergePolicy = NSMergePolicy.error
    }
    
    lazy var viewContext = persistentContainer.viewContext
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "InventoryManagement")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func fetchVendor(for id: UUID? = nil,with properties: [String] = [], check conditions: [NSPredicate] = [], sortBy: [NSSortDescriptor] = []) -> [Vendor]? {
        let fetchRequest = NSFetchRequest<Vendor>(entityName: "Vendor")
        var predicates: [NSPredicate] = []
        if let id = id {
            fetchRequest.predicate = NSPredicate(format: "vendorID == %@", id.uuidString)
        }
        if !conditions.isEmpty {
            predicates.append(contentsOf: conditions)
        }
        if !predicates.isEmpty {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        if !properties.isEmpty {
            fetchRequest.resultType = .managedObjectResultType
            fetchRequest.propertiesToFetch = properties
        }
        if !sortBy.isEmpty {
            fetchRequest.sortDescriptors = sortBy
        }
        do {
            let vendorCoreDataList = try viewContext.fetch(fetchRequest)
            return vendorCoreDataList
        } catch {
            fatalError("Check in CDM FetchVendor")
        }
    }
    
    func fetchCustomer(for id: UUID? = nil, with properties: [String] = [], check conditions: [NSPredicate] = [], sortBy: [NSSortDescriptor] = []) -> [Customer]? {
        let fetchRequest = NSFetchRequest<Customer>(entityName: "Customer")
        var predicates: [NSPredicate] = []
        if let id = id {
            fetchRequest.predicate = NSPredicate(format: "customerID == %@", id.uuidString)
        }
        if !conditions.isEmpty {
            predicates.append(contentsOf: conditions)
        }
        if !predicates.isEmpty {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        if !properties.isEmpty {
            fetchRequest.resultType = .managedObjectResultType
            fetchRequest.propertiesToFetch = properties
        }
        if !sortBy.isEmpty {
            fetchRequest.sortDescriptors = sortBy
        }
        do {
            let customerCoreDataList = try viewContext.fetch(fetchRequest)
            return customerCoreDataList
        } catch {
            fatalError("Check in CDM FetchCustomer")
        }
    }
    
    func fetchItem(for id: UUID? = nil, with properties: [String] = [], check conditions: [NSPredicate] = [], sortBy: [NSSortDescriptor] = []) -> [Item]? {
        let fetchRequest = NSFetchRequest<Item>(entityName: "Item")
        var predicates: [NSPredicate] = []
        if let id = id {
            fetchRequest.predicate = NSPredicate(format: "itemID == %@", id.uuidString)
        }
        if !conditions.isEmpty {
            predicates.append(contentsOf: conditions)
        }
        if !predicates.isEmpty {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        if !properties.isEmpty {
            fetchRequest.resultType = .managedObjectResultType
            fetchRequest.propertiesToFetch = properties
        }
        if !sortBy.isEmpty {
            fetchRequest.sortDescriptors = sortBy
        }
        do {
            let itemCoreDataList = try viewContext.fetch(fetchRequest)
            return itemCoreDataList
        } catch let error {
            print("\n\nCheck in CDM FetchItem:\n\(error)\n\n")
        }
        return nil
    }
    
    func fetchItemRecord(forItem id: UUID? = nil, with properties: [String] = [], check conditions: [NSPredicate] = [], sortBy: [NSSortDescriptor] = []) -> [ItemRecord]? {
        let fetchRequest = NSFetchRequest<ItemRecord>(entityName: "ItemRecord")
        var predicates: [NSPredicate] = []
        if let id = id {
            predicates.append( NSPredicate(format: "item.itemID == %@", id.uuidString) )
        }
        if !conditions.isEmpty {
            predicates.append(contentsOf: conditions)
        }
        if !predicates.isEmpty {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        if !properties.isEmpty {
            fetchRequest.resultType = .managedObjectResultType
            fetchRequest.propertiesToFetch = properties
        }
        if !sortBy.isEmpty {
            fetchRequest.sortDescriptors = sortBy
        }
        do {
            let itemRecordCoreDataList = try viewContext.fetch(fetchRequest)
            return itemRecordCoreDataList
        } catch {
            fatalError("Check in CDM FetchItemRecord")
        }
    }
    
    func fetchPurchaseOrder(forOrder purchaseID: UUID? = nil, forItem itemID: UUID? = nil, with properties: [String] = [], check conditions: [NSPredicate] = [], sortBy: [NSSortDescriptor] = []) -> [PurchaseOrder]? {
        let fetchRequest = NSFetchRequest<PurchaseOrder>(entityName: "PurchaseOrder")
        var predicates: [NSPredicate] = []
        if let id = itemID {
            predicates.append( NSPredicate(format: "item.itemID == %@", id.uuidString))
        }
        if let id = purchaseID {
            predicates.append( NSPredicate(format: "purchaseID == %@", id.uuidString))
        }
        if !conditions.isEmpty {
            predicates.append(contentsOf: conditions)
        }
        if !predicates.isEmpty {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        if !properties.isEmpty {
            fetchRequest.resultType = .managedObjectResultType
            fetchRequest.propertiesToFetch = properties
        }
        if !sortBy.isEmpty {
            fetchRequest.sortDescriptors = sortBy
        }
        do {
            let purchaseOrderCoreDataList = try viewContext.fetch(fetchRequest)
            return purchaseOrderCoreDataList
        } catch let error {
            print("\n\n Error From PurchaseOrder CDM : \n\(error)\n\n")
            return nil
        }
    }
    
    func fetchSalesOrder(forOrder salesID: UUID? = nil, forItem id: UUID? = nil, with properties: [String] = [], check conditions: [NSPredicate] = [], sortBy: [NSSortDescriptor] = []) -> [SalesOrder]? {
        let fetchRequest = NSFetchRequest<SalesOrder>(entityName: "SalesOrder")
        var predicates: [NSPredicate] = []
        if let id = id {
            predicates.append(NSPredicate(format: "item.itemID == %@", id.uuidString))
        }
        if let id = salesID {
            predicates.append(NSPredicate(format: "salesID == %@", id.uuidString))
        }
        if !conditions.isEmpty {
            predicates.append(contentsOf: conditions)
        }
        if !predicates.isEmpty {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        if !properties.isEmpty {
            fetchRequest.resultType = .managedObjectResultType
            fetchRequest.propertiesToFetch = properties
        }
        if !sortBy.isEmpty {
            fetchRequest.sortDescriptors = sortBy
        }
        do {
            let salesOrderCoreDataList = try viewContext.fetch(fetchRequest)
            return salesOrderCoreDataList
        } catch let error {
            print("\n\n Error From SalesOrder CDM : \n\(error)\n\n")
            return nil
        }
    }
    
}
