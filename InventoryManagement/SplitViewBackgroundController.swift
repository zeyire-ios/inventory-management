//
//  SplitViewBackgroundController.swift
//  InventoryManagement
//
//  Created by abilash-14145 on 17/10/22.
//

import UIKit

class SplitViewBackgroundController: UIViewController {
    
    private let type: TabBarSelectedType
    
    init(type: TabBarSelectedType) {
        self.type = type
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGroupedBackground
        let splitVC = SplitViewController(type: type, style: .doubleColumn)
        
        addChild(splitVC)
        view.addSubview(splitVC.view)
        splitVC.didMove(toParent: self)
        splitVC.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            splitVC.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            splitVC.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            splitVC.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            splitVC.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        ])
    }
    
}
